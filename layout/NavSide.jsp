<nav class="navbar-default navbar-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="main-menu">
			<li>
				<div class="user-img-div">
					<div class="inner-text">
						<%=(String) request.getSession().getAttribute("SessionLoginUSERNAME") %>
					</div>
				</div>
			</li>
			<% if(((String) request.getSession().getAttribute("SessionLoginROLE")).equals("ADMIN")) { %>
			<li>
				<a href="UserList.jsp"><i class="fa fa-desktop "></i>Master User</a>
			</li>
			<li>
				<a href="#"><i class="fa fa-desktop "></i>Staff and Department <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
	                <li>
	                    <a href="StaffList.jsp"><i class="fa fa-desktop"></i>Staff</a>
	                </li>
	                <li>
	                    <a href="DepartmentList.jsp"><i class="fa fa-desktop"></i>Department</a>
	                </li>
	            </ul>
			</li>
			<li>
				<a href="#"><i class="fa fa-desktop "></i>Device and Client <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
	                <li>
	                    <a href="DeviceList.jsp"><i class="fa fa-desktop"></i>Device</a>
	                </li>
	                <li>
	                    <a href="ClientList.jsp"><i class="fa fa-desktop"></i>Client</a>
	                </li>
	            </ul>
			</li>
			<li>
				<a href="#"><i class="fa fa-desktop "></i>Branch and Area<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
	                <li>
	                    <a href="BranchList.jsp"><i class="fa fa-desktop"></i>Branch</a>
	                </li>
	                <li>
	                    <a href="AreaList.jsp"><i class="fa fa-desktop"></i>Area</a>
	                </li>
	            </ul>
			</li>
			<li>
				<a  href="ReasonList.jsp"><i class="fa fa-desktop "></i>Master Reason</a>
			</li>
			<li>
				<a  href="AccessGroupList.jsp"><i class="fa fa-desktop "></i>Access Group</a>
			</li>
			<li>
				<a href="#"><i class="fa fa-desktop "></i>Transaction <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
	                <li>
	                    <a href="AttendanceList.jsp"><i class="fa fa-desktop"></i>Transaction</a>
	                </li>
	                <!-- <li>
	                    <a href="InOutList.jsp"><i class="fa fa-desktop"></i>Attendance</a>
	                </li> -->
	                <!-- <li>
	                    <a href="AbsentList.jsp"><i class="fa fa-desktop"></i>Absent</a>
	                </li>  -->
	                <li>
	                    <a href="WorkHourList.jsp"><i class="fa fa-desktop"></i>Total Hour</a>
	                </li>
	            </ul>
			</li>
			<li>
				<a  href="LogList.jsp"><i class="fa fa-desktop "></i>Log</a>
			</li>
			<% } %>
			<li>
				<a href="#"><i class="fa fa-desktop "></i>Report <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
	                <li>
	                    <a href="Report.jsp"><i class="fa fa-desktop"></i>Report</a>
	                </li>
	                <li>
	                    <a href="Stats.jsp"><i class="fa fa-desktop"></i>Statistic</a>
	                </li>
	            </ul>
			</li>
			<li>
				<a href="processor/Logout.jsp"><i class="fa fa-sign-in "></i>Logout</a>
			</li>
		</ul>
	</div>

</nav>