SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_attendance_duration_single] AS
SELECT * FROM (
SELECT
ROW_NUMBER() over (Partition by v_attendance_duration.ATTENDANCEDATE, v_attendance_duration.STAFFID ORDER BY v_attendance_duration.CLOCKINTIME DESC) as RowNum, 
v_attendance_duration.STAFFID,
v_attendance_duration.ATTENDANCEDATE,
v_attendance_duration.CLOCKINTIME,
v_attendance_duration.CLOCKOUTTIME,
v_attendance_duration.WORKHOUR,
v_attendance_duration.WORKMIN,
v_attendance_duration.WORKSEC
FROM v_attendance_duration 
) TABLE1
WHERE RowNum = 1

GO

