<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
 
 <script src="assets/js/flot/excanvas.min.js"></script>
 <script src="assets/js/flot/jquery.flot.min.js"></script>
 <script src="assets/js/flot/jquery.flot.pie.min.js"></script>
 
<!-- CSS -->
<style type="text/css">
#flotcontainer {
    width: 600px;
    height: 200px;
    text-align: left;
}
</style>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBDEPARTMENT"%>
<%@page import="util.DateFormatConverter"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>

<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="DepartmentMgr" scope="page" class="Biometrics.DepartmentManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String sTableTitle = "This Month Statistic"; %>
<% String sDataTitle = "Total Hour of Working"; %>
<% String sPageName = "Stats.jsp"; %>
<% String sBackPageName = "Stats.jsp"; %>
<% String sProcessPageName = "Stats.jsp"; %>

<%!TBUSER SessionUser;%>
<%!int iTotalShift1;%>
<%!int iTotalShift2;%>
<%!int iTotalShift3;%>
<%!int iTotalShiftA;%>
<%!String sCurrentError = "";%>
<%!String sDept = "";%>
<%!TBDEPARTMENT[] aDepartments;%>
<%!String cbDepartmentId;%>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	AttendanceMgr.setConnection(DBMgr.getConnection());
	UserMgr.setConnection(DBMgr.getConnection());
	DepartmentMgr.setConnection(DBMgr.getConnection());
	
	SessionUser = UserMgr.getUserById(SessionUserId);
	
	aDepartments = DepartmentMgr.getDepartmentAllNoOffset();
	String firstDept = "";
	if(SessionUser.getROLE().trim().equals("HOD")){
		String deptArr[] = SessionUser.getDEPARTMENTID().split(",");
		String dept = "";
		for(int i=0; i<deptArr.length; i++){
			if(dept != ""){
				dept = dept + ",";
			} else {
				firstDept = deptArr[i];
			}
			dept = dept + "'" + deptArr[i] + "'"; 
    	}
		aDepartments = DepartmentMgr.getDepartmentAllNoOffsetOnly(dept);
	}
	
	cbDepartmentId = request.getParameter("cbDepartmentId");
	
	if (cbDepartmentId == null || cbDepartmentId.trim().length() <= 0) {
		cbDepartmentId = "0";		
	}
	
	Calendar cal = Calendar.getInstance();
	Date TodayDate = cal.getTime();
	String yyyymm = util.DateFormatConverter.format(TodayDate, "yyyyMM");
	
	//yyyymm = "201812";
	
	/*if(SessionUser.getROLE().trim().equals("HOD")){
		String deptArr[] = SessionUser.getDEPARTMENTID().split(",");
		
		for(int i=0; i<deptArr.length; i++){
			if(sDept != ""){
				sDept = sDept + ",";
			}
			sDept = sDept + "'" + deptArr[i] + "'"; 
    	}	
	} else {
		sDept = "0";
	}*/

	/*iTotalShift1 = AttendanceMgr.countShift1ByDept(sDept, yyyymm);
	iTotalShift2 = AttendanceMgr.countShift2ByDept(sDept, yyyymm);
	iTotalShift3 = AttendanceMgr.countShift3ByDept(sDept, yyyymm);
	iTotalShiftA = AttendanceMgr.countShiftAbsentByDept(sDept, yyyymm);*/
	
	iTotalShift1 = AttendanceMgr.countShift1ByDept(cbDepartmentId, yyyymm);
	iTotalShift2 = AttendanceMgr.countShift2ByDept(cbDepartmentId, yyyymm);
	iTotalShift3 = AttendanceMgr.countShift3ByDept(cbDepartmentId, yyyymm);
	iTotalShiftA = AttendanceMgr.countShiftAbsentByDept(cbDepartmentId, yyyymm);
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed()) {
			DBMgr.closeConnection();
		}
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
	
$("#btnGenerate").click(function (){
   		var cbDepartmentId = $('#cbDepartmentId').val();
   		var targetHref="Stats.jsp?" +
   				"&cbDepartmentId=" + cbDepartmentId + "";
   		
   		window.location = targetHref;
     });
});
</script>
 
<!-- Javascript -->
<script type="text/javascript">
$(function () { 
    var data = [
        {label: "Less Than 8 AM", data: <%=iTotalShift1 %>},
        {label: "09:00 - 18:00", data: <%=iTotalShift2 %>},
        {label: "10:30 - 19:30", data: <%=iTotalShift3 %>},
        {label: "Absent", data: <%=iTotalShiftA %>}
    ];
 
    var options = {
    		series: {
    	        pie: {
    	            show: true,
    	            radius: 1,
    	            tilt: 0.5,
    	            combine: {
    	                color: '#000'
    	            },
    	            label: {
    	                show: true,
    	                radius: 3/4,
    	                background: { 
    	                    opacity: 0.5,
    	                    color: '#000'
    	                }
    	            }
    	        }
    	    },
    	    legend: {
    	        show: false
    	    }
    };
 
    $.plot($("#flotcontainer"), data, options);  
});
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                   
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
				
	             <div class="row">
		            <div class="col-md-12 col-sm-12 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                          
                          <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" >
                           		
								<div class="form-group">
                                    <label>Department</label>
                                    <select class="form-control" id ="cbDepartmentId" name ="cbDepartmentId">
                                    <% if(!SessionUser.getROLE().trim().equals("HOD")){  %>
										<option value = "">All Department</option>
									<% } %>
									<%if(aDepartments != null){ %>
										<%for (int i = 0; i < aDepartments.length; i++) { %>
											<option value = "<%=aDepartments[i].getDEPARTMENTID()%>" <%=cbDepartmentId.equals(aDepartments[i].getDEPARTMENTID().trim())?"selected":""%>><%=aDepartments[i].getDEPARTMENTNAME()%></option>
										<%} %>
									<%} %>
									</select>
                                </div>
								
                            </form>
                            <button class="btn btn-info" id="btnGenerate" />Generate</button>
                            <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                          
                          
  
<div id="legendPlaceholder"></div>
<div id="flotcontainer"></div>
	
	
	
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
