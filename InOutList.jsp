<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<link href="assets/css/jquery.timepicker.min.css" rel="stylesheet" />
<script src="assets/js/jquery.timepicker.min.js"></script>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.REPORT5"%>
<%@page import="util.DateFormatConverter"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>

<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<%
String CurrentParam = "?"+request.getQueryString();
request.getSession().setAttribute("CurrentParam", CurrentParam);
%>

<% String sTableTitle = "Attendance Management"; %>
<% String sPageName = "InOutList.jsp"; %>
<% String sAddPageName = "InOutList.jsp"; %>
<% String sEditPageName = "InOutList.jsp"; %>
<% String sDeletePageName = "processor/InOutList.jsp"; %>


<%!Biometrics.REPORT5[] aAttendances;%>
<%!String txtStaffId;%>
<%!String txtStaffName;%>
<%!String txtBranchId;%>
<%!String txtDeviceId;%>
<%!String sTotalRecord;%>
<%!String sOffset;%>
<%!String sReportType;%>
<%!int iPage;%>
<%!int iMaxPage;%>
<%!TBUSER SessionUser;%>
<%!String txtDate;%>
<%!String txtMonth;%>
<%!String txtYear;%>
<%!String txtStartTime;%>
<%!String txtEndTime;%>
<%!String sCurrentError = "";%>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}

	UserMgr.setConnection(DBMgr.getConnection());
	AttendanceMgr.setConnection(DBMgr.getConnection());
	
	SessionUser = UserMgr.getUserById(SessionUserId);
	/*txtStaffId = request.getParameter("txtStaffId");
	txtStaffName = request.getParameter("txtStaffName");
	txtBranchId = request.getParameter("txtBranchId");
	txtDeviceId = request.getParameter("txtDeviceId");
	sOffset = request.getParameter("txtOffset");
	sReportType = request.getParameter("comboReport");*/
	txtStaffId = "";
	txtStaffName = "";
	txtBranchId = "";
	txtDeviceId = "";
	sOffset = request.getParameter("txtOffset");
	sReportType = "";
	
	txtDate = request.getParameter("txtDate");
	txtMonth = request.getParameter("txtMonth");
	txtYear = request.getParameter("txtYear");
	/*txtStartTime = request.getParameter("txtStartTime");
	txtEndTime = request.getParameter("txtEndTime");*/
	txtStartTime = "";
	txtEndTime = "";
	
	Calendar cal = Calendar.getInstance();
	Date TodayDate = cal.getTime();
	
	if (txtStaffId == null || txtStaffId.trim().length() <= 0) {
		txtStaffId = "";		
	}
	if (txtStaffName == null || txtStaffName.trim().length() <= 0) {
		txtStaffName = "";		
	}
	if (txtBranchId == null || txtBranchId.trim().length() <= 0) {
		txtBranchId = "";		
	}
	if (txtDeviceId == null || txtDeviceId.trim().length() <= 0) {
		txtDeviceId = "";		
	}
	if (sOffset == null || sOffset.trim().length() <= 0) {
		sOffset = "0";		
	}
	if (sReportType == null || sReportType.trim().length() <= 0) {
		sReportType = "";		
	}
	if (txtDate == null || txtDate.trim().length() <= 0) {
		txtDate = util.DateFormatConverter.format(TodayDate, "dd"); 
	}
	if (txtMonth == null || txtMonth.trim().length() <= 0) {
		txtMonth = util.DateFormatConverter.format(TodayDate, "MM");
	}
	if (txtYear == null || txtYear.trim().length() <= 0) {
		txtYear = util.DateFormatConverter.format(TodayDate, "yyyy");
	}
	if (txtStartTime == null || txtStartTime.trim().length() <= 0) {
		txtStartTime = "";		
	}
	if (txtEndTime == null || txtEndTime.trim().length() <= 0) {
		txtEndTime = "";		
	}
	String cbDepartmentId = "";
	String cbDeviceId = "";
	String cbWorkingOption = "0";
	String cbShiftOption = "0";
	String Start = txtYear + txtMonth + txtDate;
	String End = txtYear + txtMonth + txtDate;
	if (txtStaffId != "" || txtBranchId != "" || txtDeviceId != "" || txtDate != "" || txtMonth != "" || txtYear != "") {
		aAttendances = AttendanceMgr.genReport5View(txtStaffId, txtStaffName, txtBranchId, cbDepartmentId, cbDeviceId, cbWorkingOption, cbShiftOption, Start, End, sOffset);
		sTotalRecord = AttendanceMgr.countReport5View(txtStaffId,txtStaffName, txtBranchId, cbDepartmentId, cbDeviceId, cbWorkingOption, cbShiftOption, Start, End) + "";
	} else {
		aAttendances = AttendanceMgr.genReport5View(txtStaffId, txtStaffName, txtBranchId, cbDepartmentId, cbDeviceId, cbWorkingOption, cbShiftOption, Start, End, sOffset);
		sTotalRecord = AttendanceMgr.countReport5View(txtStaffId,txtStaffName, txtBranchId, cbDepartmentId, cbDeviceId, cbWorkingOption, cbShiftOption, Start, End) + "";
	}
	
	iPage = (Integer.parseInt(sOffset) / 30) + 1;
	if(Integer.parseInt(sTotalRecord) % 30 != 0){
		iMaxPage = (Integer.parseInt(sTotalRecord) / 30) + 1;
	} else {
		iMaxPage = (Integer.parseInt(sTotalRecord) / 30);
	}
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
}
%>
<script language="javascript" type="text/javascript">
$(function() {
	$('#txtStartTime').timepicker({ 
		'timeFormat': 'His',
		'step': 5 
	});
	$('#txtEndTime').timepicker({ 
		'timeFormat': 'His',
		'step': 5
	});
    $(".pageNumber").click(function (){
    	var sNextOffset = (parseInt($(this).html()) - 1) * 30;
    	var TotalRecord = parseInt($("#TotalRecord").val());
    	var sAttendanceId = $('#txtAttendanceId').val();
    	//var sUserName = $('#txtAttendanceName').val();
		var sStaffId = $('#txtStaffId').val();
		var sStaffName = $('#txtStaffName').val();
		var sBranchId = $('#txtBranchId').val();
		var sDeviceId = $('#txtDeviceId').val();
		var sDate = $('#txtDate').val();
		var sMonth = $('#txtMonth').val();
		var sYear = $('#txtYear').val();
		var sStartTime = $('#txtStartTime').val();
		var sEndTime = $('#txtEndTime').val();
		HidePaging();
    	$(this).attr("href", "<%=sPageName %>?txtAttendanceId=" + sAttendanceId + "&txtStaffId=" + sStaffId + "&txtStaffName=" + sStaffName + "&txtBranchId=" + sBranchId + "&txtDeviceId=" + sDeviceId + "&txtDate=" + sDate + "&txtMonth=" + sMonth + "&txtYear=" + sYear + "&txtStartTime=" + sStartTime + "&txtEndTime=" + sEndTime + "&txtOffset=" + sNextOffset + "");
     });
    
 });
 
function HidePaging(){
	$(".pageNumber").css("display","none");	
	$(".Next").css("display","none");	
	$(".Prev").css("display","none");	
	$(".currentPage").css("display","none");
	$(".minPageNumber").css("display","none");
	$(".maxPageNumber").css("display","none");
}
 
function IsConfirmedDelete(obj)
{
	return confirm("Are you sure want to delete?");	
}
function IsConfirmedImport(obj)
{
	return confirm("Are you sure want to import?");	
}
function IsConfirmedSearch(obj)
{	
	$("#thisForm").attr("action", "<%=sPageName %>");	
	$("#txtOffset").val("0");
	$("#thisForm").submit();
	return true;
}

function Next(obj)
{	
	var nextLink = document.getElementById("Next");
	var prevLink = document.getElementById("Prev");
		
	var sOffset = parseInt($("#txtOffset").val());
	var TotalRecord = parseInt($("#TotalRecord").val());	
	if(sOffset+10 <=  TotalRecord){
		HidePaging();
		
		var sAttendanceId = $('#txtAttendanceId').val();
    	//var sUserName = $('#txtAttendanceName').val();
		var sStaffId = $('#txtStaffId').val();
		var sStaffName = $('#txtStaffName').val();
		var sBranchId = $('#txtBranchId').val();
		var sDeviceId = $('#txtDeviceId').val();
		var sDate = $('#txtDate').val();
		var sMonth = $('#txtMonth').val();
		var sYear = $('#txtYear').val();
		var sStartTime = $('#txtStartTime').val();
		var sEndTime = $('#txtEndTime').val();
		sOffset = sOffset + 30;

		var targetHref="<%=sPageName %>?txtAttendanceId=" + sAttendanceId + "&txtStaffId=" + sStaffId + "&txtStaffName=" + sStaffName + "&txtBranchId=" + sBranchId + "&txtDeviceId=" + sDeviceId + "&txtDate=" + sDate + "&txtMonth=" + sMonth + "&txtYear=" + sYear + "&txtStartTime=" + sStartTime + "&txtEndTime=" + sEndTime + "&txtOffset=" + sOffset + "";
		$(".Next").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Prev(obj)
{	
	var nextLink = document.getElementById("Next");
	var prevLink = document.getElementById("Prev");
	
	var sOffset = parseInt($("#txtOffset").val());
	if(sOffset > 0){	
		HidePaging();
		
		var sAttendanceId = $('#txtAttendanceId').val();
    	//var sUserName = $('#txtAttendanceName').val();
		var sStaffId = $('#txtStaffId').val();
		var sStaffName = $('#txtStaffName').val();
		var sBranchId = $('#txtBranchId').val();
		var sDeviceId = $('#txtDeviceId').val();	
		var sDate = $('#txtDate').val();
		var sMonth = $('#txtMonth').val();
		var sYear = $('#txtYear').val();
		var sStartTime = $('#txtStartTime').val();
		var sEndTime = $('#txtEndTime').val();
		sOffset = sOffset - 30;

		var targetHref="<%=sPageName %>?txtAttendanceId=" + sAttendanceId + "&txtStaffId=" + sStaffId + "&txtStaffName=" + sStaffName + "&txtBranchId=" + sBranchId + "&txtDeviceId=" + sDeviceId + "&txtDate=" + sDate + "&txtMonth=" + sMonth + "&txtYear=" + sYear + "&txtStartTime=" + sStartTime + "&txtEndTime=" + sEndTime + "&txtOffset=" + sOffset + "";
		$(".Prev").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Max(obj)
{
	var sOffset = (parseInt($("#MaxPage").val()) - 1) * 30;
	var TotalRecord = parseInt($("#TotalRecord").val());	
	HidePaging();		
	var sAttendanceId = $('#txtAttendanceId').val();
	//var sUserName = $('#txtAttendanceName').val();
	var sStaffId = $('#txtStaffId').val();
	var sStaffName = $('#txtStaffName').val();
	var sBranchId = $('#txtBranchId').val();
	var sDeviceId = $('#txtDeviceId').val();
	var sDate = $('#txtDate').val();
	var sMonth = $('#txtMonth').val();
	var sYear = $('#txtYear').val();
	var sStartTime = $('#txtStartTime').val();
	var sEndTime = $('#txtEndTime').val();
	var targetHref="<%=sPageName %>?txtAttendanceId=" + sAttendanceId + "&txtStaffId=" + sStaffId + "&txtStaffName=" + sStaffName + "&txtBranchId=" + sBranchId + "&txtDeviceId=" + sDeviceId + "&txtDate=" + sDate + "&txtMonth=" + sMonth + "&txtYear=" + sYear + "&txtStartTime=" + sStartTime + "&txtEndTime=" + sEndTime + "&txtOffset=" + sOffset + "";
	$(".maxPageNumber").attr("href", targetHref); 
	return true;
}

function Min(obj)
{
	var TotalRecord = parseInt($("#TotalRecord").val());	
	HidePaging();		
	var sAttendanceId = $('#txtAttendanceId').val();
	//var sUserName = $('#txtAttendanceName').val();
	var sStaffId = $('#txtStaffId').val();
	var sStaffName = $('#txtStaffName').val();
	var sBranchId = $('#txtBranchId').val();
	var sDeviceId = $('#txtDeviceId').val();	
	var sDate = $('#txtDate').val();
	var sMonth = $('#txtMonth').val();
	var sYear = $('#txtYear').val();
	var sStartTime = $('#txtStartTime').val();
	var sEndTime = $('#txtEndTime').val();
	var targetHref="<%=sPageName %>?txtAttendanceId=" + sAttendanceId + "&txtStaffId=" + sStaffId + "&txtStaffName=" + sStaffName + "&txtBranchId=" + sBranchId + "&txtDeviceId=" + sDeviceId + "&txtDate=" + sDate + "&txtMonth=" + sMonth + "&txtYear=" + sYear + "&txtStartTime=" + sStartTime + "&txtEndTime=" + sEndTime + "&txtOffset=0";
	$(".minPageNumber").attr("href", targetHref); 
	return true;
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
				
	            <div class="row">
	                <div class="col-md-12">
	                    <!--    Striped Rows Table  -->
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                        	<Form id = "thisForm">
	                        	<input type = "hidden" id = "TotalRecord" value = "<%=sTotalRecord%>">
								<input type = "hidden" id = "txtOffset" name="txtOffset"  value="<%= sOffset == null ? "0" : sOffset%>">
								<input type = "hidden" id = "MaxPage" value = "<%=iMaxPage%>">
			
	                            <div class="row">
                    				<div class="col-md-3">
                    					Date : 
		                            </div>
		                            <div class="col-md-6">
		                            	<div class="row">
                    						<div class="col-md-4">
                    							<div class="input-group">
													<input class="form-control" id="txtDate" type="text" maxlength="2" name="txtDate" size="2" onkeypress="return isNumberKey(event)" value="<%=txtDate %>" /> 
				                                </div>
                    						</div>
                    						<div class="col-md-4">
                    							<div class="input-group">
													<input class="form-control" id="txtMonth" type="text" maxlength="2" name="txtMonth" size="2" onkeypress="return isNumberKey(event)" value="<%=txtMonth %>" /> 
				                                </div>
                    						</div>
                    						<div class="col-md-4">
                    							<div class="input-group">
													<input class="form-control" id="txtYear" type="text" maxlength="4" name="txtYear" size="4" onkeypress="return isNumberKey(event)" value="<%=txtYear %>" /> 
				                                </div>
                    						</div>
                    					</div>
		                            </div>
		                            <div class="col-md-3">
			                            <div class="input-group">
		                                    <span class="input-group-btn">
		                                        <button class="btn btn-success" id = "btnSearch" type="button" onclick="return IsConfirmedSearch(this);">Search</button>
		                                    </span>
		                                </div>
		                            </div>
		                     	</div>
								<!-- <div class="row">
                    				<div class="col-md-3">
                    					Time (Start-End) : 
		                            </div>
		                            <div class="col-md-6">
		                            	<div class="row">
                    						<div class="col-md-6">
                    							<div class="input-group">
													<input class="form-control" id="txtStartTime" type="text" name="txtStartTime" size="8" placeholder="Start Time" value="<%=txtStartTime %>" /> 
				                                </div>
                    						</div>
                    						<div class="col-md-6">
                    							<div class="input-group">
													<input class="form-control" id="txtEndTime" type="text" name="txtEndTime" size="8" placeholder="End Time" value="<%=txtEndTime %>" /> 
				                                </div>
                    						</div>
                    					</div>
		                            </div>
		                     	</div>
		                     	<div class = "row">
		                     		<div class="col-md-3">
                    					Staff : 
		                            </div>
									<div class="col-md-3">
			                            <div class="input-group">
											<input class="form-control" id = "txtStaffId" name="txtStaffId" type="text" maxlength="20" size="7" placeholder="Staff Id" value="<%= txtStaffId == null ? "" : txtStaffId %>">
		                                </div>
		                            </div>
									<div class="col-md-3">
			                            <div class="input-group">
											<input class="form-control" id = "txtStaffName" name="txtStaffName" type="text" maxlength="20" size="7" placeholder="Staff Name" value="<%= txtStaffName == null ? "" : txtStaffName %>">
		                                </div>
		                            </div>
		                       	</div>
		                     	<div class = "row">
		                     		<div class="col-md-3">
                    					Branch & Device : 
		                            </div>
									<div class="col-md-3">
			                            <div class="input-group">
											<input class="form-control" id = "txtBranchId" name="txtBranchId" type="text" maxlength="20" size="7" placeholder="Branch Id" value="<%= txtBranchId == null ? "" : txtBranchId %>">
		                                </div>
		                            </div>
									<div class="col-md-3">
			                            <div class="input-group">
											<input class="form-control" id = "txtDeviceId" name="txtDeviceId" type="text" maxlength="20" size="7" placeholder="Device Id" value="<%= txtDeviceId == null ? "" : txtDeviceId %>">
		                                </div>
		                            </div>
		                            <div class="col-md-3">
			                            <div class="input-group">
		                                    <span class="input-group-btn">
		                                        <button class="btn btn-success" id = "btnSearch" type="button" onclick="return IsConfirmedSearch(this);">Search</button>
		                                    </span>
		                                </div>
		                            </div>
		                      	</div>-->
		                      	</Form>
	                        </div>
	                        <div class="panel-body">
	                            <div class="table-responsive">
	                                <table class="table table-striped">
	                                    <thead>
	                                        <tr>
	                                        	<th>#</th>
												<th>Staff Id</th>
												<th>Staff Name</th>
												<th>Branch</th>
												<th>Dept Name</th>
												<th>Date</th>
												<th>In</th>
												<th>Out</th>
												<th>Remark</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    <%
											if (aAttendances != null) {
														
												for (int i = 0; i < aAttendances.length; i++) {
										%>
	                                        <tr>
	                                        	<td><%=Integer.parseInt(sOffset)+i+1%></td>
												<td><%=aAttendances[i].getSTAFFID()%></td>
												<td><%=aAttendances[i].getSTAFFNAME()%></td>
												<td><%=aAttendances[i].getBRANCHID()%></td>
												<td><%=aAttendances[i].getDEPARTMENTNAME()%></td>
												<% 
													String sAttDate = util.DateFormatConverter.format3(aAttendances[i].getATTENDANCEDATE() + aAttendances[i].getCHECKIN(), "yyyyMMddHHmmss", "yyyy-MM-dd");
													String sAttTimeIn = util.DateFormatConverter.format3(aAttendances[i].getATTENDANCEDATE() + aAttendances[i].getCHECKIN(), "yyyyMMddHHmmss", "HH:mm:ss");
													String sAttTimeOut = util.DateFormatConverter.format3(aAttendances[i].getATTENDANCEDATE() + aAttendances[i].getCHECKOUT(), "yyyyMMddHHmmss", "HH:mm:ss");
												 %> 
												<td><%=sAttDate%></td>
												<td>
												<% if(aAttendances[i].getCHECKIN() == "000000") { %>
													-
												<% } else { %>
													<%=sAttTimeIn%>
												<% } %>
												</td>
												<td>
												<% if(aAttendances[i].getCHECKOUT() == "000000") { %>
													-
												<% } else { %>
													<%=sAttTimeOut%>
												<% } %>
												</td>
												<td>
												<% if(aAttendances[i].getDESCRIPTION() == null) { %>
												
												<% } else { %>
													<%=aAttendances[i].getDESCRIPTION()%>
												<% } %>
												</td>
	                                        </tr>
	                                    <%
												}
											}
										%> 
	                                    </tbody>
	                                </table>
	                                <hr />
                            		<ul class="pagination" style = "float:right;">
                            		<%if(Integer.parseInt(sTotalRecord) != 0) { %>
										<%if(iPage > 1){ %>
											<li><a href="<%=sPageName %>" class = "minPageNumber" onclick = "return Min(this);">First</a></li>
									  		<li><a href="<%=sPageName %>" class = "Prev" onclick="return Prev(this);">&laquo;</a></li>
										<% } %>
										<%if(iPage == 1){ %>
				
										<%} else if(iPage == 2){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber" >1</a></li>
										<%} else { %>
										
										<%if(iPage == iMaxPage){ %>
											<% if(iPage-4 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-4; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else if(iPage == iMaxPage - 1){ %>
											<% if(iPage-3 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-3; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else { %>
											<% if(iPage-2 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-2; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} %>	
									<%} %>
									<li class="active"><a class = "currentPage"><%=iPage %><span class="sr-only">(current)</span></a></li>
									<%if(iPage == iMaxPage){ %>
				
									<%} else if(iPage == iMaxPage - 1){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber" ><%=iMaxPage %></a></li>
									<%} else { %>
										<% if(iPage == 1){  %>
											<% if(iMaxPage < 5){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 5; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else if(iPage == 2){  %>
											<% if(iMaxPage <= 4){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 4; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else { %>
											<%for(int x = iPage + 1; x < iPage + 3; x++) { %>
												<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
											<% } %>
										<% } %>
									<%} %>		
									
									<%if(iPage < iMaxPage){ %>
											<li><a href="<%=sPageName %>" class = "Next" onclick = "return Next(this);">&raquo;</a></li>
									  		<li><a href="<%=sPageName %>" class = "maxPageNumber" onclick="return Max(this);">Last</a></li>
										<% } %>
									<%} %>
									</ul>
	                            
	                            	<!--<a class="btn btn-info" style = "float:left;"  href="<%=sAddPageName %>">Add New</a>-->
	                            </div>
	                            
	                            
	                            
	                        </div>
	                    </div>
	                    <!--  End  Striped Rows Table  -->
	                </div>
	                
	            </div>
                <!-- /. ROW  -->
           

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
	<%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
