package Biometrics;

import java.util.Date;

public class TBACCESSGROUPDETAIL {
	private int ID;
	private String ACCESSGROUPID;
	private String BRANCHID;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getACCESSGROUPID() {return ACCESSGROUPID;}
	public void setACCESSGROUPID(String ACCESSGROUPID) {this.ACCESSGROUPID = ACCESSGROUPID;}
	
	public String getBRANCHID() {return BRANCHID;}
	public void setBRANCHID(String BRANCHID) {this.BRANCHID = BRANCHID;}
	
}
