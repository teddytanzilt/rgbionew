package Biometrics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AccessGroupManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addAccessGroup(TBACCESSGROUP oAccessGroup) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBACCESSGROUP WHERE ACCESSGROUPID = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oAccessGroup.getACCESSGROUPID());
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "AccessGroup [" + oAccessGroup.getACCESSGROUPID()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBACCESSGROUP " + 
			"( ACCESSGROUPID , ACCESSGROUPNAME, RECORDSTATUS, CREATEDBY, CREATEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oAccessGroup.getACCESSGROUPID());
			pStmt.setString(2, oAccessGroup.getACCESSGROUPNAME());
			pStmt.setString(3, "A");
			pStmt.setString(4, oAccessGroup.getCREATEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateAccessGroup(TBACCESSGROUP oAccessGroup) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBACCESSGROUP WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oAccessGroup.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "AccessGroup [" + oAccessGroup.getACCESSGROUPID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBACCESSGROUP SET "
								+ "  ACCESSGROUPID = ? " 
								+ ", ACCESSGROUPNAME = ? "
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oAccessGroup.getACCESSGROUPID());
			pStmt.setString(2, oAccessGroup.getACCESSGROUPNAME());
			pStmt.setString(3, oAccessGroup.getLASTUPDATETIMEDBY());
			pStmt.setInt(4, oAccessGroup.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public boolean checkAccessGroupUsed(String ACCESSGROUPID) {
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT COUNT(*) AS COUNTED FROM TBSTAFF WHERE ACCESSGROUPID = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, ACCESSGROUPID);
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			int counted = 0;
			while (rsSelect.next()) {
				counted = rsSelect.getInt("COUNTED");
			}
			
			if (counted > 0) {
				return false;
			}

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public TBACCESSGROUP getAccessGroupById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBACCESSGROUP WHERE ID = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBACCESSGROUP> alAccessGroup;

		alAccessGroup = this.getAccessGroupData(pStmt);

		if (alAccessGroup.size() == 0)
			return null;

		return alAccessGroup.get(0);		
	}
	
	public TBACCESSGROUP getAccessGroupByAccessGroupId(String ACCESSGROUPID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBACCESSGROUP WHERE UPPER(ACCESSGROUPID) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, ACCESSGROUPID.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBACCESSGROUP> alAccessGroup;

		alAccessGroup = this.getAccessGroupData(pStmt);

		if (alAccessGroup.size() == 0)
			return null;

		return alAccessGroup.get(0);		
	}
	
	public TBACCESSGROUP getAccessGroupByAccessGroupName(String ACCESSGROUPNAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBACCESSGROUP WHERE UPPER(ACCESSGROUPNAME) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, ACCESSGROUPNAME.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBACCESSGROUP> alAccessGroup;

		alAccessGroup = this.getAccessGroupData(pStmt);

		if (alAccessGroup.size() == 0)
			return null;

		return alAccessGroup.get(0);		
	}
	
	public TBACCESSGROUP[] getAccessGroupAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBACCESSGROUP";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    		
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum " +
    					"FROM "+tableName+" WHERE RECORDSTATUS = ?) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBACCESSGROUP> alAccessGroup;

		alAccessGroup = this.getAccessGroupData(pStmt);

		if (alAccessGroup.size() == 0)
			return null;

		TBACCESSGROUP[] accessgroups = new TBACCESSGROUP[alAccessGroup.size()];

		alAccessGroup.toArray(accessgroups);

		return accessgroups;
	}
	
	public int countAccessGroupAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBACCESSGROUP WHERE RECORDSTATUS = ? ";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBACCESSGROUP[] getAccessGroupByFilter(String ACCESSGROUPID, String ACCESSGROUPNAME, String offset) throws SQLException {
    	String AccessGroupIdFilter = "1 = 1";
    	String AccessGroupNameFilter = "1 = 1";
    	String itemPerPage = "30";
    	if(ACCESSGROUPID != "" && ACCESSGROUPID.trim().length() > 0){
    		AccessGroupIdFilter = "UPPER(ACCESSGROUPID) = '" + ACCESSGROUPID.toUpperCase() + "'";	
    	}
    	if(ACCESSGROUPNAME != "" && ACCESSGROUPNAME.trim().length() > 0){
    		AccessGroupNameFilter = "UPPER(ACCESSGROUPNAME) LIKE ('%" + ACCESSGROUPNAME.toUpperCase() + "%')";	
    	} 
    	String tableName = "TBACCESSGROUP";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum FROM "+tableName+" " +
    					"WHERE " + AccessGroupIdFilter + " AND " + AccessGroupNameFilter + " AND RECORDSTATUS = ? ) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        
        ArrayList<TBACCESSGROUP> alAccessGroup = new ArrayList<TBACCESSGROUP>();
        alAccessGroup = this.getAccessGroupData(pStmt);
        TBACCESSGROUP[] accessgroups = new TBACCESSGROUP[alAccessGroup.size()];
		alAccessGroup.toArray(accessgroups);
		return accessgroups;
    }
	
	public int countAccessGroupByFilter(String ACCESSGROUPID, String ACCESSGROUPNAME) throws SQLException {
		String AccessGroupIdFilter = "1=1";
    	String AccessGroupNameFilter = "1=1";

    	if(ACCESSGROUPID != "" && ACCESSGROUPID.trim().length() > 0){
    		AccessGroupIdFilter = "UPPER(ACCESSGROUPID) = '" + ACCESSGROUPID.toUpperCase() + "'";	
    	}
    	if(ACCESSGROUPNAME != "" && ACCESSGROUPNAME.trim().length() > 0){
    		AccessGroupNameFilter = "UPPER(ACCESSGROUPNAME) LIKE ('%" + ACCESSGROUPNAME.toUpperCase() + "%')";	
    	} 
    	
        String sqlStmt = "SELECT * FROM TBACCESSGROUP " + 
        				"WHERE " +  AccessGroupIdFilter + " AND " + AccessGroupNameFilter + " " +
        				"AND RECORDSTATUS = ? ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteAccessGroup(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBACCESSGROUP WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBACCESSGROUP SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);

			pStmt.setInt(1, ID);
			
			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete accessgroup master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public TBACCESSGROUP[] getAccessGroupAllNoOffset() throws SQLException {
    	String tableName = "TBACCESSGROUP";
    	String sqlStmt = "SELECT * FROM "+tableName+" WHERE RECORDSTATUS = ? ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBACCESSGROUP> alAccessGroup;

		alAccessGroup = this.getAccessGroupData(pStmt);

		if (alAccessGroup.size() == 0)
			return null;

		TBACCESSGROUP[] accessgroups = new TBACCESSGROUP[alAccessGroup.size()];

		alAccessGroup.toArray(accessgroups);

		return accessgroups;
	}

	public ArrayList<TBACCESSGROUP> getAccessGroupData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBACCESSGROUP> alAccessGroup = new ArrayList<TBACCESSGROUP>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBACCESSGROUP oAccessGroup = new TBACCESSGROUP();
			oAccessGroup.setID(srs.getInt("ID"));
			oAccessGroup.setACCESSGROUPID(srs.getString("ACCESSGROUPID"));
			oAccessGroup.setACCESSGROUPNAME(srs.getString("ACCESSGROUPNAME"));
			oAccessGroup.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oAccessGroup.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oAccessGroup.setCREATEDBY(srs.getString("CREATEDBY"));
			oAccessGroup.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oAccessGroup.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alAccessGroup.add(oAccessGroup);
		}

		return alAccessGroup;
	}
	
	public String[] getAccessGroupDetailBranch(String ACCESSGROUPID) throws SQLException {
    	String tableName = "TBACCESSGROUPDETAIL";
    	String sqlStmt = "SELECT * FROM "+tableName+" WHERE ACCESSGROUPID = ? ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, ACCESSGROUPID);
    	
		ArrayList<TBACCESSGROUPDETAIL> alAccessGroupDetail;

		alAccessGroupDetail = this.getAccessGroupDetailData(pStmt);

		if (alAccessGroupDetail.size() == 0)
			return null;

		String[] accessgroupdetails = new String[alAccessGroupDetail.size()];
		
		for (int i = 0; i < alAccessGroupDetail.size(); i++) {
			accessgroupdetails[i] = alAccessGroupDetail.get(i).getBRANCHID();
		}  
		
		return accessgroupdetails;
	}
	
	public boolean addAccessGroupDetail(TBACCESSGROUPDETAIL oAccessGroupDetail) {
		try {
			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBACCESSGROUPDETAIL WHERE ACCESSGROUPID = ? AND BRANCHID = ? ";
			PreparedStatement pSelectStmt = connection.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oAccessGroupDetail.getACCESSGROUPID());
			pSelectStmt.setString(2, oAccessGroupDetail.getBRANCHID());
			
			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Data has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBACCESSGROUPDETAIL " + 
			"( ACCESSGROUPID , BRANCHID) " + 
			"VALUES (?, ?) ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oAccessGroupDetail.getACCESSGROUPID());
			pStmt.setString(2, oAccessGroupDetail.getBRANCHID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean deleteAccessGroupDetail(TBACCESSGROUPDETAIL oAccessGroupDetail) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBACCESSGROUPDETAIL WHERE ACCESSGROUPID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oAccessGroupDetail.getACCESSGROUPID());

			int iRet = pStmt.executeUpdate();

			if (iRet < 1) {
				connection.rollback();
				errorMessage = "Failed to delete accessgroup master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public ArrayList<TBACCESSGROUPDETAIL> getAccessGroupDetailData(PreparedStatement pStmt) throws SQLException {
		
		ArrayList<TBACCESSGROUPDETAIL> alAccessGroupDetail = new ArrayList<TBACCESSGROUPDETAIL>();
		
		ResultSet srs = pStmt.executeQuery();
		
		while (srs.next()) {
			TBACCESSGROUPDETAIL oAccessGroupDetail = new TBACCESSGROUPDETAIL();
			oAccessGroupDetail.setID(srs.getInt("ID"));
			oAccessGroupDetail.setACCESSGROUPID(srs.getString("ACCESSGROUPID"));
			oAccessGroupDetail.setBRANCHID(srs.getString("BRANCHID"));
			alAccessGroupDetail.add(oAccessGroupDetail);
		}
		
		return alAccessGroupDetail;
	}
}
