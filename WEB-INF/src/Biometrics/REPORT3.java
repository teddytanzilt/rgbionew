package Biometrics;

import java.util.Date;

public class REPORT3 {
	private String DEVICEID;
	private String DEVICENAME;
	private String EVENTDATE;
	private String EVENTTIME;
	private String STATUS;
	
	public String getDEVICEID() {return DEVICEID;}
	public void setDEVICEID(String DEVICEID) {this.DEVICEID = DEVICEID;}
	
	public String getDEVICENAME() {return DEVICENAME;}
	public void setDEVICENAME(String DEVICENAME) {this.DEVICENAME = DEVICENAME;}

	public String getEVENTDATE() {return EVENTDATE;}
	public void setEVENTDATE(String EVENTDATE) {this.EVENTDATE = EVENTDATE;}
	
	public String getEVENTTIME() {return EVENTTIME;}
	public void setEVENTTIME(String EVENTTIME) {this.EVENTTIME = EVENTTIME;}

	public String getSTATUS() {return STATUS;}
	public void setSTATUS(String STATUS) {this.STATUS = STATUS;}
	
}
