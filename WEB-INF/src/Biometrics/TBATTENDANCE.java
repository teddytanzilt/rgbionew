package Biometrics;

import java.util.Date;

public class TBATTENDANCE {
	private int ID;
	private String STAFFID;
	private String DEVICEID;
	private String BRANCHID;
	private String INOUT;
	private String ATTENDANCEDATE;
	private String ATTENDANCETIME;
	private String REASONID;
	private String DESCRIPTION;
	
	private String STAFFNAME;
	private String REASONNAME;
	
	private String LASTTIME;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getSTAFFID() {return STAFFID;}
	public void setSTAFFID(String STAFFID) {this.STAFFID = STAFFID;}
	
	public String getDEVICEID() {return DEVICEID;}
	public void setDEVICEID(String DEVICEID) {this.DEVICEID = DEVICEID;}
	
	public String getBRANCHID() {return BRANCHID;}
	public void setBRANCHID(String BRANCHID) {this.BRANCHID = BRANCHID;}
	
	public String getINOUT() {return INOUT;}
	public void setINOUT(String INOUT) {this.INOUT = INOUT;}
	
	public String getATTENDANCEDATE() {return ATTENDANCEDATE;}
	public void setATTENDANCEDATE(String ATTENDANCEDATE) {this.ATTENDANCEDATE = ATTENDANCEDATE;}
	
	public String getATTENDANCETIME() {return ATTENDANCETIME;}
	public void setATTENDANCETIME(String ATTENDANCETIME) {this.ATTENDANCETIME = ATTENDANCETIME;}
	
	public String getREASONID() {return REASONID;}
	public void setREASONID(String REASONID) {this.REASONID = REASONID;}
	
	public String getDESCRIPTION() {return DESCRIPTION;}
	public void setDESCRIPTION(String DESCRIPTION) {this.DESCRIPTION = DESCRIPTION;}
	
	public String getSTAFFNAME() {return STAFFNAME;}
	public void setSTAFFNAME(String STAFFNAME) {this.STAFFNAME = STAFFNAME;}
	
	public String getREASONNAME() {return REASONNAME;}
	public void setREASONNAME(String REASONNAME) {this.REASONNAME = REASONNAME;}
	
	public void setLASTTIME(String lASTTIME) {
		LASTTIME = lASTTIME;
	}
	public String getLASTTIME() {
		return LASTTIME;
	}
}
