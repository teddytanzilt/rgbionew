package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class SettingManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addSetting(TBSETTING oSetting) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBSETTING WHERE SETTINGID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oSetting.getSETTINGID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Setting [" + oSetting.getSETTINGID()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBSETTING " + 
			"( SETTINGID , SETTINGNAME, SETTINGVALUE, LASTUPDATETIMEDBY, LASTUPDATETIMEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oSetting.getSETTINGID());
			pStmt.setString(2, oSetting.getSETTINGNAME());
			pStmt.setString(3, oSetting.getSETTINGVALUE());
			pStmt.setString(4, oSetting.getLASTUPDATETIMEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateSetting(TBSETTING oSetting) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBSETTING WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oSetting.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Setting [" + oSetting.getSETTINGID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBSETTING SET "
								+ "  SETTINGID = ? " 
								+ ", SETTINGNAME = ? "
								+ ", SETTINGVALUE = ? "
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oSetting.getSETTINGID());
			pStmt.setString(2, oSetting.getSETTINGNAME());
			pStmt.setString(3, oSetting.getSETTINGVALUE());
			pStmt.setString(4, oSetting.getLASTUPDATETIMEDBY());
			pStmt.setInt(5, oSetting.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public TBSETTING getSettingById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBSETTING WHERE ID = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		
		ArrayList<TBSETTING> alSetting;

		alSetting = this.getSettingData(pStmt);

		if (alSetting.size() == 0)
			return null;

		return alSetting.get(0);		
	}
	
	public TBSETTING getSettingBySettingId(String SETTINGID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBSETTING WHERE UPPER(SETTINGID) = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, SETTINGID.toUpperCase());
		
		ArrayList<TBSETTING> alSetting;

		alSetting = this.getSettingData(pStmt);

		if (alSetting.size() == 0)
			return null;

		return alSetting.get(0);		
	}
	
	public TBSETTING getSettingBySettingName(String SETTINGNAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBSETTING WHERE UPPER(SETTINGNAME) = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, SETTINGNAME.toUpperCase());
		
		ArrayList<TBSETTING> alSetting;

		alSetting = this.getSettingData(pStmt);

		if (alSetting.size() == 0)
			return null;

		return alSetting.get(0);		
	}
	
	public TBSETTING[] getSettingAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBSETTING";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    		
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum " +
    					"FROM "+tableName+") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	
		ArrayList<TBSETTING> alSetting;

		alSetting = this.getSettingData(pStmt);

		if (alSetting.size() == 0)
			return null;

		TBSETTING[] settings = new TBSETTING[alSetting.size()];

		alSetting.toArray(settings);

		return settings;
	}
	
	public int countSettingAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBSETTING";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBSETTING[] getSettingByFilter(String SETTINGID, String SETTINGNAME, String offset) throws SQLException {
    	String SettingIdFilter = "1 = 1";
    	String SettingNameFilter = "1 = 1";
    	String itemPerPage = "30";
    	if(SETTINGID != "" && SETTINGID.trim().length() > 0){
    		SettingIdFilter = "UPPER(SETTINGID) = '" + SETTINGID.toUpperCase() + "'";	
    	}
    	if(SETTINGNAME != "" && SETTINGNAME.trim().length() > 0){
    		SettingNameFilter = "UPPER(SETTINGNAME) LIKE ('%" + SETTINGNAME.toUpperCase() + "%')";	
    	} 
    	String tableName = "TBSETTING";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum FROM "+tableName+" " +
    					"WHERE " + SettingIdFilter + " AND " + SettingNameFilter + ") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        
        ArrayList<TBSETTING> alSetting = new ArrayList<TBSETTING>();
        alSetting = this.getSettingData(pStmt);
        TBSETTING[] settings = new TBSETTING[alSetting.size()];
		alSetting.toArray(settings);
		return settings;
    }
	
	public int countSettingByFilter(String SETTINGID, String SETTINGNAME) throws SQLException {
		String SettingIdFilter = "1=1";
    	String SettingNameFilter = "1=1";

    	if(SETTINGID != "" && SETTINGID.trim().length() > 0){
    		SettingIdFilter = "UPPER(SETTINGID) = '" + SETTINGID.toUpperCase() + "'";	
    	}
    	if(SETTINGNAME != "" && SETTINGNAME.trim().length() > 0){
    		SettingNameFilter = "UPPER(SETTINGNAME) LIKE ('%" + SETTINGNAME.toUpperCase() + "%')";	
    	} 
    	
        String sqlStmt = "SELECT * FROM TBSETTING " + 
        				"WHERE " +  SettingIdFilter + " AND " + SettingNameFilter + " ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteSetting(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBSETTING WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setInt(1, ID);

			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete setting master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public TBSETTING[] getSettingAllNoOffset() throws SQLException {
    	String tableName = "TBSETTING";
    	String sqlStmt = "SELECT * FROM "+tableName+" ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	
		ArrayList<TBSETTING> alSetting;

		alSetting = this.getSettingData(pStmt);

		if (alSetting.size() == 0)
			return null;

		TBSETTING[] settings = new TBSETTING[alSetting.size()];

		alSetting.toArray(settings);

		return settings;
	}

	public ArrayList<TBSETTING> getSettingData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBSETTING> alSetting = new ArrayList<TBSETTING>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBSETTING oSetting = new TBSETTING();
			oSetting.setID(srs.getInt("ID"));
			oSetting.setSETTINGID(srs.getString("SETTINGID"));
			oSetting.setSETTINGNAME(srs.getString("SETTINGNAME"));
			oSetting.setSETTINGVALUE(srs.getString("SETTINGVALUE"));
			oSetting.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oSetting.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alSetting.add(oSetting);
		}

		return alSetting;
	}
}
