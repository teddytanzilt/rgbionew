package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.org.apache.bcel.internal.generic.NEW;
//import sun.org.mozilla.javascript.internal.regexp.SubString;

import util.DateFormatConverter;
import util.HeaderFooterPageEvent;

public class AttendanceManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public boolean updateAttendance(TBATTENDANCE oAttendance) {
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBATTENDANCE WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oAttendance.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Attendance [" + oAttendance.getID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBATTENDANCE SET REASONID = ?, DESCRIPTION = ? " 
					+ "WHERE ID = ? ";
			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oAttendance.getREASONID());
			pStmt.setString(2, oAttendance.getDESCRIPTION());
			pStmt.setInt(3, oAttendance.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public boolean updateAbsent(TBATTENDANCE oAttendance) {
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBATTENDANCE WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oAttendance.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Attendance [" + oAttendance.getID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBATTENDANCE SET REASONID = ?, " 
					+ "DEVICEID = ?, "
					+ "BRANCHID = ?, "
					+ "DESCRIPTION = ? "
					+ "WHERE ID = ? ";
			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oAttendance.getREASONID());
			pStmt.setString(2, oAttendance.getDEVICEID());
			pStmt.setString(3, oAttendance.getBRANCHID());
			pStmt.setString(4, oAttendance.getDESCRIPTION());
			pStmt.setInt(5, oAttendance.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}

	public TBATTENDANCE getAttendanceById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE ID = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}
	
	public TBATTENDANCE getAttendanceByStaffId(String STAFFID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE UPPER(STAFFID) = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, STAFFID.toUpperCase());
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}
	
	public TBATTENDANCE getAttendanceByBranchId(String BRANCHID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE UPPER(BRANCHID) = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, BRANCHID.toUpperCase());
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}
	
	public TBATTENDANCE getAttendanceByDeviceId(String DEVICEID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE UPPER(DEVICEID) = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, DEVICEID.toUpperCase());
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}	
	
	public TBATTENDANCE getAttendanceEditByStaffIdDate(String STAFFID, String ATTENDANCEDATE) throws SQLException {			
		String sqlStmt = "SELECT TOP 1 * FROM TBATTENDANCE WHERE UPPER(STAFFID) = ? AND ATTENDANCEDATE = ? ORDER BY ID DESC ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, STAFFID.toUpperCase());
		pStmt.setString(2, ATTENDANCEDATE.toUpperCase());
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}	
	
	public TBATTENDANCE[] getAttendanceAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBATTENDANCE";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    		
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select "+tableName+".*, TBSTAFF.STAFFNAME, ROW_NUMBER() over (order by "+tableName+".ID) as RowNum " +
    					"FROM "+tableName+" " +
    					"LEFT JOIN TBSTAFF ON ("+tableName+".STAFFID = TBSTAFF.STAFFID) " +
    					/*"WHERE "+tableName+".INOUT <> 'C' " +*/ 
    					") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceComplexData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		TBATTENDANCE[] areas = new TBATTENDANCE[alAttendance.size()];

		alAttendance.toArray(areas);

		return areas;
	}
	
	public int countAttendanceAll() throws SQLException {
    	//String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE INOUT <> 'C' ";
		String sqlStmt = "SELECT * FROM TBATTENDANCE ";
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBATTENDANCE[] getAttendanceByFilter(String STAFFID, String STAFFNAME, String BRANCHID, String DEVICEID, String choosenDate, String startTime, String endTime, String INOUT, String offset) throws SQLException {
    	String StaffIdFilter = "1 = 1";
    	String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String DateFilter = "1 = 1";
    	String StartTimeFilter = "1 = 1";
    	String EndTimeFilter = "1 = 1";
    	String InOutFilter = "1 = 1";
    	
    	String itemPerPage = "30";
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE ('%" + STAFFNAME.toUpperCase() + "%')";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(DEVICEID) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	} 
    	if(choosenDate != "" && choosenDate.trim().length() > 0){
    		DateFilter = "ATTENDANCEDATE = " + choosenDate + "";
    	}
    	if(startTime != "" && startTime.trim().length() > 0){
    		StartTimeFilter = "ATTENDANCETIME >= " + startTime + "";
    	}
    	if(endTime != "" && endTime.trim().length() > 0){
    		EndTimeFilter = "ATTENDANCETIME <= " + endTime + "";
    	}
    	if(Integer.parseInt(INOUT) > 0 && INOUT.trim().length() > 0){
    		if(Integer.parseInt(INOUT) == 1){
    			InOutFilter = "INOUT <> 'C'";
    		} else if(Integer.parseInt(INOUT) == 2){
    			InOutFilter = "INOUT = 'C'";
    		}
    	}
    	
    	String tableName = "TBATTENDANCE";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	/*String sqlStmt = "SELECT * FROM " + 
    					"(Select "+tableName+".*, TBSTAFF.STAFFNAME, TBREASON.REASONNAME, ROW_NUMBER() over (order by "+tableName+".ID) as RowNum FROM "+tableName+" " +
    					"LEFT JOIN TBSTAFF ON ("+tableName+".STAFFID = TBSTAFF.STAFFID) " +
    					"LEFT JOIN TBREASON ON ("+tableName+".REASONID = TBREASON.REASONID) " +
    					"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + DateFilter + " AND " + StartTimeFilter + " AND " + EndTimeFilter+ " AND " + InOutFilter + ") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";*/

    	//"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + DateFilter + " AND "+tableName+".INOUT <> 'C') TableName " +
    	
    	String sqlStmt = "SELECT * FROM " + 
						"(Select "+tableName+".*, TBSTAFF.STAFFNAME, TBREASON.REASONNAME, LASTCLOCK.LASTTIME, ROW_NUMBER() over (order by "+tableName+".ID) as RowNum FROM "+tableName+" " +
						"LEFT JOIN TBSTAFF ON ("+tableName+".STAFFID = TBSTAFF.STAFFID) " +
						"LEFT JOIN TBREASON ON ("+tableName+".REASONID = TBREASON.REASONID) " +
						"LEFT JOIN(SELECT * FROM ( " +
								"SELECT ROW_NUMBER() over (PARTITION BY [TBATTENDANCE].ATTENDANCEDATE, [TBATTENDANCE].STAFFID order by [TBATTENDANCE].ATTENDANCETIME DESC) as RowNum," + 
								"[TBATTENDANCE].ATTENDANCEDATE AS LASTDATE, [TBATTENDANCE].STAFFID AS LASTSTAFF , " +
								"[TBATTENDANCE].ATTENDANCETIME AS LASTTIME " +
								"FROM [TBATTENDANCE] " +
								") LASTCLOCK " +
								"WHERE LASTCLOCK.RowNum = 1) LASTCLOCK ON (LASTCLOCK.LASTDATE = TBATTENDANCE.ATTENDANCEDATE AND LASTCLOCK.LASTTIME = TBATTENDANCE.ATTENDANCETIME AND LASTCLOCK.LASTSTAFF = TBATTENDANCE.STAFFID )" +
						"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + DateFilter + " AND " + StartTimeFilter + " AND " + EndTimeFilter+ " AND " + InOutFilter + ") TableName " + 
						"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
					
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        
        ArrayList<TBATTENDANCE> alAttendance = new ArrayList<TBATTENDANCE>();
        alAttendance = this.getAttendanceComplexData2(pStmt);
        TBATTENDANCE[] areas = new TBATTENDANCE[alAttendance.size()];
		alAttendance.toArray(areas);
		return areas;
    }
	
	public int countAttendanceByFilter(String STAFFID, String STAFFNAME, String BRANCHID, String DEVICEID, String choosenDate,String startTime, String endTime, String INOUT) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String DateFilter = "1 = 1";
    	String StartTimeFilter = "1 = 1";
    	String EndTimeFilter = "1 = 1";
    	String InOutFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE ('%" + STAFFNAME.toUpperCase() + "%')";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(DEVICEID) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	} 
    	if(choosenDate != "" && choosenDate.trim().length() > 0){
    		DateFilter = "ATTENDANCEDATE = " + choosenDate + "";
    	}
    	if(startTime != "" && startTime.trim().length() > 0){
    		StartTimeFilter = "ATTENDANCETIME >= " + startTime + "";
    	}
    	if(endTime != "" && endTime.trim().length() > 0){
    		EndTimeFilter = "ATTENDANCETIME <= " + endTime + "";
    	}
    	if(Integer.parseInt(INOUT) > 0 && INOUT.trim().length() > 0){
    		if(Integer.parseInt(INOUT) == 1){
    			InOutFilter = "INOUT <> 'C'";
    		} else if(Integer.parseInt(INOUT) == 2){
    			InOutFilter = "INOUT = 'C'";
    		}
    	}
    	
    	String tableName = "TBATTENDANCE";
        String sqlStmt = "Select "+tableName+".*, TBSTAFF.STAFFNAME, TBREASON.REASONNAME, ROW_NUMBER() over (order by "+tableName+".ID) as RowNum FROM "+tableName+" " +
    					"LEFT JOIN TBSTAFF ON ("+tableName+".STAFFID = TBSTAFF.STAFFID) " +
    					"LEFT JOIN TBREASON ON ("+tableName+".REASONID = TBREASON.REASONID) " +
    					"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + DateFilter + " AND " + StartTimeFilter + " AND " + EndTimeFilter + " AND " + InOutFilter + "";
    					//"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + DateFilter + " AND "+tableName+".INOUT <> 'C'";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public TBATTENDANCE[] getAbsentByFilter(String STAFFID, String STAFFNAME, String BRANCHID, String DEVICEID, String choosenDate, String offset) throws SQLException {
    	String StaffIdFilter = "1 = 1";
    	String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String DateFilter = "1 = 1";
    	
    	String itemPerPage = "30";
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE ('%" + STAFFNAME.toUpperCase() + "%')";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(DEVICEID) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	} 
    	if(choosenDate != "" && choosenDate.trim().length() > 0){
    		DateFilter = "ATTENDANCEDATE = " + choosenDate + "";
    	} 
    	
    	String tableName = "TBATTENDANCE";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select "+tableName+".*, TBSTAFF.STAFFNAME, TBREASON.REASONNAME, ROW_NUMBER() over (order by "+tableName+".ID) as RowNum FROM "+tableName+" " +
    					"LEFT JOIN TBSTAFF ON ("+tableName+".STAFFID = TBSTAFF.STAFFID) " +
    					"LEFT JOIN TBREASON ON ("+tableName+".REASONID = TBREASON.REASONID) " +
    					"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + DateFilter + " AND "+tableName+".INOUT = 'C') TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";

        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        
        ArrayList<TBATTENDANCE> alAttendance = new ArrayList<TBATTENDANCE>();
        alAttendance = this.getAttendanceComplexData(pStmt);
        TBATTENDANCE[] areas = new TBATTENDANCE[alAttendance.size()];
		alAttendance.toArray(areas);
		return areas;
    }
	
	public int countAbsentByFilter(String STAFFID, String STAFFNAME, String BRANCHID, String DEVICEID, String choosenDate) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String DateFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE ('%" + STAFFNAME.toUpperCase() + "%')";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(DEVICEID) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	} 
    	if(choosenDate != "" && choosenDate.trim().length() > 0){
    		DateFilter = "ATTENDANCEDATE = " + choosenDate + "";
    	} 
    	
    	String tableName = "TBATTENDANCE";
        String sqlStmt = "Select "+tableName+".*, TBSTAFF.STAFFNAME, TBREASON.REASONNAME, ROW_NUMBER() over (order by "+tableName+".ID) as RowNum FROM "+tableName+" " +
    					"LEFT JOIN TBSTAFF ON ("+tableName+".STAFFID = TBSTAFF.STAFFID) " +
    					"LEFT JOIN TBREASON ON ("+tableName+".REASONID = TBREASON.REASONID) " +
    					"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + DateFilter + " AND "+tableName+".INOUT = 'C' ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public TBATTENDANCE[] getAbsentAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBATTENDANCE";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    		
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select "+tableName+".*, TBSTAFF.STAFFNAME, ROW_NUMBER() over (order by "+tableName+".ID) as RowNum " +
    					"FROM "+tableName+" " +
    					"LEFT JOIN TBSTAFF ON ("+tableName+".STAFFID = TBSTAFF.STAFFID) " +
    					"WHERE "+tableName+".INOUT = 'C' " +
    					") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceComplexData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		TBATTENDANCE[] areas = new TBATTENDANCE[alAttendance.size()];

		alAttendance.toArray(areas);

		return areas;
	}
	
	public int countAbsentAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE INOUT = 'C'";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public TBATTENDANCE[] getAttendanceAllNoOffset() throws SQLException {
    	String tableName = "TBATTENDANCE";
    	String sqlStmt = "SELECT * FROM "+tableName+" ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		TBATTENDANCE[] areas = new TBATTENDANCE[alAttendance.size()];

		alAttendance.toArray(areas);

		return areas;
	}

	public int countShift1ByDept(String DEPARTMENTID, String yyyymm) throws SQLException {
		/*String sqlStmt = "select * from v_attendance_single " +
						"LEFT JOIN TBSTAFF ON TBSTAFF.STAFFID = v_attendance_single.STAFFID " +
						"WHERE DEPARTMENTID = '" + DEPARTMENTID + "' " +
						"AND ATTENDANCEDATE LIKE '" + yyyymm + "%' " +
						"AND v_attendance_single.CLOCKINTIME >= '060000' AND v_attendance_single.CLOCKINTIME < '080000' AND v_attendance_single.ATTENDANCETYPE <> 'C' ";
		*/
		String sqlStmt = "select " + 
						"v_attendance_duration_single.STAFFID, v_attendance_duration_single.ATTENDANCEDATE, v_attendance_duration_single.WORKHOUR," + 
						"v_attendance_duration_single.CLOCKINTIME, v_attendance_duration_single.CLOCKOUTTIME, v_attendance_single.ATTENDANCETYPE " + 
						"from v_attendance_duration_single " + 
						"LEFT JOIN v_attendance_single ON v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE " +   
						"LEFT JOIN TBSTAFF ON TBSTAFF.STAFFID = v_attendance_duration_single.STAFFID ";
		if(DEPARTMENTID == "0" || DEPARTMENTID.equals("0")){
			sqlStmt = sqlStmt + "WHERE 1 = 1 ";
		} else {
			sqlStmt = sqlStmt + "WHERE TBSTAFF.DEPARTMENTID IN (" + DEPARTMENTID + ") ";
		}
		sqlStmt = sqlStmt + "AND v_attendance_duration_single.ATTENDANCEDATE LIKE '" + yyyymm + "%' " +  
						"AND v_attendance_duration_single.CLOCKINTIME >= '060000' AND v_attendance_duration_single.CLOCKINTIME < '080000' AND v_attendance_single.ATTENDANCETYPE <> 'C' "; 
				
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public int countShift2ByDept(String DEPARTMENTID, String yyyymm) throws SQLException {
		/*String sqlStmt = "select * from v_attendance_single " +
						"LEFT JOIN TBSTAFF ON TBSTAFF.STAFFID = v_attendance_single.STAFFID " +
						"WHERE DEPARTMENTID = '" + DEPARTMENTID + "' " +
						"AND ATTENDANCEDATE LIKE '" + yyyymm + "%' " +
						"AND v_attendance_single.CLOCKINTIME >= '080000' AND v_attendance_single.CLOCKINTIME < '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C' ";
		*/
		String sqlStmt = "select " + 
						"v_attendance_duration_single.STAFFID, v_attendance_duration_single.ATTENDANCEDATE, v_attendance_duration_single.WORKHOUR," + 
						"v_attendance_duration_single.CLOCKINTIME, v_attendance_duration_single.CLOCKOUTTIME, v_attendance_single.ATTENDANCETYPE " + 
						"from v_attendance_duration_single " + 
						"LEFT JOIN v_attendance_single ON v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE " +   
						"LEFT JOIN TBSTAFF ON TBSTAFF.STAFFID = v_attendance_duration_single.STAFFID "; 
		if(DEPARTMENTID == "0" || DEPARTMENTID.equals("0")){
			sqlStmt = sqlStmt + "WHERE 1 = 1 ";
		} else {
			sqlStmt = sqlStmt + "WHERE TBSTAFF.DEPARTMENTID IN (" + DEPARTMENTID + ") ";
		}
		sqlStmt = sqlStmt + "AND v_attendance_duration_single.ATTENDANCEDATE LIKE '" + yyyymm + "%' " +  
						"AND v_attendance_duration_single.CLOCKINTIME >= '080000' AND v_attendance_duration_single.CLOCKINTIME < '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C' "; 

        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public int countShift3ByDept(String DEPARTMENTID, String yyyymm) throws SQLException {
		/*String sqlStmt = "select * from v_attendance_single " +
						"LEFT JOIN TBSTAFF ON TBSTAFF.STAFFID = v_attendance_single.STAFFID " +
						"WHERE DEPARTMENTID = '" + DEPARTMENTID + "' " +
						"AND ATTENDANCEDATE LIKE '" + yyyymm + "%' " +
						"AND v_attendance_single.CLOCKINTIME >= '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C' ";
		*/
		String sqlStmt = "select " + 
						"v_attendance_duration_single.STAFFID, v_attendance_duration_single.ATTENDANCEDATE, v_attendance_duration_single.WORKHOUR," + 
						"v_attendance_duration_single.CLOCKINTIME, v_attendance_duration_single.CLOCKOUTTIME, v_attendance_single.ATTENDANCETYPE " + 
						"from v_attendance_duration_single " + 
						"LEFT JOIN v_attendance_single ON v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE " +   
						"LEFT JOIN TBSTAFF ON TBSTAFF.STAFFID = v_attendance_duration_single.STAFFID "; 
		if(DEPARTMENTID == "0" || DEPARTMENTID.equals("0")){
			sqlStmt = sqlStmt + "WHERE 1 = 1 ";
		} else {
			sqlStmt = sqlStmt + "WHERE TBSTAFF.DEPARTMENTID IN (" + DEPARTMENTID + ") ";
		}
		sqlStmt = sqlStmt + "AND v_attendance_duration_single.ATTENDANCEDATE LIKE '" + yyyymm + "%' " +  
						"AND v_attendance_duration_single.CLOCKINTIME >= '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C' ";
						
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public int countShiftAbsentByDept(String DEPARTMENTID, String yyyymm) throws SQLException {
		/*String sqlStmt = "select * from v_attendance_single " +
						"LEFT JOIN TBSTAFF ON TBSTAFF.STAFFID = v_attendance_single.STAFFID " +
						"WHERE DEPARTMENTID = '" + DEPARTMENTID + "' " +
						"AND ATTENDANCEDATE LIKE '" + yyyymm + "%' " +
						"AND v_attendance_single.ATTENDANCETYPE = 'C' ";
		*/
		String sqlStmt = "select " + 
						"v_attendance_duration_single.STAFFID, v_attendance_duration_single.ATTENDANCEDATE, v_attendance_duration_single.WORKHOUR," + 
						"v_attendance_duration_single.CLOCKINTIME, v_attendance_duration_single.CLOCKOUTTIME, v_attendance_single.ATTENDANCETYPE " + 
						"from v_attendance_duration_single " + 
						"LEFT JOIN v_attendance_single ON v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE " +   
						"LEFT JOIN TBSTAFF ON TBSTAFF.STAFFID = v_attendance_duration_single.STAFFID "; 
		
		if(DEPARTMENTID == "0" || DEPARTMENTID.equals("0")){
			sqlStmt = sqlStmt + "WHERE 1 = 1 ";
		} else {
			sqlStmt = sqlStmt + "WHERE TBSTAFF.DEPARTMENTID IN (" + DEPARTMENTID + ") ";
		}
		sqlStmt = sqlStmt + "AND v_attendance_duration_single.ATTENDANCEDATE LIKE '" + yyyymm + "%' " +  
				"AND v_attendance_single.ATTENDANCETYPE = 'C' ";
		
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public ArrayList<TBATTENDANCE> getAttendanceComplexData(PreparedStatement pStmt)
			throws SQLException {
		
		ArrayList<TBATTENDANCE> alAttendance = new ArrayList<TBATTENDANCE>();
		
		ResultSet srs = pStmt.executeQuery();
		
		while (srs.next()) {
			TBATTENDANCE oAttendance = new TBATTENDANCE();
			oAttendance.setID(srs.getInt("ID"));
			oAttendance.setDEVICEID(srs.getString("DEVICEID"));
			oAttendance.setBRANCHID(srs.getString("BRANCHID"));
			oAttendance.setSTAFFID(srs.getString("STAFFID"));
			oAttendance.setINOUT(srs.getString("INOUT"));
			oAttendance.setATTENDANCEDATE(srs.getString("ATTENDANCEDATE"));
			oAttendance.setATTENDANCETIME(srs.getString("ATTENDANCETIME"));
			oAttendance.setREASONID(srs.getString("REASONID"));
			oAttendance.setDESCRIPTION(srs.getString("DESCRIPTION"));
			oAttendance.setSTAFFNAME(srs.getString("STAFFNAME"));
			oAttendance.setREASONNAME(srs.getString("REASONNAME"));
			
			alAttendance.add(oAttendance);
		}
		
		return alAttendance;
	}
			
	public ArrayList<TBATTENDANCE> getAttendanceComplexData2(PreparedStatement pStmt)
		throws SQLException {
	
		ArrayList<TBATTENDANCE> alAttendance = new ArrayList<TBATTENDANCE>();
		
		ResultSet srs = pStmt.executeQuery();
		
		while (srs.next()) {
			TBATTENDANCE oAttendance = new TBATTENDANCE();
			oAttendance.setID(srs.getInt("ID"));
			oAttendance.setDEVICEID(srs.getString("DEVICEID"));
			oAttendance.setBRANCHID(srs.getString("BRANCHID"));
			oAttendance.setSTAFFID(srs.getString("STAFFID"));
			oAttendance.setINOUT(srs.getString("INOUT"));
			oAttendance.setATTENDANCEDATE(srs.getString("ATTENDANCEDATE"));
			oAttendance.setATTENDANCETIME(srs.getString("ATTENDANCETIME"));
			oAttendance.setREASONID(srs.getString("REASONID"));
			oAttendance.setDESCRIPTION(srs.getString("DESCRIPTION"));
			oAttendance.setSTAFFNAME(srs.getString("STAFFNAME"));
			oAttendance.setREASONNAME(srs.getString("REASONNAME"));
			oAttendance.setLASTTIME(srs.getString("LASTTIME"));
			
			alAttendance.add(oAttendance);
		}
		
		return alAttendance;
	}
		
	public ArrayList<TBATTENDANCE> getAttendanceData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBATTENDANCE> alAttendance = new ArrayList<TBATTENDANCE>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBATTENDANCE oAttendance = new TBATTENDANCE();
			oAttendance.setID(srs.getInt("ID"));
			oAttendance.setDEVICEID(srs.getString("DEVICEID"));
			oAttendance.setBRANCHID(srs.getString("BRANCHID"));
			oAttendance.setSTAFFID(srs.getString("STAFFID"));
			oAttendance.setINOUT(srs.getString("INOUT"));
			oAttendance.setATTENDANCEDATE(srs.getString("ATTENDANCEDATE"));
			oAttendance.setATTENDANCETIME(srs.getString("ATTENDANCETIME"));
			oAttendance.setREASONID(srs.getString("REASONID"));
			oAttendance.setDESCRIPTION(srs.getString("DESCRIPTION"));
			
			alAttendance.add(oAttendance);
		}

		return alAttendance;
	}
	
	public TBREPLTRANS[] getLogByFilter(String STAFFID, String BRANCHID, String DEVICEID, String Action, String LogDate, String offset) throws SQLException {
    	String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String ActionFilter = "1 = 1";
    	String DateFilter = "1 = 1";
    	
    	String itemPerPage = "30";
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBREPLTRANS.StaffId) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBREPLTRANS.DeviceId) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	}
    	if(Action != "" && Action.trim().length() > 0){
    		ActionFilter = "TBREPLTRANS.RType = '" + Action + "'";	
    	}  
    	if(LogDate != "" && LogDate.trim().length() > 0){
    		DateFilter = " CONVERT(VARCHAR, Logdate , 112) = '" + LogDate + "'";	
    	}
    	
    	String tableName = "TBREPLTRANS";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select TBREPLTRANS.*, TBDEVICE.BRANCHID, ROW_NUMBER() over (order by Logdate DESC) as RowNum FROM "+tableName+" " +
    					"LEFT JOIN TBDEVICE ON (TBDEVICE.DEVICEID = TBREPLTRANS.DeviceId) " +
    					"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + ActionFilter + " AND " + DateFilter + ") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        
        ArrayList<TBREPLTRANS> alAttendance = new ArrayList<TBREPLTRANS>();
        alAttendance = this.getLogData(pStmt);
        TBREPLTRANS[] areas = new TBREPLTRANS[alAttendance.size()];
		alAttendance.toArray(areas);
		return areas;
    }
	
	public int countLogByFilter(String STAFFID, String BRANCHID, String DEVICEID, String Action, String LogDate) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String ActionFilter = "1 = 1";
    	String DateFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBREPLTRANS.StaffId) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBREPLTRANS.DeviceId) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	} 
    	if(Action != "" && Action.trim().length() > 0){
    		ActionFilter = "TBREPLTRANS.RType = '" + Action + "'";	
    	}  
    	if(LogDate != "" && LogDate.trim().length() > 0){
    		DateFilter = " CONVERT(VARCHAR, Logdate , 112) = '" + LogDate + "'";
    	}
    	
        String sqlStmt = "SELECT TBREPLTRANS.*, TBDEVICE.BRANCHID FROM TBREPLTRANS " + 
        				"LEFT JOIN TBDEVICE ON (TBDEVICE.DEVICEID = TBREPLTRANS.DeviceId) " + 
        				"WHERE " +  StaffIdFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + ActionFilter + " AND " + DateFilter + " ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public ArrayList<TBREPLTRANS> getLogData(PreparedStatement pStmt) throws SQLException {
		
		ArrayList<TBREPLTRANS> alAttendance = new ArrayList<TBREPLTRANS>();
		
		ResultSet srs = pStmt.executeQuery();
		
		while (srs.next()) {
			TBREPLTRANS oAttendance = new TBREPLTRANS();
			oAttendance.setRecordId(srs.getInt("RecordId"));
			oAttendance.setDeviceId(srs.getString("DeviceId"));
			oAttendance.setStaffId(srs.getString("StaffId"));
			oAttendance.setFingerIndex(srs.getInt("FingerIndex"));
			oAttendance.setRType(srs.getString("RType"));
			oAttendance.setLogdate(srs.getString("Logdate"));
			oAttendance.setExtInfo(srs.getString("ExtInfo"));
			oAttendance.setBRANCHID(srs.getString("BRANCHID"));
			
			alAttendance.add(oAttendance);
		}
		
		return alAttendance;
	}
	
	public REPORT1[] genReport1View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END, String offset) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String itemPerPage = "30";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    			
    	} 
    	if(START != "" && START.trim().length() > 0){
    			
    	} 
    	if(END != "" && END.trim().length() > 0){
    			
    	} 
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
					"(SELECT ROW_NUMBER() over (order by TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,''), TBSTAFF.RECORDSTATUS) as RowNum, " + 
					"TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,'') AS THUMBCOUNT, TBSTAFF.RECORDSTATUS, TBACCESSGROUPDETAIL.BRANCHID " + 
					"FROM TBSTAFF " + 
					"LEFT JOIN (SELECT STAFFID, COUNT(*) THUMBCOUNT FROM TBSTAFFTHUMB GROUP BY STAFFID) THUMB ON (THUMB.STAFFID = TBSTAFF.STAFFID)" +
					"LEFT JOIN TBACCESSGROUPDETAIL ON (TBACCESSGROUPDETAIL.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID) " + 
					"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + ") TableName " + 
		"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";

    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);        
        ArrayList<REPORT1> alReport = new ArrayList<REPORT1>();
        ResultSet srs = pStmt.executeQuery();
        
		while (srs.next()) {
			REPORT1 oReport = new REPORT1();
			oReport.setSTAFFID(srs.getString("STAFFID"));
			oReport.setSTAFFNAME(srs.getString("STAFFNAME"));
			oReport.setREGISTERED(srs.getString("THUMBCOUNT"));
			oReport.setSTATUS(srs.getString("RECORDSTATUS"));
			alReport.add(oReport);
		}

		REPORT1[] reports = new REPORT1[alReport.size()];
		alReport.toArray(reports);
		return reports;
	}
	
	public int countReport1View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    			
    	} 
    	if(START != "" && START.trim().length() > 0){
    			
    	} 
    	if(END != "" && END.trim().length() > 0){
    			
    	} 
    	
        String sqlStmt = "SELECT " + 
					"TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,'') AS THUMBCOUNT, TBSTAFF.RECORDSTATUS, TBACCESSGROUPDETAIL.BRANCHID " + 
					"FROM TBSTAFF " + 
					"LEFT JOIN (SELECT STAFFID, COUNT(*) THUMBCOUNT FROM TBSTAFFTHUMB GROUP BY STAFFID) THUMB ON (THUMB.STAFFID = TBSTAFF.STAFFID)" +
					"LEFT JOIN TBACCESSGROUPDETAIL ON (TBACCESSGROUPDETAIL.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID) " + 
					"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + "";

        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public REPORT2[] genReport2View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END, String offset) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String itemPerPage = "30";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(DEVICEID) = '" + DEVICEID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "ATTENDANCEDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "ATTENDANCEDATE <= " + END + "";
    	} 
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
	    	"(SELECT " +
		    	"ROW_NUMBER() over (order by ATTENDANCEDATE,ATTENDANCETIME DESC) as RowNum , * " +
		    	"FROM " +
		    	"( " +
		    		"SELECT " +
		    		"TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " +
		    		"TBATTENDANCE.DEVICEID, TBDEVICE.DEVICENAME, " +
		    		"TBATTENDANCE.BRANCHID, TBBRANCH.BRANCHNAME, " +
		    		"TBSTAFF.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, " +
		    		"TBATTENDANCE.ATTENDANCEDATE, " +
		    		"TBATTENDANCE.ATTENDANCETIME, " +
		    		"TBATTENDANCE.INOUT, " +
		    		"TBATTENDANCE.DESCRIPTION, " +
		    		"TBREASON.REASONID, TBREASON.REASONNAME " +
		    		"FROM TBATTENDANCE " +
		    		"LEFT JOIN TBSTAFF ON (TBATTENDANCE.STAFFID = TBSTAFF.STAFFID AND TBSTAFF.RECORDSTATUS = 'A') " + 
		    		"LEFT JOIN TBDEVICE ON (TBATTENDANCE.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " + 
		    		"LEFT JOIN TBBRANCH ON (TBATTENDANCE.BRANCHID = TBBRANCH.BRANCHID AND TBBRANCH.RECORDSTATUS = 'A') " +
		    		"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID AND TBDEPARTMENT.RECORDSTATUS = 'A') " +
		    		"LEFT JOIN TBREASON ON (TBATTENDANCE.REASONID = TBREASON.REASONID AND TBREASON.RECORDSTATUS = 'A') " +
		    	") AS TheTables " +
		    	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " AND INOUT IN ('A','B') " + 
		    ") TableName " +			
		"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";

    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);        
        ArrayList<REPORT2> alReport = new ArrayList<REPORT2>();
        ResultSet srs = pStmt.executeQuery();
        
		while (srs.next()) {
			REPORT2 oReport = new REPORT2();
			
			oReport.setSTAFFID(srs.getString("STAFFID"));
			oReport.setSTAFFNAME(srs.getString("STAFFNAME"));
			oReport.setDEVICEID(srs.getString("DEVICEID"));
			oReport.setDEVICENAME(srs.getString("DEVICENAME"));
			oReport.setBRANCHID(srs.getString("BRANCHID"));
			oReport.setBRANCHNAME(srs.getString("BRANCHNAME"));
			oReport.setATTENDANCEDATE(srs.getString("ATTENDANCEDATE"));
			oReport.setATTENDANCETIME(srs.getString("ATTENDANCETIME"));
			oReport.setINOUT(srs.getString("INOUT"));
			oReport.setDESCRIPTION(srs.getString("DESCRIPTION"));
			oReport.setREASONID(srs.getString("REASONID"));
			oReport.setREASONNAME(srs.getString("REASONNAME"));
			alReport.add(oReport);
		}

		REPORT2[] reports = new REPORT2[alReport.size()];
		alReport.toArray(reports);
		return reports;
	}
	
	public int countReport2View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBATTENDANCE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBATTENDANCE.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "TBATTENDANCE.ATTENDANCEDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "TBATTENDANCE.ATTENDANCEDATE <= " + END + "";
    	}
    	
    	String sqlStmt = "SELECT " +
		    		"TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " +
		    		"TBATTENDANCE.DEVICEID, TBDEVICE.DEVICENAME, " +
		    		"TBATTENDANCE.BRANCHID, TBBRANCH.BRANCHNAME, " +
		    		"TBSTAFF.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, " +
		    		"TBATTENDANCE.ATTENDANCEDATE, " +
		    		"TBATTENDANCE.ATTENDANCETIME, " +
		    		"TBATTENDANCE.INOUT, " +
		    		"TBATTENDANCE.DESCRIPTION, " +
		    		"TBREASON.REASONNAME " +
		    		"FROM TBATTENDANCE " +
		    		"LEFT JOIN TBSTAFF ON (TBATTENDANCE.STAFFID = TBSTAFF.STAFFID AND TBSTAFF.RECORDSTATUS = 'A') " + 
		    		"LEFT JOIN TBDEVICE ON (TBATTENDANCE.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " + 
		    		"LEFT JOIN TBBRANCH ON (TBATTENDANCE.BRANCHID = TBBRANCH.BRANCHID AND TBBRANCH.RECORDSTATUS = 'A') " +
		    		"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID AND TBDEPARTMENT.RECORDSTATUS = 'A') " +
		    		"LEFT JOIN TBREASON ON (TBATTENDANCE.REASONID = TBREASON.REASONID AND TBREASON.RECORDSTATUS = 'A') " +
		    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " AND INOUT IN ('A','B') " + "";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public REPORT3[] genReport3View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END, String offset) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String itemPerPage = "30";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    			
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    			
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    			
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
			
    	}
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBDEVICEDOWNTIME.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "EVENTDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "EVENTDATE <= " + END + "";
    	} 
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
					"(SELECT " +
					"ROW_NUMBER() over (order by TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME DESC) as RowNum, " + 
					"TBDEVICEDOWNTIME.DEVICEID, TBDEVICE.DEVICENAME, " +
					"TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME, " + 
					"TBDEVICEDOWNTIME.STATUS " +
					"FROM TBDEVICEDOWNTIME " +
					"LEFT JOIN TBDEVICE ON (TBDEVICEDOWNTIME.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " + 
					"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + ") TableName " + 
		"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";

    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);        
        ArrayList<REPORT3> alReport = new ArrayList<REPORT3>();
        ResultSet srs = pStmt.executeQuery();
        
		while (srs.next()) {
			REPORT3 oReport = new REPORT3();
			oReport.setDEVICEID(srs.getString("DEVICEID"));
			oReport.setDEVICENAME(srs.getString("DEVICENAME"));
			oReport.setEVENTDATE(srs.getString("EVENTDATE"));
			oReport.setEVENTTIME(srs.getString("EVENTTIME"));
			oReport.setSTATUS(srs.getString("STATUS"));
			alReport.add(oReport);
		}

		REPORT3[] reports = new REPORT3[alReport.size()];
		alReport.toArray(reports);
		return reports;
	}
	
	public int countReport3View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    			
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    			
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
			
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBDEVICEDOWNTIME.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "TBDEVICEDOWNTIME.EVENTDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "TBDEVICEDOWNTIME.EVENTDATE <= " + START + "";
    	} 
    	
        String sqlStmt = "SELECT " +
					"TBDEVICEDOWNTIME.DEVICEID, TBDEVICE.DEVICENAME, " +
					"TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME, " + 
					"TBDEVICEDOWNTIME.STATUS " +
					"FROM TBDEVICEDOWNTIME " +
					"LEFT JOIN TBDEVICE ON (TBDEVICEDOWNTIME.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " + 
					"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + "";

        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public REPORT4[] genReport4View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END, String offset) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String itemPerPage = "30";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(v_attendance_single.STAFFID) = '" + STAFFID.toUpperCase() + "'";
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    			
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(TBDEPARTMENT.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    			
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "v_attendance_single.ATTENDANCEDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "v_attendance_single.ATTENDANCEDATE <= " + END + "";
    	}
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
/*
    	String sqlStmt = "" + 
"SELECT * " + 
"FROM ( " + 
	"SELECT  " + 
	"ROW_NUMBER() over (ORDER BY TABLE2.STAFFID, TABLE2.ATTENDANCEDATE) as RowNum, " + 
	"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +   
	"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " + 
		"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " + 
		"FROM ( " + 
			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +  
			"ID, TBATTENDANCE.STAFFID, " + 
			"CASE " +    
			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +    
			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +    
			"END AS DESCRIPTION, " + 
			"TBATTENDANCE.ATTENDANCEDATE, " + 
			"TBATTENDANCE.ATTENDANCETIME " + 
			"FROM TBATTENDANCE " + 
		") TABLE1 " + 
		"WHERE RowNum = 1 " + 
	") TABLE2 " + 
	"LEFT JOIN ( " +
		"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
		"FROM ( " +
			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
			"ID, TBATTENDANCE.STAFFID, " + 
			"CASE " +
			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
			"END AS DESCRIPTION, " +
			"TBATTENDANCE.ATTENDANCEDATE, " + 
			"TBATTENDANCE.ATTENDANCETIME " +
			"FROM TBATTENDANCE " +
		") TABLE1 " +
		"WHERE RowNum = 1 " +
	") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
	"LEFT JOIN( " + 
		"SELECT STAFFID , ATTENDANCEDATE, " +   
		"STUFF( " + 
			"(SELECT " +
			"CASE " +
				"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " + 
				"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " + 
			"END " +
			"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " + 
		")  DESCRIPTION " + 
		"FROM TBATTENDANCE b " + 
		"GROUP BY STAFFID, ATTENDANCEDATE " + 
	") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +  
	"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " + 
	"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " + 
    "WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
	"GROUP BY TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION " + 
") TABLE3 " + 
"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
*/
    	/*String sqlStmt = "" + 
    	"SELECT * FROM " +
    	"( " +
    		"SELECT " + 
    		"ROW_NUMBER() over (ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID, v_attendance_single.ATTENDANCEDATE) as RowNum, " + 
    		"v_attendance_single.STAFFID, " +
    		"TBSTAFF.STAFFNAME, " + 
    		"TBDEPARTMENT.DEPARTMENTID, " + 
    		"TBDEPARTMENT.DEPARTMENTNAME, " + 
    		"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    		"v_attendance_single.ATTENDANCEDATE, " +
    		"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    		"CASE v_attendance_single.ATTENDANCETYPE " + 
    			"WHEN 'C' THEN '000000' " +
    			"ELSE TABLE2.ATTENDANCETIME  " +
    		"END AS CHECKOUT, " +
    		"v_attendance_single.ATTENDANCETYPE " +
    		"FROM v_attendance_single " +
    		"LEFT JOIN ( " +
    			"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    			"FROM ( " +  
    				"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    				"ID, TBATTENDANCE.STAFFID, " +  
    				"CASE " +     
    					"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    					"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    				"END AS DESCRIPTION, " +  
    				"TBATTENDANCE.ATTENDANCEDATE, " +  
    				"TBATTENDANCE.ATTENDANCETIME " +  
    				"FROM TBATTENDANCE " +  
    			") TABLE1 " +  
    			"WHERE RowNum = 1 " +
    		") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    		"LEFT JOIN( " +  
    			"SELECT STAFFID , ATTENDANCEDATE, " +    
    			"STUFF( " +  
    				"(SELECT " + 
    				"CASE " + 
    					"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    					"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    				"END " + 
    				"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    			")  DESCRIPTION   " +
    			"FROM TBATTENDANCE b  " + 
    			"GROUP BY STAFFID, ATTENDANCEDATE " +  
    		") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    		"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    		"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
    	") SUMMARY " +
    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";*/
    	
    			String sqlStmt = "" + 
    	    	"SELECT * FROM " +
    	    	"( " +
    	    		"SELECT " + 
    	    		"ROW_NUMBER() over (ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID, v_attendance_single.ATTENDANCEDATE) as RowNum, " + 
    	    		"v_attendance_single.STAFFID, " +
    	    		"TBSTAFF.STAFFNAME, " + 
    	    		"TBDEPARTMENT.DEPARTMENTID, " + 
    	    		"TBDEPARTMENT.DEPARTMENTNAME, " + 
    	    		"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    	    		"v_attendance_single.ATTENDANCEDATE, " +
    	    		"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    	    		"CASE v_attendance_single.ATTENDANCETYPE " + 
    	    			"WHEN 'C' THEN '000000' " +
    	    			"ELSE TABLE2.ATTENDANCETIME  " +
    	    		"END AS CHECKOUT, " +
    	    		"v_attendance_duration_single.WORKHOUR, " + 
    	    		"v_attendance_duration_single.WORKMIN, " +  
    	    		"v_attendance_duration_single.WORKSEC, " + 
    	    		"v_attendance_single.ATTENDANCETYPE " +
    	    		"FROM v_attendance_single " +
    	    		"LEFT JOIN ( " +
    	    			"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    	    			"FROM ( " +  
    	    				"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    	    				"ID, TBATTENDANCE.STAFFID, " +  
    	    				"CASE " +     
    	    					"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    	    					"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    	    				"END AS DESCRIPTION, " +  
    	    				"TBATTENDANCE.ATTENDANCEDATE, " +  
    	    				"TBATTENDANCE.ATTENDANCETIME " +  
    	    				"FROM TBATTENDANCE " +  
    	    			") TABLE1 " +  
    	    			"WHERE RowNum = 1 " +
    	    		") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    	    		"LEFT JOIN( " +  
    	    			"SELECT STAFFID , ATTENDANCEDATE, " +    
    	    			"STUFF( " +  
    	    				"(SELECT " + 
    	    				"CASE " + 
    	    					"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    	    					"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    	    				"END " + 
    	    				"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    	    			")  DESCRIPTION   " +
    	    			"FROM TBATTENDANCE b  " + 
    	    			"GROUP BY STAFFID, ATTENDANCEDATE " +  
    	    		") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    	    		"LEFT JOIN v_attendance_duration_single ON (v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE) " +
    	    		"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    	    		"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	    	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
    	    	") SUMMARY " +
    	    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	    	
    	
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);        
        ArrayList<REPORT4> alReport = new ArrayList<REPORT4>();
        ResultSet srs = pStmt.executeQuery();
        
		while (srs.next()) {
			REPORT4 oReport = new REPORT4();
			oReport.setATTENDANCEDATE(srs.getString("ATTENDANCEDATE"));
			oReport.setSTAFFID(srs.getString("STAFFID"));
			oReport.setSTAFFNAME(srs.getString("STAFFNAME"));
			oReport.setDEPARTMENTID(srs.getString("DEPARTMENTID"));
			oReport.setDEPARTMENTNAME(srs.getString("DEPARTMENTNAME"));
			/*oReport.setREASONID(srs.getString("REASONID"));
			oReport.setREASONNAME(srs.getString("REASONNAME"));*/
			oReport.setDESCRIPTION(srs.getString("DESCRIPTION"));
			oReport.setCHECKIN(srs.getString("CHECKIN"));
			oReport.setCHECKOUT(srs.getString("CHECKOUT"));
			if(!srs.getString("ATTENDANCETYPE").equals("C")) {
				oReport.setWORKHOUR(String.format("%02d" , Integer.parseInt(srs.getString("WORKHOUR")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKMIN")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKSEC"))));
			} else {
				oReport.setWORKHOUR("-");
			}
			
			alReport.add(oReport);
		}

		REPORT4[] reports = new REPORT4[alReport.size()];
		alReport.toArray(reports);
		return reports;
	}
	
	public int countReport4View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(v_attendance_single.STAFFID) = '" + STAFFID.toUpperCase() + "'";
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    			
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(TBDEPARTMENT.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    			
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "v_attendance_single.ATTENDANCEDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "v_attendance_single.ATTENDANCEDATE <= " + END + "";
    	}
    	
    	/*
        String sqlStmt = "" + 
"SELECT " + 
"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +  
"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " +
	"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
	"FROM ( " +
		"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " + 
		"ID, TBATTENDANCE.STAFFID, " +
		"CASE " +
		  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +
		  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +   
		"END AS DESCRIPTION, " +
		"TBATTENDANCE.ATTENDANCEDATE, " +
		"TBATTENDANCE.ATTENDANCETIME " +
		"FROM TBATTENDANCE " +
	") TABLE1 " +
	"WHERE RowNum = 1 " +
") TABLE2 " +
"LEFT JOIN ( " +
"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
"FROM ( " +
	"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
	"ID, TBATTENDANCE.STAFFID, " + 
	"CASE " +
	  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
	  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
	"END AS DESCRIPTION, " +
	"TBATTENDANCE.ATTENDANCEDATE, " + 
	"TBATTENDANCE.ATTENDANCETIME " +
	"FROM TBATTENDANCE " +
") TABLE1 " +
"WHERE RowNum = 1 " +
") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
"LEFT JOIN( " + 
	"SELECT STAFFID , ATTENDANCEDATE, " +   
	"STUFF( " + 
		"(SELECT " +
		"CASE " +
			"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " + 
			"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " + 
		"END " +
		"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " + 
	")  DESCRIPTION " + 
	"FROM TBATTENDANCE b " + 
	"GROUP BY STAFFID, ATTENDANCEDATE " + 
") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +  
"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " +
"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +
"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
"GROUP BY TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION ";
*/
    	/*String sqlStmt = "" + 
    		"SELECT " +  
    		"v_attendance_single.STAFFID, " +
    		"TBSTAFF.STAFFNAME, " + 
    		"TBDEPARTMENT.DEPARTMENTID, " + 
    		"TBDEPARTMENT.DEPARTMENTNAME, " + 
    		"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    		"v_attendance_single.ATTENDANCEDATE, " +
    		"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    		"CASE v_attendance_single.ATTENDANCETYPE " + 
    			"WHEN 'C' THEN '000000' " +
    			"ELSE TABLE2.ATTENDANCETIME  " +
    		"END AS CHECKOUT, " +
    		"v_attendance_single.ATTENDANCETYPE " +
    		"FROM v_attendance_single " +
    		"LEFT JOIN ( " +
    			"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    			"FROM ( " +  
    				"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    				"ID, TBATTENDANCE.STAFFID, " +  
    				"CASE " +     
    					"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    					"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    				"END AS DESCRIPTION, " +  
    				"TBATTENDANCE.ATTENDANCEDATE, " +  
    				"TBATTENDANCE.ATTENDANCETIME " +  
    				"FROM TBATTENDANCE " +  
    			") TABLE1 " +  
    			"WHERE RowNum = 1 " +
    		") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    		"LEFT JOIN( " +  
    			"SELECT STAFFID , ATTENDANCEDATE, " +    
    			"STUFF( " +  
    				"(SELECT " + 
    				"CASE " + 
    					"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    					"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    				"END " + 
    				"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    			")  DESCRIPTION   " +
    			"FROM TBATTENDANCE b  " + 
    			"GROUP BY STAFFID, ATTENDANCEDATE " +  
    		") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    		"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    		"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
    	*/
    	String sqlStmt = "" + 
        		"SELECT " +  
        		"v_attendance_single.STAFFID, " +
        		"TBSTAFF.STAFFNAME, " + 
        		"TBDEPARTMENT.DEPARTMENTID, " + 
        		"TBDEPARTMENT.DEPARTMENTNAME, " + 
        		"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
        		"v_attendance_single.ATTENDANCEDATE, " +
        		"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
        		"v_attendance_duration_single.WORKHOUR, " + 
	    		"v_attendance_duration_single.WORKMIN, " + 
	    		"v_attendance_duration_single.WORKSEC, " + 
        		"CASE v_attendance_single.ATTENDANCETYPE " + 
        			"WHEN 'C' THEN '000000' " +
        			"ELSE TABLE2.ATTENDANCETIME  " +
        		"END AS CHECKOUT, " +
        		"v_attendance_single.ATTENDANCETYPE " +
        		"FROM v_attendance_single " +
        		"LEFT JOIN ( " +
        			"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
        			"FROM ( " +  
        				"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
        				"ID, TBATTENDANCE.STAFFID, " +  
        				"CASE " +     
        					"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
        					"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
        				"END AS DESCRIPTION, " +  
        				"TBATTENDANCE.ATTENDANCEDATE, " +  
        				"TBATTENDANCE.ATTENDANCETIME " +  
        				"FROM TBATTENDANCE " +  
        			") TABLE1 " +  
        			"WHERE RowNum = 1 " +
        		") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
        		"LEFT JOIN( " +  
        			"SELECT STAFFID , ATTENDANCEDATE, " +    
        			"STUFF( " +  
        				"(SELECT " + 
        				"CASE " + 
        					"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
        					"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
        				"END " + 
        				"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
        			")  DESCRIPTION   " +
        			"FROM TBATTENDANCE b  " + 
        			"GROUP BY STAFFID, ATTENDANCEDATE " +  
        		") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
        		"LEFT JOIN v_attendance_duration_single ON (v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE) " +
	    		"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
        		"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
        	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
        	
    	
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public REPORT5[] genReport5View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String WORKINGOPTION, String SHIFTOPTION, String START, String END, String offset) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String WorkOptionFilter = "1 = 1";
    	String ShiftOptionFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String itemPerPage = "30";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(v_attendance_single.STAFFID) = '" + STAFFID.toUpperCase() + "'";
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBSTAFF.REGISTEREDBRANCHID) = '" + BRANCHID.toUpperCase() + "'";
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(TBDEPARTMENT.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    			
    	} 
    	if(WORKINGOPTION != "" && WORKINGOPTION != "0" && WORKINGOPTION.trim().length() > 0){
    		if(WORKINGOPTION == "1" || WORKINGOPTION.equals("1")){
    			WorkOptionFilter = "v_attendance_duration_single.WORKHOUR >= 9";
    		} else if (WORKINGOPTION == "2" || WORKINGOPTION.equals("2")){
    			WorkOptionFilter = "v_attendance_duration_single.WORKHOUR < 9";
    		}
    	} 
    	if(SHIFTOPTION != "" && SHIFTOPTION != "0" && SHIFTOPTION.trim().length() > 0){
    		if(SHIFTOPTION == "1" || SHIFTOPTION.equals("1")){
    			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '060000' AND v_attendance_single.CLOCKINTIME < '080000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
    		} else if (SHIFTOPTION == "2" || SHIFTOPTION.equals("2")){
    			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '080000' AND v_attendance_single.CLOCKINTIME < '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
    		} else if (SHIFTOPTION == "3" || SHIFTOPTION.equals("3")){
    			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
    		}
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "v_attendance_single.ATTENDANCEDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "v_attendance_single.ATTENDANCEDATE <= " + END + "";
    	}
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	/*String sqlStmt = "" + 
"SELECT * " + 
"FROM ( " + 
	"SELECT  " + 
	"ROW_NUMBER() over (ORDER BY TBDEPARTMENT.DEPARTMENTID, TABLE2.STAFFID, TABLE2.ATTENDANCEDATE) as RowNum, " + 
	"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TABLE2.BRANCHID, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +   
	"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " + 
		"SELECT STAFFID, BRANCHID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " + 
		"FROM ( " + 
			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +  
			"ID, TBATTENDANCE.STAFFID, TBATTENDANCE.BRANCHID, " + 
			"CASE " +    
			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +    
			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +    
			"END AS DESCRIPTION, " + 
			"TBATTENDANCE.ATTENDANCEDATE, " + 
			"TBATTENDANCE.ATTENDANCETIME " + 
			"FROM TBATTENDANCE " + 
		") TABLE1 " + 
		"WHERE RowNum = 1 " + 
	") TABLE2 " + 
	"LEFT JOIN ( " +
		"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
		"FROM ( " +
			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
			"ID, TBATTENDANCE.STAFFID, " + 
			"CASE " +
			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
			"END AS DESCRIPTION, " +
			"TBATTENDANCE.ATTENDANCEDATE, " + 
			"TBATTENDANCE.ATTENDANCETIME " +
			"FROM TBATTENDANCE " +
		") TABLE1 " +
		"WHERE RowNum = 1 " +
	") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
	"LEFT JOIN( " + 
		"SELECT STAFFID , ATTENDANCEDATE, " +   
		"STUFF( " + 
			"(SELECT " +
			"CASE " +
				"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " + 
				"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " + 
			"END " +
			"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " + 
		")  DESCRIPTION " + 
		"FROM TBATTENDANCE b " + 
		"GROUP BY STAFFID, ATTENDANCEDATE " + 
	") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +  
	"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " + 
	"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " + 
    "WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
	"GROUP BY TABLE2.STAFFID, TABLE2.BRANCHID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION " + 
") TABLE3 " + 
"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";*/
    	
    	/*String sqlStmt = "" + 
    	"SELECT * FROM " +
    	"( " +
    		"SELECT " + 
    		"ROW_NUMBER() over (ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID, v_attendance_single.ATTENDANCEDATE) as RowNum, " + 
    		"v_attendance_single.STAFFID, " +
    		"TBSTAFF.STAFFNAME, " + 
    		"TBDEPARTMENT.DEPARTMENTID, " + 
    		"TBDEPARTMENT.DEPARTMENTNAME, " +
    		"TBSTAFF.REGISTEREDBRANCHID, " +
    		"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    		"v_attendance_single.ATTENDANCEDATE, " +
    		"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    		"CASE v_attendance_single.ATTENDANCETYPE " + 
    			"WHEN 'C' THEN '000000' " +
    			"ELSE TABLE2.ATTENDANCETIME  " +
    		"END AS CHECKOUT, " +
    		"v_attendance_single.ATTENDANCETYPE " +
    		"FROM v_attendance_single " +
    		"LEFT JOIN ( " +
    			"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    			"FROM ( " +  
    				"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    				"ID, TBATTENDANCE.STAFFID, " +  
    				"CASE " +     
    					"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    					"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    				"END AS DESCRIPTION, " +  
    				"TBATTENDANCE.ATTENDANCEDATE, " +  
    				"TBATTENDANCE.ATTENDANCETIME " +  
    				"FROM TBATTENDANCE " +  
    			") TABLE1 " +  
    			"WHERE RowNum = 1 " +
    		") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    		"LEFT JOIN( " +  
    			"SELECT STAFFID , ATTENDANCEDATE, " +    
    			"STUFF( " +  
    				"(SELECT " + 
    				"CASE " + 
    					"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    					"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    				"END " + 
    				"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    			")  DESCRIPTION   " +
    			"FROM TBATTENDANCE b  " + 
    			"GROUP BY STAFFID, ATTENDANCEDATE " +  
    		") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    		"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    		"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
    	") SUMMARY " +
    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";*/
    	String sqlStmt = "" + 
    	    	"SELECT * FROM " +
    	    	"( " +
    	    		"SELECT " + 
    	    		"ROW_NUMBER() over (ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID, v_attendance_single.ATTENDANCEDATE) as RowNum, " + 
    	    		"v_attendance_single.STAFFID, " +
    	    		"TBSTAFF.STAFFNAME, " + 
    	    		"TBDEPARTMENT.DEPARTMENTID, " + 
    	    		"TBDEPARTMENT.DEPARTMENTNAME, " +
    	    		"TBSTAFF.REGISTEREDBRANCHID, " +
    	    		"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    	    		"v_attendance_single.ATTENDANCEDATE, " +
    	    		"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    	    		"v_attendance_duration_single.WORKHOUR, " + 
    	    		"v_attendance_duration_single.WORKMIN, " + 
    	    		"v_attendance_duration_single.WORKSEC, " + 
            		"CASE v_attendance_single.ATTENDANCETYPE " + 
    	    			"WHEN 'C' THEN '000000' " +
    	    			"ELSE TABLE2.ATTENDANCETIME  " +
    	    		"END AS CHECKOUT, " +
    	    		"v_attendance_single.ATTENDANCETYPE " +
    	    		"FROM v_attendance_single " +
    	    		"LEFT JOIN ( " +
    	    			"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    	    			"FROM ( " +  
    	    				"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    	    				"ID, TBATTENDANCE.STAFFID, " +  
    	    				"CASE " +     
    	    					"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    	    					"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    	    				"END AS DESCRIPTION, " +  
    	    				"TBATTENDANCE.ATTENDANCEDATE, " +  
    	    				"TBATTENDANCE.ATTENDANCETIME " +  
    	    				"FROM TBATTENDANCE " +  
    	    			") TABLE1 " +  
    	    			"WHERE RowNum = 1 " +
    	    		") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    	    		"LEFT JOIN( " +  
    	    			"SELECT STAFFID , ATTENDANCEDATE, " +    
    	    			"STUFF( " +  
    	    				"(SELECT " + 
    	    				"CASE " + 
    	    					"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    	    					"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    	    				"END " + 
    	    				"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    	    			")  DESCRIPTION   " +
    	    			"FROM TBATTENDANCE b  " + 
    	    			"GROUP BY STAFFID, ATTENDANCEDATE " +  
    	    		") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    	    		"LEFT JOIN v_attendance_duration_single ON (v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE) " +
    	    		"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    	    		"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	    	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + WorkOptionFilter + " AND " + ShiftOptionFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
    	    	") SUMMARY " +
    	    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);        
        ArrayList<REPORT5> alReport = new ArrayList<REPORT5>();
        ResultSet srs = pStmt.executeQuery();
        
		while (srs.next()) {
			REPORT5 oReport = new REPORT5();
			oReport.setATTENDANCEDATE(srs.getString("ATTENDANCEDATE"));
			oReport.setSTAFFID(srs.getString("STAFFID"));
			oReport.setSTAFFNAME(srs.getString("STAFFNAME"));
			oReport.setDEPARTMENTID(srs.getString("DEPARTMENTID"));
			oReport.setDEPARTMENTNAME(srs.getString("DEPARTMENTNAME"));
			/*oReport.setREASONID(srs.getString("REASONID"));
			oReport.setREASONNAME(srs.getString("REASONNAME"));*/
			oReport.setDESCRIPTION(srs.getString("DESCRIPTION"));
			oReport.setCHECKIN(srs.getString("CHECKIN"));
			oReport.setCHECKOUT(srs.getString("CHECKOUT"));
			if(!srs.getString("ATTENDANCETYPE").equals("C")) {
				oReport.setWORKHOUR(String.format("%02d" , Integer.parseInt(srs.getString("WORKHOUR")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKMIN")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKSEC"))));
			} else {
				oReport.setWORKHOUR("-");
			}
			//oReport.setBRANCHID(srs.getString("BRANCHID"));
			
			oReport.setBRANCHID(srs.getString("REGISTEREDBRANCHID"));
			
			alReport.add(oReport);
		}

		REPORT5[] reports = new REPORT5[alReport.size()];
		alReport.toArray(reports);
		return reports;
	}
	
	public int countReport5View(String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String WORKINGOPTION, String SHIFTOPTION, String START, String END) throws SQLException {
		String StaffIdFilter = "1 = 1";
		String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String WorkOptionFilter = "1 = 1";
    	String ShiftOptionFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(v_attendance_single.STAFFID) = '" + STAFFID.toUpperCase() + "'";
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBSTAFF.REGISTEREDBRANCHID) = '" + BRANCHID.toUpperCase() + "'";
    	} 
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(TBDEPARTMENT.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    			
    	} 
    	if(WORKINGOPTION != "" && WORKINGOPTION != "0" && WORKINGOPTION.trim().length() > 0){
    		if(WORKINGOPTION == "1" || WORKINGOPTION.equals("1")){
    			WorkOptionFilter = "v_attendance_duration_single.WORKHOUR >= 9";
    		} else if (WORKINGOPTION == "2" || WORKINGOPTION.equals("2")){
    			WorkOptionFilter = "v_attendance_duration_single.WORKHOUR < 9";
    		}
    	} 
    	if(SHIFTOPTION != "" && SHIFTOPTION != "0" && SHIFTOPTION.trim().length() > 0){
    		if(SHIFTOPTION == "1" || SHIFTOPTION.equals("1")){
    			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '060000' AND v_attendance_single.CLOCKINTIME < '080000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
    		} else if (SHIFTOPTION == "2" || SHIFTOPTION.equals("2")){
    			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '080000' AND v_attendance_single.CLOCKINTIME < '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
    		} else if (SHIFTOPTION == "3" || SHIFTOPTION.equals("3")){
    			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
    		}
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "v_attendance_single.ATTENDANCEDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "v_attendance_single.ATTENDANCEDATE <= " + END + "";
    	}
    	
        /*String sqlStmt = "" + 
"SELECT " + 
"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +  
"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " +
	"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
	"FROM ( " +
		"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " + 
		"ID, TBATTENDANCE.STAFFID, " +
		"CASE " +
		  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +
		  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +   
		"END AS DESCRIPTION, " +
		"TBATTENDANCE.ATTENDANCEDATE, " +
		"TBATTENDANCE.ATTENDANCETIME " +
		"FROM TBATTENDANCE " +
	") TABLE1 " +
	"WHERE RowNum = 1 " +
") TABLE2 " +
"LEFT JOIN ( " +
"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
"FROM ( " +
	"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
	"ID, TBATTENDANCE.STAFFID, " + 
	"CASE " +
	  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
	  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
	"END AS DESCRIPTION, " +
	"TBATTENDANCE.ATTENDANCEDATE, " + 
	"TBATTENDANCE.ATTENDANCETIME " +
	"FROM TBATTENDANCE " +
") TABLE1 " +
"WHERE RowNum = 1 " +
") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
"LEFT JOIN( " + 
	"SELECT STAFFID , ATTENDANCEDATE, " +   
	"STUFF( " + 
		"(SELECT " +
		"CASE " +
			"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " + 
			"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " + 
		"END " +
		"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " + 
	")  DESCRIPTION " + 
	"FROM TBATTENDANCE b " + 
	"GROUP BY STAFFID, ATTENDANCEDATE " + 
") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +  
"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " +
"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +
"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
"GROUP BY TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION ";
*/
    	/*String sqlStmt = "" + 
			"SELECT " +  
			"v_attendance_single.STAFFID, " +
			"TBSTAFF.STAFFNAME, " + 
			"TBDEPARTMENT.DEPARTMENTID, " + 
			"TBDEPARTMENT.DEPARTMENTNAME, " + 
			"TBSTAFF.REGISTEREDBRANCHID, " +
			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
			"v_attendance_single.ATTENDANCEDATE, " +
			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
			"CASE v_attendance_single.ATTENDANCETYPE " + 
				"WHEN 'C' THEN '000000' " +
				"ELSE TABLE2.ATTENDANCETIME  " +
			"END AS CHECKOUT, " +
			"v_attendance_single.ATTENDANCETYPE " +
			"FROM v_attendance_single " +
			"LEFT JOIN ( " +
				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
				"FROM ( " +  
					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
					"ID, TBATTENDANCE.STAFFID, " +  
					"CASE " +     
						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
					"END AS DESCRIPTION, " +  
					"TBATTENDANCE.ATTENDANCEDATE, " +  
					"TBATTENDANCE.ATTENDANCETIME " +  
					"FROM TBATTENDANCE " +  
				") TABLE1 " +  
				"WHERE RowNum = 1 " +
			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
			"LEFT JOIN( " +  
				"SELECT STAFFID , ATTENDANCEDATE, " +    
				"STUFF( " +  
					"(SELECT " + 
					"CASE " + 
						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
					"END " + 
					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
				")  DESCRIPTION   " +
				"FROM TBATTENDANCE b  " + 
				"GROUP BY STAFFID, ATTENDANCEDATE " +  
			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
		*/
    	String sqlStmt = "" + 
    			"SELECT " +  
    			"v_attendance_single.STAFFID, " +
    			"TBSTAFF.STAFFNAME, " + 
    			"TBDEPARTMENT.DEPARTMENTID, " + 
    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
    			"TBSTAFF.REGISTEREDBRANCHID, " +
    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    			"v_attendance_single.ATTENDANCEDATE, " +
    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    			"v_attendance_duration_single.WORKHOUR, " + 
    			"v_attendance_duration_single.WORKMIN, " + 
    			"v_attendance_duration_single.WORKSEC, " + 
        		"CASE v_attendance_single.ATTENDANCETYPE " + 
    				"WHEN 'C' THEN '000000' " +
    				"ELSE TABLE2.ATTENDANCETIME  " +
    			"END AS CHECKOUT, " +
    			"v_attendance_single.ATTENDANCETYPE " +
    			"FROM v_attendance_single " +
    			"LEFT JOIN ( " +
    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    				"FROM ( " +  
    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    					"ID, TBATTENDANCE.STAFFID, " +  
    					"CASE " +     
    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    					"END AS DESCRIPTION, " +  
    					"TBATTENDANCE.ATTENDANCEDATE, " +  
    					"TBATTENDANCE.ATTENDANCETIME " +  
    					"FROM TBATTENDANCE " +  
    				") TABLE1 " +  
    				"WHERE RowNum = 1 " +
    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    			"LEFT JOIN( " +  
    				"SELECT STAFFID , ATTENDANCEDATE, " +    
    				"STUFF( " +  
    					"(SELECT " + 
    					"CASE " + 
    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    					"END " + 
    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    				")  DESCRIPTION   " +
    				"FROM TBATTENDANCE b  " + 
    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    			"LEFT JOIN v_attendance_duration_single ON (v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE) " +
	    		"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + WorkOptionFilter + " AND " + ShiftOptionFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
    		
	    	
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public String ExportReport(String masterPath, String reportType, String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String WORKINGOPTION, String SHIFTOPTION, String START, String END) throws SQLException {
   	 	String outputName = "";
   	 	DataOutputStream outData = null;     
   	 	Calendar cal = Calendar.getInstance();
   	 	cal.add(Calendar.DATE, -1);
        Date BackupDate = cal.getTime();
        String sExportId = util.DateFormatConverter.format(BackupDate, "yyyyMMdd"); 
        //String sOutputFolder = masterPath+"\\";
        String sOutputFolder = masterPath;
        
        String StaffIdFilter = "1 = 1";
        String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String WorkOptionFilter = "1 = 1";
    	String ShiftOptionFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
        
        try {
            this.createFolder(new File(sOutputFolder));        
            String sqlStmt = "";
            
            if(reportType.equals("1")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		BranchIdFilter = "UPPER(TBACCESSGROUPDETAIL.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		
            	}
            	if(START != "" && START.trim().length() > 0){
            			
            	} 
            	if(END != "" && END.trim().length() > 0){
            			
            	} 
            	sqlStmt = "SELECT TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,'') AS THUMBCOUNT, TBSTAFF.RECORDSTATUS FROM TBSTAFF LEFT JOIN (SELECT STAFFID, COUNT(*) THUMBCOUNT FROM TBSTAFFTHUMB GROUP BY STAFFID) THUMB ON (THUMB.STAFFID = TBSTAFF.STAFFID) " +
            				"LEFT JOIN TBACCESSGROUPDETAIL ON (TBACCESSGROUPDETAIL.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID) " + 
            			"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
            } else if (reportType.equals("2")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		BranchIdFilter = "UPPER(TBATTENDANCE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		DeviceIdFilter = "UPPER(TBATTENDANCE.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";
            	}
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBATTENDANCE.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBATTENDANCE.ATTENDANCEDATE <= " + END + "";
            	} 
            	
            	sqlStmt = "SELECT TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " + 
						"TBATTENDANCE.DEVICEID, TBDEVICE.DEVICENAME, " +				
						"TBATTENDANCE.BRANCHID, TBBRANCH.BRANCHNAME, " +
						"TBATTENDANCE.ATTENDANCEDATE, " +
						"TBATTENDANCE.ATTENDANCETIME, " +
						"TBATTENDANCE.INOUT, " +
						"TBATTENDANCE.DESCRIPTION, " +
						"TBATTENDANCE.REASONID, TBREASON.REASONNAME " +
						"FROM TBATTENDANCE  " +
						"LEFT JOIN TBSTAFF ON (TBATTENDANCE.STAFFID = TBSTAFF.STAFFID AND TBSTAFF.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBDEVICE ON (TBATTENDANCE.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBBRANCH ON (TBATTENDANCE.BRANCHID = TBBRANCH.BRANCHID AND TBBRANCH.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBREASON ON (TBATTENDANCE.REASONID = TBREASON.REASONID AND TBREASON.RECORDSTATUS = 'A') " +
						"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
						"ORDER BY TBATTENDANCE.ATTENDANCEDATE,TBATTENDANCE.ATTENDANCETIME ";
            } else if (reportType.equals("3")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            			
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            			
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		
            	} 
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		DeviceIdFilter = "UPPER(TBDEVICEDOWNTIME.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";
            	}
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBDEVICEDOWNTIME.EVENTDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBDEVICEDOWNTIME.EVENTDATE <= " + END + "";
            	} 
            	
            	sqlStmt = "SELECT TBDEVICEDOWNTIME.DEVICEID, TBDEVICE.DEVICENAME, " + 				
						"TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME, " + 
						"TBDEVICEDOWNTIME.STATUS " +  
						"FROM TBDEVICEDOWNTIME " + 
						"LEFT JOIN TBDEVICE ON (TBDEVICEDOWNTIME.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " +
						"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
						"ORDER BY TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME";
            } else if (reportType.equals("4")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(v_attendance_single.STAFFID) = '" + STAFFID.toUpperCase() + "'";
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            			
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBDEPARTMENT.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            			
            	} 
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "v_attendance_single.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "v_attendance_single.ATTENDANCEDATE <= " + END + "";
            	}
            	
            	/*sqlStmt = "" + 
            	"SELECT " + 
            	"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +  
            	"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " +
            		"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
            		"FROM ( " +
            			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " + 
            			"ID, TBATTENDANCE.STAFFID, " +
            			"CASE " +
            			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +
            			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +   
            			"END AS DESCRIPTION, " +
            			"TBATTENDANCE.ATTENDANCEDATE, " +
            			"TBATTENDANCE.ATTENDANCETIME " +
            			"FROM TBATTENDANCE " +
            		") TABLE1 " +
            		"WHERE RowNum = 1 " +
            	") TABLE2 " +
            	"LEFT JOIN ( " +
	            	"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
	            	"FROM ( " +
	            		"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
	            		"ID, TBATTENDANCE.STAFFID, " + 
	            		"CASE " +
	            		  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
	            		  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
	            		"END AS DESCRIPTION, " +
	            		"TBATTENDANCE.ATTENDANCEDATE, " + 
	            		"TBATTENDANCE.ATTENDANCETIME " +
	            		"FROM TBATTENDANCE " +
	            	") TABLE1 " +
	            	"WHERE RowNum = 1 " +
            	") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
            	"LEFT JOIN( " + 
	        		"SELECT STAFFID , ATTENDANCEDATE, " +   
	        		"STUFF( " + 
		        		"(SELECT " +
	        			"CASE " +
	        				"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " + 
	        				"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " + 
	        			"END " +
	        			"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " + 
	        		")  DESCRIPTION " + 
	        		"FROM TBATTENDANCE b " + 
	        		"GROUP BY STAFFID, ATTENDANCEDATE " + 
	        	") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +
            	"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " +
            	"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +
            	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
            	"GROUP BY TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION ";*/
            	
            	/*sqlStmt = "" + 
	    			"SELECT " +  
	    			"v_attendance_single.STAFFID, " +
	    			"TBSTAFF.STAFFNAME, " + 
	    			"TBDEPARTMENT.DEPARTMENTID, " + 
	    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
	    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
	    			"v_attendance_single.ATTENDANCEDATE, " +
	    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
	    			"CASE v_attendance_single.ATTENDANCETYPE " + 
	    				"WHEN 'C' THEN '000000' " +
	    				"ELSE TABLE2.ATTENDANCETIME  " +
	    			"END AS CHECKOUT, " +
	    			"v_attendance_single.ATTENDANCETYPE " +
	    			"FROM v_attendance_single " +
	    			"LEFT JOIN ( " +
	    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
	    				"FROM ( " +  
	    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
	    					"ID, TBATTENDANCE.STAFFID, " +  
	    					"CASE " +     
	    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
	    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
	    					"END AS DESCRIPTION, " +  
	    					"TBATTENDANCE.ATTENDANCEDATE, " +  
	    					"TBATTENDANCE.ATTENDANCETIME " +  
	    					"FROM TBATTENDANCE " +  
	    				") TABLE1 " +  
	    				"WHERE RowNum = 1 " +
	    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
	    			"LEFT JOIN( " +  
	    				"SELECT STAFFID , ATTENDANCEDATE, " +    
	    				"STUFF( " +  
	    					"(SELECT " + 
	    					"CASE " + 
	    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
	    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
	    					"END " + 
	    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
	    				")  DESCRIPTION   " +
	    				"FROM TBATTENDANCE b  " + 
	    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
	    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
	    			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
	    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
	    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
	    		"ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID";*/
            	
            	sqlStmt = "" + 
    	    			"SELECT " +  
    	    			"v_attendance_single.STAFFID, " +
    	    			"TBSTAFF.STAFFNAME, " + 
    	    			"TBDEPARTMENT.DEPARTMENTID, " + 
    	    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
    	    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    	    			"v_attendance_single.ATTENDANCEDATE, " +
    	    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    	    			"v_attendance_duration_single.WORKHOUR, " + 
        	    		"v_attendance_duration_single.WORKMIN, " + 
        	    		"v_attendance_duration_single.WORKSEC, " + 
    	    			"CASE v_attendance_single.ATTENDANCETYPE " + 
    	    				"WHEN 'C' THEN '000000' " +
    	    				"ELSE TABLE2.ATTENDANCETIME  " +
    	    			"END AS CHECKOUT, " +
    	    			"v_attendance_single.ATTENDANCETYPE " +
    	    			"FROM v_attendance_single " +
    	    			"LEFT JOIN ( " +
    	    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    	    				"FROM ( " +  
    	    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    	    					"ID, TBATTENDANCE.STAFFID, " +  
    	    					"CASE " +     
    	    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    	    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    	    					"END AS DESCRIPTION, " +  
    	    					"TBATTENDANCE.ATTENDANCEDATE, " +  
    	    					"TBATTENDANCE.ATTENDANCETIME " +  
    	    					"FROM TBATTENDANCE " +  
    	    				") TABLE1 " +  
    	    				"WHERE RowNum = 1 " +
    	    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    	    			"LEFT JOIN( " +  
    	    				"SELECT STAFFID , ATTENDANCEDATE, " +    
    	    				"STUFF( " +  
    	    					"(SELECT " + 
    	    					"CASE " + 
    	    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    	    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    	    					"END " + 
    	    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    	    				")  DESCRIPTION   " +
    	    				"FROM TBATTENDANCE b  " + 
    	    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
    	    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    	    			"LEFT JOIN v_attendance_duration_single ON (v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE) " +
    	    			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    	    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
    	    		"ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID";
	    	    	
	            	
            } else if (reportType.equals("5")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(v_attendance_single.STAFFID) = '" + STAFFID.toUpperCase() + "'";
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            			
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBDEPARTMENT.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            			
            	} 
            	if(WORKINGOPTION != "" && WORKINGOPTION != "0" && WORKINGOPTION.trim().length() > 0){
            		if(WORKINGOPTION == "1" || WORKINGOPTION.equals("1")){
            			WorkOptionFilter = "v_attendance_duration_single.WORKHOUR >= 9";
            		} else if (WORKINGOPTION == "2" || WORKINGOPTION.equals("2")){
            			WorkOptionFilter = "v_attendance_duration_single.WORKHOUR < 9";
            		}
            	} 
            	if(SHIFTOPTION != "" && SHIFTOPTION != "0" && SHIFTOPTION.trim().length() > 0){
            		if(SHIFTOPTION == "1" || SHIFTOPTION.equals("1")){
            			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '060000' AND v_attendance_single.CLOCKINTIME < '080000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
            		} else if (SHIFTOPTION == "2" || SHIFTOPTION.equals("2")){
            			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '080000' AND v_attendance_single.CLOCKINTIME < '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
            		} else if (SHIFTOPTION == "3" || SHIFTOPTION.equals("3")){
            			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
            		}
            	} 
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "v_attendance_single.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "v_attendance_single.ATTENDANCEDATE <= " + END + "";
            	}
            	
            	/*sqlStmt = "" + 
            	"SELECT " + 
            	"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +  
            	"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " +
            		"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
            		"FROM ( " +
            			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " + 
            			"ID, TBATTENDANCE.STAFFID, " +
            			"CASE " +
            			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +
            			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +   
            			"END AS DESCRIPTION, " +
            			"TBATTENDANCE.ATTENDANCEDATE, " +
            			"TBATTENDANCE.ATTENDANCETIME " +
            			"FROM TBATTENDANCE " +
            		") TABLE1 " +
            		"WHERE RowNum = 1 " +
            	") TABLE2 " +
            	"LEFT JOIN ( " +
	            	"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
	            	"FROM ( " +
	            		"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
	            		"ID, TBATTENDANCE.STAFFID, " + 
	            		"CASE " +
	            		  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
	            		  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
	            		"END AS DESCRIPTION, " +
	            		"TBATTENDANCE.ATTENDANCEDATE, " + 
	            		"TBATTENDANCE.ATTENDANCETIME " +
	            		"FROM TBATTENDANCE " +
	            	") TABLE1 " +
	            	"WHERE RowNum = 1 " +
            	") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
            	"LEFT JOIN( " + 
	        		"SELECT STAFFID , ATTENDANCEDATE, " +   
	        		"STUFF( " + 
	        		"(SELECT " +
	        			"CASE " +
	        				"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " + 
	        				"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " + 
	        			"END " +
	        			"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " + 
	        		")  DESCRIPTION " + 
	        		"FROM TBATTENDANCE b " + 
	        		"GROUP BY STAFFID, ATTENDANCEDATE " + 
	        	") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +
            	"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " +
            	"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +
            	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
            	"GROUP BY TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION " + 
            	"ORDER BY TBDEPARTMENT.DEPARTMENTID, TABLE2.STAFFID, TABLE2.ATTENDANCEDATE ";*/
            	
            	/*sqlStmt = "" + 
	    			"SELECT " +  
	    			"v_attendance_single.STAFFID, " +
	    			"TBSTAFF.STAFFNAME, " + 
	    			"TBDEPARTMENT.DEPARTMENTID, " + 
	    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
	    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
	    			"v_attendance_single.ATTENDANCEDATE, " +
	    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
	    			"CASE v_attendance_single.ATTENDANCETYPE " + 
	    				"WHEN 'C' THEN '000000' " +
	    				"ELSE TABLE2.ATTENDANCETIME  " +
	    			"END AS CHECKOUT, " +
	    			"v_attendance_single.ATTENDANCETYPE " +
	    			"FROM v_attendance_single " +
	    			"LEFT JOIN ( " +
	    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
	    				"FROM ( " +  
	    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
	    					"ID, TBATTENDANCE.STAFFID, " +  
	    					"CASE " +     
	    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
	    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
	    					"END AS DESCRIPTION, " +  
	    					"TBATTENDANCE.ATTENDANCEDATE, " +  
	    					"TBATTENDANCE.ATTENDANCETIME " +  
	    					"FROM TBATTENDANCE " +  
	    				") TABLE1 " +  
	    				"WHERE RowNum = 1 " +
	    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
	    			"LEFT JOIN( " +  
	    				"SELECT STAFFID , ATTENDANCEDATE, " +    
	    				"STUFF( " +  
	    					"(SELECT " + 
	    					"CASE " + 
	    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
	    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
	    					"END " + 
	    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
	    				")  DESCRIPTION   " +
	    				"FROM TBATTENDANCE b  " + 
	    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
	    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
	    			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
	    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
	    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
	    		"ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID";*/
            	sqlStmt = "" + 
    	    			"SELECT " +  
    	    			"v_attendance_single.STAFFID, " +
    	    			"TBSTAFF.STAFFNAME, " + 
    	    			"TBDEPARTMENT.DEPARTMENTID, " + 
    	    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
    	    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    	    			"v_attendance_single.ATTENDANCEDATE, " +
    	    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    	    			"v_attendance_duration_single.WORKHOUR, " + 
        	    		"v_attendance_duration_single.WORKMIN, " + 
        	    		"v_attendance_duration_single.WORKSEC, " + 
    	    			"CASE v_attendance_single.ATTENDANCETYPE " + 
    	    				"WHEN 'C' THEN '000000' " +
    	    				"ELSE TABLE2.ATTENDANCETIME  " +
    	    			"END AS CHECKOUT, " +
    	    			"v_attendance_single.ATTENDANCETYPE " +
    	    			"FROM v_attendance_single " +
    	    			"LEFT JOIN ( " +
    	    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    	    				"FROM ( " +  
    	    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    	    					"ID, TBATTENDANCE.STAFFID, " +  
    	    					"CASE " +     
    	    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    	    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    	    					"END AS DESCRIPTION, " +  
    	    					"TBATTENDANCE.ATTENDANCEDATE, " +  
    	    					"TBATTENDANCE.ATTENDANCETIME " +  
    	    					"FROM TBATTENDANCE " +  
    	    				") TABLE1 " +  
    	    				"WHERE RowNum = 1 " +
    	    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    	    			"LEFT JOIN( " +  
    	    				"SELECT STAFFID , ATTENDANCEDATE, " +    
    	    				"STUFF( " +  
    	    					"(SELECT " + 
    	    					"CASE " + 
    	    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    	    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    	    					"END " + 
    	    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    	    				")  DESCRIPTION   " +
    	    				"FROM TBATTENDANCE b  " + 
    	    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
    	    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    	    			"LEFT JOIN v_attendance_duration_single ON (v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE) " +
    	    			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    	    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + WorkOptionFilter + " AND " + ShiftOptionFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
    	    		"ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID";
	    		
	    	    	
            }
            
            PreparedStatement pSelectStmt = this.connection.prepareStatement(sqlStmt);
            //pSelectStmt.setInt(1, 1);
            ResultSet srs = pSelectStmt.executeQuery();
            
            FileOutputStream outfstreamData = null;
            
            if(reportType.equals("1")){
            	outfstreamData = new FileOutputStream(sOutputFolder + "Staff Register Report " + sExportId + ".csv");
            	outputName = "Staff Register Report " + sExportId + ".csv";
          		outData = new DataOutputStream(outfstreamData);
          		
                outData.writeBytes("" + "STAFF ID" + ","  + 
            			"STAFF NAME" + "," + 
            			"REGISTERED" + "," + 
            			"STATUS" + "\r\n");
                
                while (srs.next()) {
    	           	 String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
    	           	 String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
    	           	 String registered = "No";
    	           	 if(srs.getInt("THUMBCOUNT") > 0){
    	           		registered = "Yes";
    	           	 }
    	           	 String status = (srs.getString("RECORDSTATUS") == null)?"-":srs.getString("RECORDSTATUS");
    	           	 
    	       		 outData.writeBytes("" + staff_id + "," + 
    	       				staff_name + "," + 
    	       				registered + "," +
    	       				status + "\r\n");
                }
            } else if (reportType.equals("2")){
            	outfstreamData = new FileOutputStream(sOutputFolder + "Attendance Report " + sExportId + ".csv");
            	outputName = "Attendance Report " + sExportId + ".csv";
          		outData = new DataOutputStream(outfstreamData);
          		
                outData.writeBytes("" + "STAFF ID" + ","  + 
            			"STAFF NAME" + "," + 
            			//"DEVICE ID" + "," + 
            			//"DEVICE NAME" + "," +
            			"BRANCH ID" + "," + 
            			"DATE" + "," + 
            			"TIME" + "," +
            			"IN OUT" + "," + 
            			"REMARK" + "\r\n");
                
                while (srs.next()) {
    	           	 String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
    	           	 String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
    	           	 String device_id = (srs.getString("DEVICEID") == null)?"-":srs.getString("DEVICEID");
    	           	 String device_name = (srs.getString("DEVICENAME") == null)?"-":srs.getString("DEVICENAME");
    	           	 String branch_id = (srs.getString("BRANCHID") == null)?"-":srs.getString("BRANCHID");
    	           	 String branch_name = (srs.getString("BRANCHNAME") == null)?"-":srs.getString("BRANCHNAME");
    	           	 String atendance_date = (srs.getString("ATTENDANCEDATE") == null)?"-":srs.getString("ATTENDANCEDATE");
    	           	 String atendance_time = (srs.getString("ATTENDANCETIME") == null)?"0":srs.getString("ATTENDANCETIME");
    	           	
    	           	 String sAttDate = atendance_date;
    	           	 String sAttTime = atendance_time;
    	           	 if(atendance_date != "-" && atendance_time != "0"){
    	           		 sAttDate = util.DateFormatConverter.format3(atendance_date + atendance_time, "yyyyMMddHHmmss", "dd-MM-yyyy");
    	           		 sAttTime = util.DateFormatConverter.format3(atendance_date + atendance_time, "yyyyMMddHHmmss", "HH:mm:ss");
    	           	 }
    	           	 
    	           	 String in_out = (srs.getString("INOUT") == null)?"-":srs.getString("INOUT");
    	           	 if(in_out.equals("A")) { 
    	           		in_out = "IN";	
    	           	 } else if(in_out.equals("B")) { 
    	           		in_out = "OUT";
    	           	 } else { 
    	           	 	in_out = "-";
    	           	 }
    	           	 String reason_id = (srs.getString("REASONID") == null)?"-":srs.getString("REASONID");
					 String reason_name = (srs.getString("REASONNAME") == null)?"-":srs.getString("REASONNAME");
				     String description = (srs.getString("DESCRIPTION") == null)?"-":srs.getString("DESCRIPTION");
					 if(description == null || description.toString().trim().equals("")) { 
						 if(reason_id == null || reason_id == "-") {
							 description = "-";
						 } else {
							 description = reason_name;
						 }
					 } else {
						 description = reason_id + " - " + description;
					 }
					 if(description.length() > 28){
						 description = description.substring(0, 28);
					 }	
    	       		 outData.writeBytes("" + staff_id + "," + 
    	       				staff_name + "," + 
    	       				//device_id + "," +
    	       				//device_name + "," + 
    	       				branch_id + "," + 
    	       				sAttDate + "," + 
    	       				sAttTime + "," +
    	       				in_out + "," + 
    	       				description + "\r\n");
                }
            } else if (reportType.equals("3")){
            	outfstreamData = new FileOutputStream(sOutputFolder + "Device Downtime Report " + sExportId + ".csv");
            	outputName = "Device Downtime Report " + sExportId + ".csv";
          		outData = new DataOutputStream(outfstreamData);
          		
          		outData.writeBytes("" + "DEVICE ID" + ","  + 
            			"DEVICE NAME" + "," +
            			"DATE" + "," + 
            			"TIME" + "," + 
            			"STATUS" + "\r\n");
                
                while (srs.next()) {
    	           	 String device_id = (srs.getString("DEVICEID") == null)?"-":srs.getString("DEVICEID");
    	           	 String device_name = (srs.getString("DEVICENAME") == null)?"-":srs.getString("DEVICENAME");
    	           	 String event_date = (srs.getString("EVENTDATE") == null)?"-":srs.getString("EVENTDATE");
    	           	 String event_time = (srs.getString("EVENTTIME") == null)?"-":srs.getString("EVENTTIME");
    	           	 String sEventDate = event_date;
	   	           	 String sEventTime = event_time;
	   	           	 if(event_date != "-" && event_time != "-"){
	   	           		 sEventDate = util.DateFormatConverter.format3(event_date + event_time, "yyyyMMddHHmmss", "dd-MM-yyyy");
	   	           		 sEventTime = util.DateFormatConverter.format3(event_date + event_time, "yyyyMMddHHmmss", "HH:mm:ss");
	   	           	 }
	   	           	 
    	           	 String status = (srs.getString("STATUS") == null)?"-":srs.getString("STATUS");
    	           	 if(status.equals("1")){
    	           		status = "Up";
    	           	 } else {
    	           		 status = "Down";    	           		 
    	           	 }
    	           	 
    	       		 outData.writeBytes("" + device_id + "," + 
    	       				device_name + "," + 
    	       				sEventDate + "," + 
    	       				sEventTime + "," + 
    	       				status + "\r\n");
                }
            } else if (reportType.equals("4")){
            	outfstreamData = new FileOutputStream(sOutputFolder + "General Report " + sExportId + ".csv");
            	outputName = "General Report " + sExportId + ".csv";
          		outData = new DataOutputStream(outfstreamData);
          		
                outData.writeBytes("" + "STAFF ID" + ","  + 
            			"STAFF NAME" + "," + 
            			"DEPARTMENT NAME" + "," +
            			"DATE" + "," + 
            			"IN" + "," + 
            			"OUT" + "," + 
            			"OUT - IN HOURS" + "," + 
            			"REMARK" + "\r\n");
                
                while (srs.next()) {
    	           	 String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
    	           	 String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
    	           	 String department_id = (srs.getString("DEPARTMENTID") == null)?"-":srs.getString("DEPARTMENTID");
    	           	 String department_name = (srs.getString("DEPARTMENTNAME") == null)?"-":srs.getString("DEPARTMENTNAME");
    	           	 String atendance_date = (srs.getString("ATTENDANCEDATE") == null)?"-":srs.getString("ATTENDANCEDATE");
    	           	 String in = (srs.getString("CHECKIN") == null)?"0":srs.getString("CHECKIN");
    	           	 String out = (srs.getString("CHECKOUT") == null)?"-":srs.getString("CHECKOUT");
    	           	 String work_hour = "";
    	           	 if(!srs.getString("ATTENDANCETYPE").equals("C")) {
    	           		work_hour = String.format("%02d" , Integer.parseInt(srs.getString("WORKHOUR")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKMIN")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKSEC")));
    	           	 } else {
    	           		work_hour = "-";
    	           	 }
    	           	 
    	           	 String sAttDate = atendance_date;
	   	           	 String sAttTimeIn = in;
	   	           	 String sAttTimeOut = out;
	   	           	 if(atendance_date != "-" && in != "0"){
	   	           		 sAttDate = util.DateFormatConverter.format3(atendance_date + in, "yyyyMMddHHmmss", "dd-MM-yyyy") + " (" + util.DateFormatConverter.getWeekDay(atendance_date) + ")";
	   	           		 sAttTimeIn = util.DateFormatConverter.format3(atendance_date + in, "yyyyMMddHHmmss", "HH:mm:ss");	   	           		 
	   	           	 }
		   	         if(atendance_date != "-" && out != "-"){
		   	        	 sAttTimeOut = util.DateFormatConverter.format3(atendance_date + out, "yyyyMMddHHmmss", "HH:mm:ss");	   	           		 
		   	         }
	   	         
				     String description = (srs.getString("DESCRIPTION") == null)?"-":srs.getString("DESCRIPTION");
				     description = description.replace("  "," ");
				     description = description.replace("  "," ");
				     
    	       		 outData.writeBytes("" + staff_id + "," + 
    	       				staff_name + "," + 
    	       				department_name + "," + 
    	       				sAttDate + "," + 
    	       				sAttTimeIn + "," + 
    	       				sAttTimeOut + "," + 
    	       				work_hour + "," +
    	       				description + "\r\n");
                }
            } else if (reportType.equals("5")){
            	outfstreamData = new FileOutputStream(sOutputFolder + "In Out Report " + sExportId + ".csv");
            	outputName = "In Out Report " + sExportId + ".csv";
          		outData = new DataOutputStream(outfstreamData);
          		
                outData.writeBytes("" + "DATE" + ","  + 
                		"DEPARTMENT" + "," + 
                		"STAFF ID" + "," + 
                		"STAFF NAME" + "," + 
            			"IN" + "," + 
            			"OUT" + "," + 
            			"OUT - IN HOURS" + "," + 
            			"REMARK" + "\r\n");
                
                while (srs.next()) {
    	           	 String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
    	           	 String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
    	           	 String department_id = (srs.getString("DEPARTMENTID") == null)?"-":srs.getString("DEPARTMENTID");
    	           	 String department_name = (srs.getString("DEPARTMENTNAME") == null)?"-":srs.getString("DEPARTMENTNAME");
    	           	 String atendance_date = (srs.getString("ATTENDANCEDATE") == null)?"-":srs.getString("ATTENDANCEDATE");
    	           	 String in = (srs.getString("CHECKIN") == null)?"0":srs.getString("CHECKIN");
    	           	 String out = (srs.getString("CHECKOUT") == null)?"-":srs.getString("CHECKOUT");
    	           	 String work_hour = "";
	   	           	 if(!srs.getString("ATTENDANCETYPE").equals("C")) {
	   	           		work_hour = String.format("%02d" , Integer.parseInt(srs.getString("WORKHOUR")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKMIN")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKSEC")));
	   	           	 } else {
	   	           		work_hour = "-";
	   	           	 }
	   	           	 
    	           	 String sAttDate = atendance_date;
	   	           	 String sAttTimeIn = in;
	   	           	 String sAttTimeOut = out;
	   	           	 if(atendance_date != "-" && in != "0"){
	   	           		 sAttDate = util.DateFormatConverter.format3(atendance_date + in, "yyyyMMddHHmmss", "dd-MM-yyyy");
	   	           		 sAttTimeIn = util.DateFormatConverter.format3(atendance_date + in, "yyyyMMddHHmmss", "HH:mm:ss");	   	           		 
	   	           	 }
		   	         if(atendance_date != "-" && out != "-"){
		   	        	 sAttTimeOut = util.DateFormatConverter.format3(atendance_date + out, "yyyyMMddHHmmss", "HH:mm:ss");	   	           		 
		   	         }
    	           	 
				     String description = (srs.getString("DESCRIPTION") == null)?"-":srs.getString("DESCRIPTION");
				     description = description.replace("  "," ");
				     description = description.replace("  "," ");
				     
    	       		 outData.writeBytes("" + sAttDate + "," + 
    	       				department_name + "," + 
    	       				staff_id + "," + 
    	       				staff_name + "," + 
    	       				sAttTimeIn + "," + 
    	       				sAttTimeOut + "," + 
    	       				work_hour + "," +
    	       				description + "\r\n");
                }
            } 
            
            if(outData != null ){
           	 	outData.flush();        
            	outData.close();
            }
        }catch (Exception e){//Catch exception if any       
       	 errorMessage = e.getMessage();
       	 outputName = "";
        }    	
        return outputName;
   }

	public String ExportAttendance(String masterPath, String START, String END) throws SQLException {
   	 	String outputName = "";
   	 	DataOutputStream outData = null;     
   	 	Calendar cal = Calendar.getInstance();
   	 	cal.add(Calendar.DATE, -1);
        Date BackupDate = cal.getTime();
        String sExportId = util.DateFormatConverter.format(BackupDate, "ddMMyyyy"); 
        //String sOutputFolder = masterPath+"\\";
        String sOutputFolder = masterPath;
        
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
        
        try {
            this.createFolder(new File(sOutputFolder));        
            String sqlStmt = "";

        	if(START != "" && START.trim().length() > 0){
        		StartFilter = "ATTENDANCEDATE >= " + START + "";
        	} 
        	if(END != "" && END.trim().length() > 0){
        		EndFilter = "ATTENDANCEDATE <= " + END + "";
        	}  
        	sqlStmt = "SELECT STAFFID, DEVICEID, BRANCHID, INOUT, ATTENDANCEDATE, ATTENDANCETIME FROM TBATTENDANCE " + 
        			"WHERE " + StartFilter + " AND " + EndFilter + " ORDER BY ATTENDANCEDATE, ATTENDANCETIME ";
        	
            PreparedStatement pSelectStmt = this.connection.prepareStatement(sqlStmt);
            ResultSet srs = pSelectStmt.executeQuery();
            
            FileOutputStream outfstreamData = null;
            
            outfstreamData = new FileOutputStream(sOutputFolder + sExportId + ".txt");
        	outputName = sExportId + ".txt";
      		outData = new DataOutputStream(outfstreamData);
      		
            while (srs.next()) {
            	 String StartChar = "00";
            	 String MiddleChar = "000";
            	 String StaffId = (srs.getString("STAFFID") == null)?"":srs.getString("STAFFID").trim(); 
            	 String Inout = (srs.getString("INOUT") == null)?"":srs.getString("INOUT").trim();
            	 String AttDate = (srs.getString("ATTENDANCEDATE") == null)?"":srs.getString("ATTENDANCEDATE").trim();
            	 String AttDateShow = AttDate.substring(6, 8) + AttDate.substring(4, 6) + AttDate.substring(0, 4);
                 String AttTime = (srs.getString("ATTENDANCETIME") == null)?"":srs.getString("ATTENDANCETIME").trim();
                 String AttTimeShow = AttTime.substring(0, 4);
                 outData.writeBytes("" + StartChar +  
                		 AttDateShow +  
                		 AttTimeShow +
                		 Inout + 
                		 MiddleChar + 
                		 StaffId + "\r\n");
            }
            
            if(outData != null ){
           	 	outData.flush();        
            	outData.close();
            }
        }catch (Exception e){//Catch exception if any       
       	 errorMessage = e.getMessage();
       	 outputName = "";
        }    	
        return outputName;
   }

	public String ExportReportPdf(String masterPath, String reportType, String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String WORKINGOPTION, String SHIFTOPTION, String START, String END, String printed_by) throws DocumentException, IOException, SQLException   {
		String outputName = "";
		String reportName = "";
   	 	DataOutputStream outData = null;     
   	 	Calendar cal = Calendar.getInstance();
   	 	cal.add(Calendar.DATE, -1);
        Date BackupDate = cal.getTime();
        String sExportId = util.DateFormatConverter.format(BackupDate, "yyyyMMdd"); 
        //String sOutputFolder = masterPath+"\\";
        String sOutputFolder = masterPath;
        
        String StaffIdFilter = "1 = 1";
        String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String WorkOptionFilter = "1 = 1";
    	String ShiftOptionFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
  
        Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
        Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD, BaseColor.BLACK);
        Font tableHeaderFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, BaseColor.WHITE);
        Font small = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
        Font pageNumFont = new Font(Font.FontFamily.TIMES_ROMAN,10, Font.NORMAL);
        
        Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone("Asia/Singapore"));
        Date printDate = cal2.getTime();
        String sPrintDate = util.DateFormatConverter.format2(printDate, "yyyy-MM-dd HH:mm:ss"); 
        
        try {
        	this.createFolder(new File(sOutputFolder));     
        	String sqlStmt = "";
        	if(reportType.equals("1")){
        		if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
            	}
        		if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		BranchIdFilter = "UPPER(TBACCESSGROUPDETAIL.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";		
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		
            	}
            	if(START != "" && START.trim().length() > 0){
            			
            	} 
            	if(END != "" && END.trim().length() > 0){
            			
            	}  
            	sqlStmt = "SELECT TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,'') AS THUMBCOUNT, TBSTAFF.RECORDSTATUS FROM TBSTAFF LEFT JOIN (SELECT STAFFID, COUNT(*) THUMBCOUNT FROM TBSTAFFTHUMB GROUP BY STAFFID) THUMB ON (THUMB.STAFFID = TBSTAFF.STAFFID) " +
            			"LEFT JOIN TBACCESSGROUPDETAIL ON (TBACCESSGROUPDETAIL.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID) " + 
            			"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
            } else if (reportType.equals("2")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		BranchIdFilter = "UPPER(TBATTENDANCE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		DeviceIdFilter = "UPPER(TBATTENDANCE.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";
            	}
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBATTENDANCE.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBATTENDANCE.ATTENDANCEDATE <= " + END + "";
            	} 
            	
            	sqlStmt = "SELECT TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " + 
						"TBATTENDANCE.DEVICEID, TBDEVICE.DEVICENAME, " +				
						"TBATTENDANCE.BRANCHID, TBBRANCH.BRANCHNAME, " +
						"TBSTAFF.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, " +
						"TBATTENDANCE.ATTENDANCEDATE, " +
						"TBATTENDANCE.ATTENDANCETIME, " +
						"TBATTENDANCE.INOUT, " +
						"TBATTENDANCE.DESCRIPTION, " +
						"TBATTENDANCE.REASONID, TBREASON.REASONNAME " +
						"FROM TBATTENDANCE  " +
						"LEFT JOIN TBSTAFF ON (TBATTENDANCE.STAFFID = TBSTAFF.STAFFID AND TBSTAFF.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBDEVICE ON (TBATTENDANCE.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBBRANCH ON (TBATTENDANCE.BRANCHID = TBBRANCH.BRANCHID AND TBBRANCH.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID AND TBDEPARTMENT.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBREASON ON (TBATTENDANCE.REASONID = TBREASON.REASONID AND TBREASON.RECORDSTATUS = 'A') " +
						"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
						"ORDER BY TBATTENDANCE.ATTENDANCEDATE,TBATTENDANCE.ATTENDANCETIME ";
            } else if (reportType.equals("3")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
        			
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		
            	} 
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		DeviceIdFilter = "UPPER(TBDEVICEDOWNTIME.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";
            	}
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBDEVICEDOWNTIME.EVENTDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBDEVICEDOWNTIME.EVENTDATE <= " + END + "";
            	} 
            	
            	sqlStmt = "SELECT TBDEVICEDOWNTIME.DEVICEID, TBDEVICE.DEVICENAME, " + 				
						"TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME, " + 
						"TBDEVICEDOWNTIME.STATUS " +  
						"FROM TBDEVICEDOWNTIME " + 
						"LEFT JOIN TBDEVICE ON (TBDEVICEDOWNTIME.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " +
						"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
						"ORDER BY TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME";
            } else if (reportType.equals("4")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(v_attendance_single.STAFFID) = '" + STAFFID.toUpperCase() + "'";
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            			
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBDEPARTMENT.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            			
            	} 
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "v_attendance_single.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "v_attendance_single.ATTENDANCEDATE <= " + END + "";
            	}
            	
            	/*sqlStmt = "" + 
            	"SELECT " + 
            	"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +  
            	"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " +
            		"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
            		"FROM ( " +
            			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " + 
            			"ID, TBATTENDANCE.STAFFID, " +
            			"CASE " +
            			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +
            			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +   
            			"END AS DESCRIPTION, " +
            			"TBATTENDANCE.ATTENDANCEDATE, " +
            			"TBATTENDANCE.ATTENDANCETIME " +
            			"FROM TBATTENDANCE " +
            		") TABLE1 " +
            		"WHERE RowNum = 1 " +
            	") TABLE2 " +
            	"LEFT JOIN ( " +
	            	"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
	            	"FROM ( " +
	            		"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
	            		"ID, TBATTENDANCE.STAFFID, " + 
	            		"CASE " +
	            		  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
	            		  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
	            		"END AS DESCRIPTION, " +
	            		"TBATTENDANCE.ATTENDANCEDATE, " + 
	            		"TBATTENDANCE.ATTENDANCETIME " +
	            		"FROM TBATTENDANCE " +
	            	") TABLE1 " +
	            	"WHERE RowNum = 1 " +
                ") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
                "LEFT JOIN( " + 
            		"SELECT STAFFID , ATTENDANCEDATE, " +   
            		"STUFF( " + 
            			"(SELECT ','+ DESCRIPTION FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,1, '' " + 
            		") DESCRIPTION " + 
            		"FROM TBATTENDANCE b " + 
            		"GROUP BY STAFFID, ATTENDANCEDATE " + 
            	") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +  
            	"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " +
            	"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +
            	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
            	"GROUP BY TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION ";
            	*/
            	/*sqlStmt = "" + 
            	"SELECT " + 
            	"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +  
            	"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " +
            		"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
            		"FROM ( " +
            			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " + 
            			"ID, TBATTENDANCE.STAFFID, " +
            			"CASE " +
            			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +
            			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +   
            			"END AS DESCRIPTION, " +
            			"TBATTENDANCE.ATTENDANCEDATE, " +
            			"TBATTENDANCE.ATTENDANCETIME " +
            			"FROM TBATTENDANCE " +
            		") TABLE1 " +
            		"WHERE RowNum = 1 " +
            	") TABLE2 " +
            	"LEFT JOIN ( " +
	            	"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
	            	"FROM ( " +
	            		"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
	            		"ID, TBATTENDANCE.STAFFID, " + 
	            		"CASE " +
	            		  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
	            		  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
	            		"END AS DESCRIPTION, " +
	            		"TBATTENDANCE.ATTENDANCEDATE, " + 
	            		"TBATTENDANCE.ATTENDANCETIME " +
	            		"FROM TBATTENDANCE " +
	            	") TABLE1 " +
	            	"WHERE RowNum = 1 " +
                ") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
                "LEFT JOIN( " + 
            		"SELECT STAFFID , ATTENDANCEDATE, " +   
            		"STUFF( " + 
            			"(SELECT " +
            			"CASE " +
            				"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " + 
            				"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " + 
            			"END " +
            			"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " + 
            		") DESCRIPTION " + 
            		"FROM TBATTENDANCE b " + 
            		"GROUP BY STAFFID, ATTENDANCEDATE " + 
            	") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +  
            	"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " +
            	"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +
            	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
            	"GROUP BY TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION ";*/
            	/*sqlStmt = "" + 
	    			"SELECT " +  
	    			"v_attendance_single.STAFFID, " +
	    			"TBSTAFF.STAFFNAME, " + 
	    			"TBDEPARTMENT.DEPARTMENTID, " + 
	    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
	    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
	    			"v_attendance_single.ATTENDANCEDATE, " +
	    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
	    			"CASE v_attendance_single.ATTENDANCETYPE " + 
	    				"WHEN 'C' THEN '000000' " +
	    				"ELSE TABLE2.ATTENDANCETIME  " +
	    			"END AS CHECKOUT, " +
	    			"v_attendance_single.ATTENDANCETYPE " +
	    			"FROM v_attendance_single " +
	    			"LEFT JOIN ( " +
	    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
	    				"FROM ( " +  
	    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
	    					"ID, TBATTENDANCE.STAFFID, " +  
	    					"CASE " +     
	    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
	    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
	    					"END AS DESCRIPTION, " +  
	    					"TBATTENDANCE.ATTENDANCEDATE, " +  
	    					"TBATTENDANCE.ATTENDANCETIME " +  
	    					"FROM TBATTENDANCE " +  
	    				") TABLE1 " +  
	    				"WHERE RowNum = 1 " +
	    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
	    			"LEFT JOIN( " +  
	    				"SELECT STAFFID , ATTENDANCEDATE, " +    
	    				"STUFF( " +  
	    					"(SELECT " + 
	    					"CASE " + 
	    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
	    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
	    					"END " + 
	    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
	    				")  DESCRIPTION   " +
	    				"FROM TBATTENDANCE b  " + 
	    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
	    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
	    			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
	    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
	    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
	    		"ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID";*/
            	sqlStmt = "" + 
    	    			"SELECT " +  
    	    			"v_attendance_single.STAFFID, " +
    	    			"TBSTAFF.STAFFNAME, " + 
    	    			"TBDEPARTMENT.DEPARTMENTID, " + 
    	    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
    	    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    	    			"v_attendance_single.ATTENDANCEDATE, " +
    	    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    	    			"v_attendance_duration_single.WORKHOUR, " + 
    	    			"v_attendance_duration_single.WORKMIN, " + 
    	    			"v_attendance_duration_single.WORKSEC, " + 
    	    			"CASE v_attendance_single.ATTENDANCETYPE " + 
    	    				"WHEN 'C' THEN '000000' " +
    	    				"ELSE TABLE2.ATTENDANCETIME  " +
    	    			"END AS CHECKOUT, " +
    	    			"v_attendance_single.ATTENDANCETYPE " +
    	    			"FROM v_attendance_single " +
    	    			"LEFT JOIN ( " +
    	    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    	    				"FROM ( " +  
    	    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    	    					"ID, TBATTENDANCE.STAFFID, " +  
    	    					"CASE " +     
    	    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    	    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    	    					"END AS DESCRIPTION, " +  
    	    					"TBATTENDANCE.ATTENDANCEDATE, " +  
    	    					"TBATTENDANCE.ATTENDANCETIME " +  
    	    					"FROM TBATTENDANCE " +  
    	    				") TABLE1 " +  
    	    				"WHERE RowNum = 1 " +
    	    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    	    			"LEFT JOIN( " +  
    	    				"SELECT STAFFID , ATTENDANCEDATE, " +    
    	    				"STUFF( " +  
    	    					"(SELECT " + 
    	    					"CASE " + 
    	    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    	    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    	    					"END " + 
    	    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    	    				")  DESCRIPTION   " +
    	    				"FROM TBATTENDANCE b  " + 
    	    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
    	    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    	    			"LEFT JOIN v_attendance_duration_single ON (v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE) " +
    	    			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    	    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
    	    		"ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID";
            } else if (reportType.equals("5")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(v_attendance_single.STAFFID) = '" + STAFFID.toUpperCase() + "'";
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            			
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBDEPARTMENT.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            			
            	} 
            	if(WORKINGOPTION != "" && WORKINGOPTION != "0" && WORKINGOPTION.trim().length() > 0){
            		if(WORKINGOPTION == "1" || WORKINGOPTION.equals("1")){
            			WorkOptionFilter = "v_attendance_duration_single.WORKHOUR >= 9";
            		} else if (WORKINGOPTION == "2" || WORKINGOPTION.equals("2")){
            			WorkOptionFilter = "v_attendance_duration_single.WORKHOUR < 9";
            		}
            	} 
            	if(SHIFTOPTION != "" && SHIFTOPTION != "0" && SHIFTOPTION.trim().length() > 0){
            		if(SHIFTOPTION == "1" || SHIFTOPTION.equals("1")){
            			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '060000' AND v_attendance_single.CLOCKINTIME < '080000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
            		} else if (SHIFTOPTION == "2" || SHIFTOPTION.equals("2")){
            			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '080000' AND v_attendance_single.CLOCKINTIME < '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
            		} else if (SHIFTOPTION == "3" || SHIFTOPTION.equals("3")){
            			ShiftOptionFilter = "v_attendance_single.CLOCKINTIME >= '103000' AND v_attendance_single.ATTENDANCETYPE <> 'C'";
            		}
            	} 
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "v_attendance_single.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "v_attendance_single.ATTENDANCEDATE <= " + END + "";
            	}
            	
            	/*sqlStmt = "" + 
            	"SELECT " + 
            	"TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +  
            	"MIN(TABLE2MIN.ATTENDANCETIME) AS CHECKIN, MAX(TABLE2.ATTENDANCETIME) AS CHECKOUT FROM ( " +
            		"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
            		"FROM ( " +
            			"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " + 
            			"ID, TBATTENDANCE.STAFFID, " +
            			"CASE " +
            			  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +
            			  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +   
            			"END AS DESCRIPTION, " +
            			"TBATTENDANCE.ATTENDANCEDATE, " +
            			"TBATTENDANCE.ATTENDANCETIME " +
            			"FROM TBATTENDANCE " +
            		") TABLE1 " +
            		"WHERE RowNum = 1 " +
            	") TABLE2 " +
            	"LEFT JOIN ( " +
            	"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +
            	"FROM ( " +
            		"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME) as RowNum, " + 
            		"ID, TBATTENDANCE.STAFFID, " + 
            		"CASE " +
            		  "WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " + 
            		  "WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " + 
            		"END AS DESCRIPTION, " +
            		"TBATTENDANCE.ATTENDANCEDATE, " + 
            		"TBATTENDANCE.ATTENDANCETIME " +
            		"FROM TBATTENDANCE " +
            	") TABLE1 " +
            	"WHERE RowNum = 1 " +
            	") TABLE2MIN ON(TABLE2.STAFFID = TABLE2MIN.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE2MIN.ATTENDANCEDATE) " +
            	"LEFT JOIN( " + 
        			"SELECT STAFFID , ATTENDANCEDATE, " +   
        			"STUFF( " + 
        			"(SELECT " +
	        			"CASE " +
	        				"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " + 
	        				"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " + 
	        			"END " +
	        			"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " + 
	        		") DESCRIPTION " + 
        			"FROM TBATTENDANCE b " + 
        			"GROUP BY STAFFID, ATTENDANCEDATE " + 
        		") TABLE3 ON(TABLE2.STAFFID = TABLE3.STAFFID AND TABLE2.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +  
            	"LEFT JOIN TBSTAFF ON (TABLE2.STAFFID = TBSTAFF.STAFFID) " +
            	"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +
            	"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
            	"GROUP BY TABLE2.STAFFID, TBSTAFF.STAFFNAME, TBDEPARTMENT.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, TABLE2.ATTENDANCEDATE, TABLE3.DESCRIPTION " + 
             	"ORDER BY TBDEPARTMENT.DEPARTMENTID, TABLE2.STAFFID, TABLE2.ATTENDANCEDATE ";;*/
            	/*sqlStmt = "" + 
	    			"SELECT " +  
	    			"v_attendance_single.STAFFID, " +
	    			"TBSTAFF.STAFFNAME, " + 
	    			"TBDEPARTMENT.DEPARTMENTID, " + 
	    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
	    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
	    			"v_attendance_single.ATTENDANCEDATE, " +
	    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
	    			"CASE v_attendance_single.ATTENDANCETYPE " + 
	    				"WHEN 'C' THEN '000000' " +
	    				"ELSE TABLE2.ATTENDANCETIME  " +
	    			"END AS CHECKOUT, " +
	    			"v_attendance_single.ATTENDANCETYPE " +
	    			"FROM v_attendance_single " +
	    			"LEFT JOIN ( " +
	    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
	    				"FROM ( " +  
	    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
	    					"ID, TBATTENDANCE.STAFFID, " +  
	    					"CASE " +     
	    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
	    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
	    					"END AS DESCRIPTION, " +  
	    					"TBATTENDANCE.ATTENDANCEDATE, " +  
	    					"TBATTENDANCE.ATTENDANCETIME " +  
	    					"FROM TBATTENDANCE " +  
	    				") TABLE1 " +  
	    				"WHERE RowNum = 1 " +
	    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
	    			"LEFT JOIN( " +  
	    				"SELECT STAFFID , ATTENDANCEDATE, " +    
	    				"STUFF( " +  
	    					"(SELECT " + 
	    					"CASE " + 
	    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
	    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
	    					"END " + 
	    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
	    				")  DESCRIPTION   " +
	    				"FROM TBATTENDANCE b  " + 
	    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
	    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
	    			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
	    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
	    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
	    		"ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID";*/
            	sqlStmt = "" + 
    	    			"SELECT " +  
    	    			"v_attendance_single.STAFFID, " +
    	    			"TBSTAFF.STAFFNAME, " + 
    	    			"TBDEPARTMENT.DEPARTMENTID, " + 
    	    			"TBDEPARTMENT.DEPARTMENTNAME, " + 
    	    			"REPLACE(TABLE3.DESCRIPTION,',',' ') AS DESCRIPTION, " +    
    	    			"v_attendance_single.ATTENDANCEDATE, " +
    	    			"v_attendance_single.CLOCKINTIME AS CHECKIN, " +
    	    			"v_attendance_duration_single.WORKHOUR, " + 
    	    			"v_attendance_duration_single.WORKMIN, " + 
    	    			"v_attendance_duration_single.WORKSEC, " +
    	    			"CASE v_attendance_single.ATTENDANCETYPE " + 
    	    				"WHEN 'C' THEN '000000' " +
    	    				"ELSE TABLE2.ATTENDANCETIME  " +
    	    			"END AS CHECKOUT, " +
    	    			"v_attendance_single.ATTENDANCETYPE " +
    	    			"FROM v_attendance_single " +
    	    			"LEFT JOIN ( " +
    	    				"SELECT STAFFID, DESCRIPTION, ATTENDANCEDATE, ATTENDANCETIME " +  
    	    				"FROM ( " +  
    	    					"SELECT ROW_NUMBER() over (Partition by TBATTENDANCE.STAFFID, TBATTENDANCE.ATTENDANCEDATE order by TBATTENDANCE.ATTENDANCEDATE DESC,TBATTENDANCE.ATTENDANCETIME DESC) as RowNum, " +   
    	    					"ID, TBATTENDANCE.STAFFID, " +  
    	    					"CASE " +     
    	    						"WHEN TBATTENDANCE.REASONID = '-' THEN TBATTENDANCE.DESCRIPTION " +     
    	    						"WHEN TBATTENDANCE.REASONID <> '-' THEN TBATTENDANCE.REASONID + ' - ' + TBATTENDANCE.DESCRIPTION " +     
    	    					"END AS DESCRIPTION, " +  
    	    					"TBATTENDANCE.ATTENDANCEDATE, " +  
    	    					"TBATTENDANCE.ATTENDANCETIME " +  
    	    					"FROM TBATTENDANCE " +  
    	    				") TABLE1 " +  
    	    				"WHERE RowNum = 1 " +
    	    			") TABLE2 ON (TABLE2.STAFFID = v_attendance_single.STAFFID AND TABLE2.ATTENDANCEDATE = v_attendance_single.ATTENDANCEDATE) " +
    	    			"LEFT JOIN( " +  
    	    				"SELECT STAFFID , ATTENDANCEDATE, " +    
    	    				"STUFF( " +  
    	    					"(SELECT " + 
    	    					"CASE " + 
    	    						"WHEN a.REASONID = '-' THEN a.DESCRIPTION + ',' " +  
    	    						"WHEN a.REASONID <> '-' THEN a.REASONID + ' - ' + a.DESCRIPTION + ',' " +  
    	    					"END " + 
    	    					"FROM TBATTENDANCE a WHERE b.STAFFID = a.STAFFID AND  b.ATTENDANCEDATE = a.ATTENDANCEDATE FOR XML PATH('')),1 ,0, '' " +  
    	    				")  DESCRIPTION   " +
    	    				"FROM TBATTENDANCE b  " + 
    	    				"GROUP BY STAFFID, ATTENDANCEDATE " +  
    	    			") TABLE3 ON(v_attendance_single.STAFFID = TABLE3.STAFFID AND v_attendance_single.ATTENDANCEDATE = TABLE3.ATTENDANCEDATE) " +   
    	    			"LEFT JOIN v_attendance_duration_single ON (v_attendance_single.STAFFID = v_attendance_duration_single.STAFFID AND v_attendance_single.ATTENDANCEDATE = v_attendance_duration_single.ATTENDANCEDATE) " +
    	    			"LEFT JOIN TBSTAFF ON (v_attendance_single.STAFFID = TBSTAFF.STAFFID) " +  
    	    			"LEFT JOIN TBDEPARTMENT ON (TBSTAFF.DEPARTMENTID = TBDEPARTMENT.DEPARTMENTID) " +  
    	    		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + WorkOptionFilter + " AND " + ShiftOptionFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
    	    		"ORDER BY TBDEPARTMENT.DEPARTMENTID, v_attendance_single.STAFFID";
	    		
            }
            PreparedStatement pSelectStmt = this.connection.prepareStatement(sqlStmt);
            ResultSet srs = pSelectStmt.executeQuery();
            
            if(reportType.equals("1")){
            	outputName = "Staff Register Report " + sExportId + ".pdf";
            	reportName = "Staff Register Report";
          	} else if(reportType.equals("2")){
          		outputName = "Attendance Report " + sExportId + ".pdf";
          		reportName = "Attendance Report";
          	} else if(reportType.equals("3")){
          		outputName = "Device Downtime Report " + sExportId + ".pdf";
          		reportName = "Device Downtime Report";
          	} else if(reportType.equals("4")){
          		outputName = "General Report " + sExportId + ".pdf";
          		reportName = "General Report";
          	} else if(reportType.equals("5")){
          		outputName = "In Out Report " + sExportId + ".pdf";
          		reportName = "In Out Report";
          	} else {
          		outputName = "REPORT" + sExportId + ".pdf";
          		reportName = "REPORT";
          	}
            
            String pdfLocation = "-";
            int iTotalPages = this.CountPagesTransactionReport(reportType, STAFFID, STAFFNAME, BRANCHID, DEPARTMENTID, DEVICEID, START, END);
            Document document = new Document();
            if(reportType.equals("2") || reportType.equals("5")){
            	document = new Document(PageSize.A4.rotate());
          	} else {
          		document = new Document();
          	}
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(sOutputFolder + outputName));
            
            HeaderFooterPageEvent event = new HeaderFooterPageEvent(iTotalPages);
            writer.setPageEvent(event);
            
        	document.open();
  
        	document.addTitle(reportName);
        	document.addSubject(reportName);
        	document.addKeywords(reportType + ", " + STAFFID + ", " + BRANCHID);
        	document.addAuthor("Radiant Global");
        	document.addCreator("Radiant Global");
        	
            int colOddEven = 0; 
            int rowNum = 0;
            int numOfRowPerPage = 38;
            if(reportType.equals("2")){
            	numOfRowPerPage = 24;
          	} else if(reportType.equals("5")){
            	numOfRowPerPage = 23;
          	} else {
          		numOfRowPerPage = 38;
          	}
            String currentSeparator = "";
            boolean firstLoopOnLocation = true;
            
            Paragraph paragraphTitle = new Paragraph();
            Paragraph preface = new Paragraph();
            PdfPTable table = new PdfPTable(new float[] { 1 });
            PdfPCell cell = new PdfPCell();
            
            while (srs.next()) {
            	if(reportType.equals("1")){
            		String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
    				String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
    				if(staff_name.length() > 52){
    					staff_name = staff_name.substring(0, 52);
    	           	}	 
    				String registered = "No";
    				if(srs.getInt("THUMBCOUNT") > 0){
    					registered = "Yes";
    				}
    				String status = (srs.getString("RECORDSTATUS") == null)?"-":srs.getString("RECORDSTATUS");
    				String separator = "-";
    				
    				if(!currentSeparator.equals(separator)){
    	           		if(rowNum % numOfRowPerPage == 0){
    	           			firstLoopOnLocation = true;
    	           		}
    	           		
    	           		currentSeparator = separator;
    	           		colOddEven = 0; 
    	                rowNum = 0;
    	                
    	                if(firstLoopOnLocation){
    	                	firstLoopOnLocation = false;
    	                } else {
    	                	document.add(table);
    	            		document.newPage();	
    	                }
    	           	}
    				
    				if(rowNum % numOfRowPerPage == 0){    
                		paragraphTitle = new Paragraph();
        	            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
        	            paragraphTitle.setFont(catFont);
        	            Chunk chunk = new Chunk(" Attendance System ");
        	            paragraphTitle.add(chunk);
        	            document.add(paragraphTitle);
        	            
        	        	preface = new Paragraph();
        	            addEmptyLine(preface, 1);
        	            preface.add(new Paragraph(reportName, catFont));            
        	            addEmptyLine(preface, 1);
        	            //preface.add(new Paragraph("Store                : " + store_code + " (" + store_name + ")", smallBold));
        	            //preface.add(new Paragraph("Location          : " + oLocationCode, smallBold));  
        	            preface.add(new Paragraph("From - To        : " + START + " - " + END, smallBold));  
        	            preface.add(new Paragraph("Printed By       : " + printed_by , smallBold));  
        	            preface.add(new Paragraph("Printed Date   : " + sPrintDate, smallBold));  
        	            addEmptyLine(preface, 1);  
        	            document.add(preface);
        	            
        	            table = new PdfPTable(new float[] { 2, 4, 1, 1 });
        	            table.setWidthPercentage(100f);
        	            table.getDefaultCell().setPadding(2);
        	            table.getDefaultCell().setUseAscender(true);
        	            table.getDefaultCell().setUseDescender(true);
        	            table.getDefaultCell().setColspan(1);            
        	            table.getDefaultCell().setBackgroundColor(BaseColor.DARK_GRAY);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);    
        	            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER); 
        	            cell = table.getDefaultCell();
        	            
        	            cell.setPhrase(new Phrase("Staff Id",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Staff Name",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Register",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Status",tableHeaderFont));
        	            table.addCell(cell);

        	            table.getDefaultCell().setBackgroundColor(null);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        	            table.getDefaultCell().setPadding(4);  
                	}
                		            
                	colOddEven++;
                	rowNum++;
                	
                	if(colOddEven % 2 == 0){
                		table.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                	} else {
                		table.getDefaultCell().setBackgroundColor(null);
                	}   
                	
                	cell.setPhrase(new Phrase(staff_id.toUpperCase(),small));	           	
    	           	table.addCell(cell);
    	            cell.setPhrase(new Phrase(staff_name,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(registered.toUpperCase(),small));
    	            table.addCell(cell);
    	           	cell.setPhrase(new Phrase(status,small));
    	            table.addCell(cell);
                	            	
                	if(rowNum % numOfRowPerPage == 0){
                		document.add(table);
                		document.newPage();		
                	}
            	} else if(reportType.equals("2")){
					String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
					String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
					if(staff_name.length() > 28){
    					staff_name = staff_name.substring(0, 28);
    	           	}
					String device_id = (srs.getString("DEVICEID") == null)?"-":srs.getString("DEVICEID");
					String device_name = (srs.getString("DEVICENAME") == null)?"-":srs.getString("DEVICENAME");
					String branch_id = (srs.getString("BRANCHID") == null)?"-":srs.getString("BRANCHID");
					String branch_name = (srs.getString("BRANCHNAME") == null)?"-":srs.getString("BRANCHNAME");
					if(branch_name.length() > 10){
						branch_name = branch_name.substring(0, 10);
    	           	}
					String atendance_date = (srs.getString("ATTENDANCEDATE") == null)?"-":srs.getString("ATTENDANCEDATE");
					String atendance_time = (srs.getString("ATTENDANCETIME") == null)?"0":srs.getString("ATTENDANCETIME");
					
					String sAttDate = atendance_date;
	   	           	String sAttTime = atendance_time;
	   	           	if(atendance_date != "-" && atendance_time != "0"){
	   	           		 sAttDate = util.DateFormatConverter.format3(atendance_date + atendance_time, "yyyyMMddHHmmss", "dd-MM-yyyy");
	   	           		 sAttTime = util.DateFormatConverter.format3(atendance_date + atendance_time, "yyyyMMddHHmmss", "HH:mm:ss");
	   	           	}
					
					String in_out = (srs.getString("INOUT") == null)?"-":srs.getString("INOUT");
					if(in_out.equals("A")) { 
    	           		in_out = "IN";	
    	           	 } else if(in_out.equals("B")) { 
    	           		in_out = "OUT";
    	           	 } else { 
    	           	 	in_out = "-";
    	           	 }
					String reason_id = (srs.getString("REASONID") == null)?"-":srs.getString("REASONID");
					String reason_name = (srs.getString("REASONNAME") == null)?"-":srs.getString("REASONNAME");
					String description = (srs.getString("DESCRIPTION") == null)?"-":srs.getString("DESCRIPTION");
					if(description == null || description.toString().trim().equals("")) { 
						if(reason_id == null || reason_id == "-") {
							description = "-";
						} else {
							description = reason_name;
						}
					} else {
						description = reason_id + " - " + description;
					}
					if(description.length() > 28){
						description = description.substring(0, 28);
    	           	}	
    				String separator = "-";
    				
    				if(!currentSeparator.equals(separator)){
    	           		if(rowNum % numOfRowPerPage == 0){
    	           			firstLoopOnLocation = true;
    	           		}
    	           		
    	           		currentSeparator = separator;
    	           		colOddEven = 0; 
    	                rowNum = 0;
    	                
    	                if(firstLoopOnLocation){
    	                	firstLoopOnLocation = false;
    	                } else {
    	                	document.add(table);
    	            		document.newPage();	
    	                }
    	           	}
    				
    				if(rowNum % numOfRowPerPage == 0){    
                		paragraphTitle = new Paragraph();
        	            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
        	            paragraphTitle.setFont(catFont);
        	            Chunk chunk = new Chunk(" Attendance System ");
        	            paragraphTitle.add(chunk);
        	            document.add(paragraphTitle);
        	            
        	        	preface = new Paragraph();
        	            addEmptyLine(preface, 1);
        	            preface.add(new Paragraph(reportName, catFont));            
        	            addEmptyLine(preface, 1);
        	            //preface.add(new Paragraph("Store                : " + store_code + " (" + store_name + ")", smallBold));
        	            //preface.add(new Paragraph("Location          : " + oLocationCode, smallBold));  
        	            preface.add(new Paragraph("From - To        : " + START + " - " + END, smallBold));  
        	            preface.add(new Paragraph("Printed By       : " + printed_by , smallBold));  
        	            preface.add(new Paragraph("Printed Date   : " + sPrintDate, smallBold));
        	            addEmptyLine(preface, 1);  
        	            document.add(preface);
        	            
        	            table = new PdfPTable(new float[] { 2, 5, /*1, 2,*/ 1, 2, 1, 1, 6 });
        	            table.setWidthPercentage(100f);
        	            table.getDefaultCell().setPadding(2);
        	            table.getDefaultCell().setUseAscender(true);
        	            table.getDefaultCell().setUseDescender(true);
        	            table.getDefaultCell().setColspan(1);            
        	            table.getDefaultCell().setBackgroundColor(BaseColor.DARK_GRAY);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);    
        	            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER); 
        	            cell = table.getDefaultCell();
        	            
        	            cell.setPhrase(new Phrase("Staff Id",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Staff Name",tableHeaderFont));
        	            table.addCell(cell);
        	            //cell.setPhrase(new Phrase("Device Id",tableHeaderFont));
        	            //table.addCell(cell);
        	            //cell.setPhrase(new Phrase("Device Name",tableHeaderFont));
        	            //table.addCell(cell);
        	            cell.setPhrase(new Phrase("Branch Id",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Date",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Time",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("In Out",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Remark",tableHeaderFont));
        	            table.addCell(cell);

        	            table.getDefaultCell().setBackgroundColor(null);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        	            table.getDefaultCell().setPadding(4);  
                	}
                		            
                	colOddEven++;
                	rowNum++;
                	
                	if(colOddEven % 2 == 0){
                		table.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                	} else {
                		table.getDefaultCell().setBackgroundColor(null);
                	}   
                	
                	cell.setPhrase(new Phrase(staff_id.toUpperCase(),small));	           	
    	           	table.addCell(cell);
    	            cell.setPhrase(new Phrase(staff_name,small));
    	            table.addCell(cell);
    	            //cell.setPhrase(new Phrase(device_id.toUpperCase(),small));
    	            //table.addCell(cell);
    	           	//cell.setPhrase(new Phrase(device_name,small));
    	            //table.addCell(cell);
    	            cell.setPhrase(new Phrase(branch_id.toUpperCase(),small));	           	
    	           	table.addCell(cell);
    	            cell.setPhrase(new Phrase(sAttDate,small));
    	            table.addCell(cell);
    	           	cell.setPhrase(new Phrase(sAttTime,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(in_out,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(description,small));
    	            table.addCell(cell);
    	            	
                	if(rowNum % numOfRowPerPage == 0){
                		document.add(table);
                		document.newPage();		
                	}
            	} else if(reportType.equals("3")){
					String device_id = (srs.getString("DEVICEID") == null)?"-":srs.getString("DEVICEID");
					String device_name = (srs.getString("DEVICENAME") == null)?"-":srs.getString("DEVICENAME");
					String event_date = (srs.getString("EVENTDATE") == null)?"-":srs.getString("EVENTDATE");
					String event_time = (srs.getString("EVENTTIME") == null)?"-":srs.getString("EVENTTIME");
					
					String sEventDate = event_date;
					String sEventTime = event_time;
					if(event_date != "-" && event_time != "-"){
						sEventDate = util.DateFormatConverter.format3(event_date + event_time, "yyyyMMddHHmmss", "dd-MM-yyyy");
						sEventTime = util.DateFormatConverter.format3(event_date + event_time, "yyyyMMddHHmmss", "HH:mm:ss");
					}
					
					String status = (srs.getString("STATUS") == null)?"-":srs.getString("STATUS");
    				String separator = "-";
    				
    				if(!currentSeparator.equals(separator)){
    	           		if(rowNum % numOfRowPerPage == 0){
    	           			firstLoopOnLocation = true;
    	           		}
    	           		
    	           		currentSeparator = separator;
    	           		colOddEven = 0; 
    	                rowNum = 0;
    	                
    	                if(firstLoopOnLocation){
    	                	firstLoopOnLocation = false;
    	                } else {
    	                	document.add(table);
    	            		document.newPage();	
    	                }
    	           	}
    				
    				if(rowNum % numOfRowPerPage == 0){    
                		paragraphTitle = new Paragraph();
        	            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
        	            paragraphTitle.setFont(catFont);
        	            Chunk chunk = new Chunk(" Attendance System ");
        	            paragraphTitle.add(chunk);
        	            document.add(paragraphTitle);
        	            
        	        	preface = new Paragraph();
        	            addEmptyLine(preface, 1);
        	            preface.add(new Paragraph(reportName, catFont));            
        	            addEmptyLine(preface, 1);
        	            //preface.add(new Paragraph("Store                : " + store_code + " (" + store_name + ")", smallBold));
        	            //preface.add(new Paragraph("Location          : " + oLocationCode, smallBold));  
        	            preface.add(new Paragraph("From - To        : " + START + " - " + END, smallBold));  
        	            preface.add(new Paragraph("Printed By       : " + printed_by , smallBold));  
        	            preface.add(new Paragraph("Printed Date   : " + sPrintDate, smallBold));
        	            addEmptyLine(preface, 1);  
        	            document.add(preface);
        	            
        	            table = new PdfPTable(new float[] { 2, 3, 1, 1, 1 });
        	            table.setWidthPercentage(100f);
        	            table.getDefaultCell().setPadding(2);
        	            table.getDefaultCell().setUseAscender(true);
        	            table.getDefaultCell().setUseDescender(true);
        	            table.getDefaultCell().setColspan(1);            
        	            table.getDefaultCell().setBackgroundColor(BaseColor.DARK_GRAY);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);    
        	            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER); 
        	            cell = table.getDefaultCell();
        	            
        	            cell.setPhrase(new Phrase("Device Id",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Device Name",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Date",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Time",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Status",tableHeaderFont));
        	            table.addCell(cell);
        	            
        	            table.getDefaultCell().setBackgroundColor(null);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        	            table.getDefaultCell().setPadding(4);  
                	}
                		            
                	colOddEven++;
                	rowNum++;
                	
                	if(colOddEven % 2 == 0){
                		table.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                	} else {
                		table.getDefaultCell().setBackgroundColor(null);
                	}   
                	
                	cell.setPhrase(new Phrase(device_id.toUpperCase(),small));	           	
    	           	table.addCell(cell);
    	            cell.setPhrase(new Phrase(device_name,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(sEventDate,small));
    	            table.addCell(cell);
    	           	cell.setPhrase(new Phrase(sEventTime,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(status,small));
    	            table.addCell(cell);
                	            	
                	if(rowNum % numOfRowPerPage == 0){
                		document.add(table);
                		document.newPage();		
                	}
            	} else if(reportType.equals("4")){
					String attendance_date = (srs.getString("ATTENDANCEDATE") == null)?"-":srs.getString("ATTENDANCEDATE");
					String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
					String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
					String department_id = (srs.getString("DEPARTMENTID") == null)?"-":srs.getString("DEPARTMENTID");
					String department_name = (srs.getString("DEPARTMENTNAME") == null)?"-":srs.getString("DEPARTMENTNAME");
					String description = (srs.getString("DESCRIPTION") == null)?"-":srs.getString("DESCRIPTION");
					String check_in = (srs.getString("CHECKIN") == null)?"-":srs.getString("CHECKIN");
					String check_out = (srs.getString("CHECKOUT") == null)?"-":srs.getString("CHECKOUT");
					String work_hour = "";
	   	           	if(!srs.getString("ATTENDANCETYPE").equals("C")) {
	   	           		work_hour = String.format("%02d" , Integer.parseInt(srs.getString("WORKHOUR")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKMIN")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKSEC")));
	   	           	} else {
	   	           		work_hour = "-";
	   	           	}
	   	           	
					String sAttDate = attendance_date;
	   	           	String sAttTimeIn = check_in;
	   	           	String sAttTimeOut = check_out;
	   	           	if(attendance_date != "-" && check_in != "-"){
	   	           		 sAttDate = util.DateFormatConverter.format3(attendance_date + check_in, "yyyyMMddHHmmss", "dd-MM-yyyy") + " (" + util.DateFormatConverter.getWeekDay(attendance_date) + ")";
	   	           		 sAttTimeIn = util.DateFormatConverter.format3(attendance_date + check_in, "yyyyMMddHHmmss", "HH:mm:ss");	   	           		 
	   	           	}
		   	        if(attendance_date != "-" && check_out != "-"){
		   	        	sAttTimeOut = util.DateFormatConverter.format3(attendance_date + check_out, "yyyyMMddHHmmss", "HH:mm:ss");
		   	        }
					
					String time_period = "Normal(09:00-18:00)";
					String work = "";
					String late = "";
    				String separator = staff_id;
    				
    				if(!currentSeparator.equals(separator)){
    	           		if(rowNum % numOfRowPerPage == 0){
    	           			firstLoopOnLocation = true;
    	           		}
    	           		
    	           		currentSeparator = separator;
    	           		colOddEven = 0; 
    	                rowNum = 0;
    	                
    	                if(firstLoopOnLocation){
    	                	firstLoopOnLocation = false;
    	                } else {
    	                	document.add(table);
    	            		document.newPage();	
    	                }
    	           	}
    				
    				if(rowNum % numOfRowPerPage == 0){    
                		paragraphTitle = new Paragraph();
        	            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
        	            paragraphTitle.setFont(catFont);
        	            Chunk chunk = new Chunk(" Attendance System ");
        	            paragraphTitle.add(chunk);
        	            document.add(paragraphTitle);
        	            
        	        	preface = new Paragraph();
        	            addEmptyLine(preface, 1);
        	            preface.add(new Paragraph(reportName, catFont));            
        	            addEmptyLine(preface, 1);
        	            //preface.add(new Paragraph("Store                : " + store_code + " (" + store_name + ")", smallBold));
        	            preface.add(new Paragraph("Staff                 : " + staff_id + " - " + staff_name, smallBold));
        	            preface.add(new Paragraph("Department     : " + department_id + " - " + department_name, smallBold)); 
        	            preface.add(new Paragraph("From - To        : " + START + " - " + END, smallBold));  
        	            preface.add(new Paragraph("Printed By       : " + printed_by , smallBold));  
        	            preface.add(new Paragraph("Printed Date   : " + sPrintDate, smallBold));
        	            addEmptyLine(preface, 1);  
        	            document.add(preface);
        	            
        	            table = new PdfPTable(new float[] { 2, 1, 1, 2, 1, 1, 3 });
        	            table.setWidthPercentage(100f);
        	            table.getDefaultCell().setPadding(2);
        	            table.getDefaultCell().setUseAscender(true);
        	            table.getDefaultCell().setUseDescender(true);
        	            table.getDefaultCell().setColspan(1);            
        	            table.getDefaultCell().setBackgroundColor(BaseColor.DARK_GRAY);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);    
        	            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER); 
        	            cell = table.getDefaultCell();
        	            
        	            cell.setPhrase(new Phrase("Date",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("In",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Out",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Out - In Hours",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Work",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Late",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Remark",tableHeaderFont));
        	            table.addCell(cell);
        	            
        	            table.getDefaultCell().setBackgroundColor(null);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        	            table.getDefaultCell().setPadding(4);  
                	}
                		            
                	colOddEven++;
                	rowNum++;
                	
                	if(colOddEven % 2 == 0){
                		table.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                	} else {
                		table.getDefaultCell().setBackgroundColor(null);
                	}   
                	
                	cell.setPhrase(new Phrase(sAttDate,small));	           	
    	           	table.addCell(cell);
    	            cell.setPhrase(new Phrase(sAttTimeIn,small));
    	            table.addCell(cell);
    	           	cell.setPhrase(new Phrase(sAttTimeOut,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(work_hour,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(work,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(late,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(description,small));
    	            table.addCell(cell);
                	            	
                	if(rowNum % numOfRowPerPage == 0){
                		document.add(table);
                		document.newPage();		
                	}
            	} else if(reportType.equals("5")){
					String attendance_date = (srs.getString("ATTENDANCEDATE") == null)?"-":srs.getString("ATTENDANCEDATE");
					String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
					String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
					String department_id = (srs.getString("DEPARTMENTID") == null)?"-":srs.getString("DEPARTMENTID");
					String department_name = (srs.getString("DEPARTMENTNAME") == null)?"-":srs.getString("DEPARTMENTNAME");
					String description = (srs.getString("DESCRIPTION") == null)?"-":srs.getString("DESCRIPTION");
					String check_in = (srs.getString("CHECKIN") == null)?"-":srs.getString("CHECKIN");
					String check_out = (srs.getString("CHECKOUT") == null)?"-":srs.getString("CHECKOUT");
					String work_hour = "";
	   	           	if(!srs.getString("ATTENDANCETYPE").equals("C")) {
	   	           		work_hour = String.format("%02d" , Integer.parseInt(srs.getString("WORKHOUR")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKMIN")))  + ":" + String.format("%02d" , Integer.parseInt(srs.getString("WORKSEC")));
	   	           	} else {
	   	           		work_hour = "-";
	   	           	}
	   	           	
					String sAttDate = attendance_date;
	   	           	String sAttTimeIn = check_in;
	   	           	String sAttTimeOut = check_out;
	   	           	if(attendance_date != "-" && check_in != "-"){
	   	           		 sAttDate = util.DateFormatConverter.format3(attendance_date + check_in, "yyyyMMddHHmmss", "dd-MM-yyyy");
	   	           		 sAttTimeIn = util.DateFormatConverter.format3(attendance_date + check_in, "yyyyMMddHHmmss", "HH:mm:ss");	   	           		 
	   	           	}
		   	        if(attendance_date != "-" && check_out != "-"){
		   	        	sAttTimeOut = util.DateFormatConverter.format3(attendance_date + check_out, "yyyyMMddHHmmss", "HH:mm:ss");
		   	        }
		   	        	
					String time_period = "Normal(09:00-18:00)";
					String work = "";
					String late = "";
    				String separator = attendance_date;
    				
    				if(!currentSeparator.equals(separator)){
    	           		if(rowNum % numOfRowPerPage == 0){
    	           			firstLoopOnLocation = true;
    	           		}
    	           		
    	           		currentSeparator = separator;
    	           		colOddEven = 0; 
    	                rowNum = 0;
    	                
    	                if(firstLoopOnLocation){
    	                	firstLoopOnLocation = false;
    	                } else {
    	                	document.add(table);
    	            		document.newPage();	
    	                }
    	           	}
    				
    				if(rowNum % numOfRowPerPage == 0){    
                		paragraphTitle = new Paragraph();
        	            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
        	            paragraphTitle.setFont(catFont);
        	            Chunk chunk = new Chunk(" Attendance System ");
        	            paragraphTitle.add(chunk);
        	            document.add(paragraphTitle);
        	            
        	        	preface = new Paragraph();
        	            addEmptyLine(preface, 1);
        	            preface.add(new Paragraph(reportName, catFont));            
        	            addEmptyLine(preface, 1);
        	            //preface.add(new Paragraph("Store                : " + store_code + " (" + store_name + ")", smallBold));
        	            preface.add(new Paragraph("Date                : " + sAttDate, smallBold));
        	            preface.add(new Paragraph("From - To        : " + START + " - " + END, smallBold));  
        	            preface.add(new Paragraph("Printed By       : " + printed_by , smallBold));  
        	            preface.add(new Paragraph("Printed Date   : " + sPrintDate, smallBold));
        	            addEmptyLine(preface, 1);  
        	            document.add(preface);
        	            
        	            table = new PdfPTable(new float[] { 2, 1, 3, 1, 1, 2, 3 });
        	            table.setWidthPercentage(100f);
        	            table.getDefaultCell().setPadding(2);
        	            table.getDefaultCell().setUseAscender(true);
        	            table.getDefaultCell().setUseDescender(true);
        	            table.getDefaultCell().setColspan(1);            
        	            table.getDefaultCell().setBackgroundColor(BaseColor.DARK_GRAY);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);    
        	            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER); 
        	            cell = table.getDefaultCell();
        	            
        	            
        	            cell.setPhrase(new Phrase("Department",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Staff Id",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Name",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("In",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Out",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Out - In Hours",tableHeaderFont));
        	            table.addCell(cell);
        	            cell.setPhrase(new Phrase("Remark",tableHeaderFont));
        	            table.addCell(cell);
        	            table.getDefaultCell().setBackgroundColor(null);
        	            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        	            table.getDefaultCell().setPadding(4);  
                	}
                		            
                	colOddEven++;
                	rowNum++;
                	
                	if(colOddEven % 2 == 0){
                		table.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                	} else {
                		table.getDefaultCell().setBackgroundColor(null);
                	}   
                	
                	cell.setPhrase(new Phrase(department_name,small));	           	
    	           	table.addCell(cell);
    	            cell.setPhrase(new Phrase(staff_id,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(staff_name,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(sAttTimeIn,small));
    	            table.addCell(cell);
    	           	cell.setPhrase(new Phrase(sAttTimeOut,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(work_hour,small));
    	            table.addCell(cell);
    	            cell.setPhrase(new Phrase(description,small));
    	            table.addCell(cell);
                	            	
                	if(rowNum % numOfRowPerPage == 0){
                		document.add(table);
                		document.newPage();		
                	}
            	}
            }    
            document.add(table);
            document.close();
        } catch (Exception e) {
        	e.printStackTrace();
        }
    	
        return outputName;
    }
    
    public int CountPagesTransactionReport(String reportType, String STAFFID, String STAFFNAME, String BRANCHID, String DEPARTMENTID, String DEVICEID, String START, String END) throws SQLException   {
    	String StaffIdFilter = "1 = 1";
    	String StaffNameFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DepartmentIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";

	  	int numOfPage = 0;
        try {    
        	String sqlStmt = "";
        	if(reportType.equals("1")){
        		if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
            	}
        		if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		BranchIdFilter = "UPPER(TBACCESSGROUPDETAIL.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		
            	}
            	if(START != "" && START.trim().length() > 0){
            			
            	} 
            	if(END != "" && END.trim().length() > 0){
            			
            	}  
            	sqlStmt = "SELECT COUNT(*) AS NUMROW FROM TBSTAFF LEFT JOIN (SELECT STAFFID, COUNT(*) THUMBCOUNT FROM TBSTAFFTHUMB GROUP BY STAFFID) THUMB ON (THUMB.STAFFID = TBSTAFF.STAFFID) " +
            				"LEFT JOIN TBACCESSGROUPDETAIL ON (TBACCESSGROUPDETAIL.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID) " + 
            				"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
            } else if (reportType.equals("2")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		BranchIdFilter = "UPPER(TBATTENDANCE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		DeviceIdFilter = "UPPER(TBATTENDANCE.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";
            	}
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBATTENDANCE.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBATTENDANCE.ATTENDANCEDATE <= " + END + "";
            	} 
            	
            	sqlStmt = "SELECT COUNT(*) AS NUMROW " +
						"FROM TBATTENDANCE  " +
						"LEFT JOIN TBSTAFF ON (TBATTENDANCE.STAFFID = TBSTAFF.STAFFID AND TBSTAFF.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBDEVICE ON (TBATTENDANCE.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBBRANCH ON (TBATTENDANCE.BRANCHID = TBBRANCH.BRANCHID AND TBBRANCH.RECORDSTATUS = 'A') " +
						"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
            } else if (reportType.equals("3")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
        			
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            			
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		
            	} 
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            		DeviceIdFilter = "UPPER(TBDEVICEDOWNTIME.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";
            	}
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBDEVICEDOWNTIME.EVENTDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBDEVICEDOWNTIME.EVENTDATE <= " + END + "";
            	} 
            	
            	sqlStmt = "SELECT COUNT(*) AS NUMROW " +  
						"FROM TBDEVICEDOWNTIME " + 
						"LEFT JOIN TBDEVICE ON (TBDEVICEDOWNTIME.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " +
						"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
            } else if (reportType.equals("4")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            			
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            			
            	} 
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBATTENDANCE.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBATTENDANCE.ATTENDANCEDATE <= " + END + "";
            	}
            	
            	sqlStmt = "SELECT STAFFID, COUNT(*) AS NUMROW FROM (SELECT " +  
				    		"ATTENDANCEDATE, " + 
				    		"TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " +
				    		"TBSTAFF.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, " +
				    		/*"dbo.GROUP_CONCAT_D(DISTINCT TBATTENDANCE.DESCRIPTION,'') AS DESCRIPTION, " +*/
				    		"MIN(TBATTENDANCE.ATTENDANCETIME) AS CHECKIN, " +
				    		"MAX(TBATTENDANCE.ATTENDANCETIME) AS CHECKOUT " + 
				    		"FROM TBATTENDANCE " + 
				    		"LEFT JOIN TBSTAFF ON (TBSTAFF.STAFFID = TBATTENDANCE.STAFFID) " + 
				    		"LEFT JOIN TBDEPARTMENT ON (TBDEPARTMENT.DEPARTMENTID = TBSTAFF.DEPARTMENTID) " + 
				    		"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
				    		"GROUP BY ATTENDANCEDATE, " +
							"TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " + 
							"TBSTAFF.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME ) THETABLE " + 
							"GROUP BY STAFFID ";
            } else if (reportType.equals("5")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";
            	}
            	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
            		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE '%" + STAFFNAME.toUpperCase() + "%'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            			
            	} 
            	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
            		DepartmentIdFilter = "UPPER(TBSTAFF.DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
            	}
            	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
            			
            	} 
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBATTENDANCE.ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBATTENDANCE.ATTENDANCEDATE <= " + END + "";
            	}
            	
            	sqlStmt = "SELECT ATTENDANCEDATE, COUNT(*) AS NUMROW FROM (SELECT " +  
				    		"ATTENDANCEDATE, " + 
				    		"TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " +
				    		"TBSTAFF.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME, " +
				    		/*"dbo.GROUP_CONCAT_D(DISTINCT TBATTENDANCE.DESCRIPTION,'') AS DESCRIPTION, " +*/
				    		"MIN(TBATTENDANCE.ATTENDANCETIME) AS CHECKIN, " +
				    		"MAX(TBATTENDANCE.ATTENDANCETIME) AS CHECKOUT " + 
				    		"FROM TBATTENDANCE " + 
				    		"LEFT JOIN TBSTAFF ON (TBSTAFF.STAFFID = TBATTENDANCE.STAFFID) " + 
				    		"LEFT JOIN TBDEPARTMENT ON (TBDEPARTMENT.DEPARTMENTID = TBSTAFF.DEPARTMENTID) " + 
				    		"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + DepartmentIdFilter + " AND " + DeviceIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " + 
				    		"GROUP BY ATTENDANCEDATE, " +
							"TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " + 
							"TBSTAFF.DEPARTMENTID, TBDEPARTMENT.DEPARTMENTNAME ) THETABLE " + 
							"GROUP BY STAFFID ";
            }
        	
            PreparedStatement pSelectStmt = this.connection.prepareStatement(sqlStmt);
            ResultSet srs = pSelectStmt.executeQuery();
            
            int numOfRowPerPage = 38;
            if(reportType.equals("2")){
            	numOfRowPerPage = 24;
          	} else if(reportType.equals("5")){
            	numOfRowPerPage = 23;
          	} else {
          		numOfRowPerPage = 38;
          	}
            
            while (srs.next()) {
	           	int iTotalRow = srs.getInt("NUMROW");
	           	
	           	int additionalPage = iTotalRow / numOfRowPerPage;
	           	numOfPage = numOfPage + additionalPage;
	           	
	           	if(iTotalRow % numOfRowPerPage != 0){
	           		numOfPage++;
	           	}
            }
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return numOfPage;
    }
     
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
          paragraph.add(new Paragraph(" "));
        }
      }
	
	private void createFolder(File file) throws IOException {
        boolean exists = file.exists();
        if (!exists) {
            file.mkdirs();
        }
    }
    
    public String zipping(String masterPath,String filename1,String filename2){
    	String zipFile = masterPath + "ReportArchive.zip";
    	String[] srcFiles = { masterPath + filename1, masterPath + filename2};
    	try {
    		byte[] buffer = new byte[1024];
    		FileOutputStream fos = new FileOutputStream(zipFile);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		for (int i=0; i < srcFiles.length; i++) {
    			File srcFile = new File(srcFiles[i]);
    			FileInputStream fis = new FileInputStream(srcFile);
    			zos.putNextEntry(new ZipEntry(srcFile.getName()));
    			int length;
    			while ((length = fis.read(buffer)) > 0) {
    				zos.write(buffer, 0, length);
    			}
    			zos.closeEntry();
    			fis.close();
    		}
    		zos.close();
    		return "ReportArchive.zip";
    	} catch (IOException ioe) {
    		System.out.println("Error creating zip file: " + ioe);
    		return "";
    	}
    }
}