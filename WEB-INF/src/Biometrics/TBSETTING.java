package Biometrics;

import java.util.Date;

public class TBSETTING {
	private int ID;
	private String SETTINGID;
	private String SETTINGNAME;
	private String SETTINGVALUE;
	private String LASTUPDATETIMEDBY;
	private Date LASTUPDATETIMEDSTAMP;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getSETTINGID() {return SETTINGID;}
	public void setSETTINGID(String SETTINGID) {this.SETTINGID = SETTINGID;}
	
	public String getSETTINGNAME() {return SETTINGNAME;}
	public void setSETTINGNAME(String SETTINGNAME) {this.SETTINGNAME = SETTINGNAME;}
	
	public String getSETTINGVALUE() {return SETTINGVALUE;}
	public void setSETTINGVALUE(String SETTINGVALUE) {this.SETTINGVALUE = SETTINGVALUE;}
	
	public String getLASTUPDATETIMEDBY() {return LASTUPDATETIMEDBY;}
	public void setLASTUPDATETIMEDBY(String LASTUPDATETIMEDBY) {this.LASTUPDATETIMEDBY = LASTUPDATETIMEDBY;}
	
	public Date getLASTUPDATETIMEDSTAMP() {return LASTUPDATETIMEDSTAMP;}
	public void setLASTUPDATETIMEDSTAMP(Date LASTUPDATETIMEDSTAMP) {this.LASTUPDATETIMEDSTAMP = LASTUPDATETIMEDSTAMP;}
	
}
