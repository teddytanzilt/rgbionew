package Biometrics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DepartmentManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addDepartment(TBDEPARTMENT oDepartment) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBDEPARTMENT WHERE DEPARTMENTID = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oDepartment.getDEPARTMENTID());
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Department [" + oDepartment.getDEPARTMENTID()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBDEPARTMENT " + 
			"( DEPARTMENTID , DEPARTMENTNAME, RECORDSTATUS, CREATEDBY, CREATEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oDepartment.getDEPARTMENTID());
			pStmt.setString(2, oDepartment.getDEPARTMENTNAME());
			pStmt.setString(3, "A");
			pStmt.setString(4, oDepartment.getCREATEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateDepartment(TBDEPARTMENT oDepartment) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBDEPARTMENT WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oDepartment.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Department [" + oDepartment.getDEPARTMENTID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBDEPARTMENT SET "
								+ "  DEPARTMENTID = ? " 
								+ ", DEPARTMENTNAME = ? "
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oDepartment.getDEPARTMENTID());
			pStmt.setString(2, oDepartment.getDEPARTMENTNAME());
			pStmt.setString(3, oDepartment.getLASTUPDATETIMEDBY());
			pStmt.setInt(4, oDepartment.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public TBDEPARTMENT getDepartmentById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBDEPARTMENT WHERE ID = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBDEPARTMENT> alDepartment;

		alDepartment = this.getDepartmentData(pStmt);

		if (alDepartment.size() == 0)
			return null;

		return alDepartment.get(0);		
	}
	
	public TBDEPARTMENT getDepartmentByDepartmentId(String DEPARTMENTID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBDEPARTMENT WHERE UPPER(DEPARTMENTID) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, DEPARTMENTID.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBDEPARTMENT> alDepartment;

		alDepartment = this.getDepartmentData(pStmt);

		if (alDepartment.size() == 0)
			return null;

		return alDepartment.get(0);		
	}
	
	public TBDEPARTMENT getDepartmentByDepartmentName(String DEPARTMENTNAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBDEPARTMENT WHERE UPPER(DEPARTMENTNAME) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, DEPARTMENTNAME.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBDEPARTMENT> alDepartment;

		alDepartment = this.getDepartmentData(pStmt);

		if (alDepartment.size() == 0)
			return null;

		return alDepartment.get(0);		
	}
	
	public TBDEPARTMENT[] getDepartmentAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBDEPARTMENT";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    		
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum " +
    					"FROM "+tableName+" WHERE RECORDSTATUS = ?) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBDEPARTMENT> alDepartment;

		alDepartment = this.getDepartmentData(pStmt);

		if (alDepartment.size() == 0)
			return null;

		TBDEPARTMENT[] departments = new TBDEPARTMENT[alDepartment.size()];

		alDepartment.toArray(departments);

		return departments;
	}
	
	public int countDepartmentAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBDEPARTMENT WHERE RECORDSTATUS = ? ";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBDEPARTMENT[] getDepartmentByFilter(String DEPARTMENTID, String DEPARTMENTNAME, String offset) throws SQLException {
    	String DepartmentIdFilter = "1 = 1";
    	String DepartmentNameFilter = "1 = 1";
    	String itemPerPage = "30";
    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEPARTMENTNAME != "" && DEPARTMENTNAME.trim().length() > 0){
    		DepartmentNameFilter = "UPPER(DEPARTMENTNAME) LIKE ('%" + DEPARTMENTNAME.toUpperCase() + "%')";	
    	} 
    	String tableName = "TBDEPARTMENT";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum FROM "+tableName+" " +
    					"WHERE " + DepartmentIdFilter + " AND " + DepartmentNameFilter + " AND RECORDSTATUS = ? ) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        
        ArrayList<TBDEPARTMENT> alDepartment = new ArrayList<TBDEPARTMENT>();
        alDepartment = this.getDepartmentData(pStmt);
        TBDEPARTMENT[] departments = new TBDEPARTMENT[alDepartment.size()];
		alDepartment.toArray(departments);
		return departments;
    }
	
	public int countDepartmentByFilter(String DEPARTMENTID, String DEPARTMENTNAME) throws SQLException {
		String DepartmentIdFilter = "1=1";
    	String DepartmentNameFilter = "1=1";

    	if(DEPARTMENTID != "" && DEPARTMENTID.trim().length() > 0){
    		DepartmentIdFilter = "UPPER(DEPARTMENTID) = '" + DEPARTMENTID.toUpperCase() + "'";	
    	}
    	if(DEPARTMENTNAME != "" && DEPARTMENTNAME.trim().length() > 0){
    		DepartmentNameFilter = "UPPER(DEPARTMENTNAME) LIKE ('%" + DEPARTMENTNAME.toUpperCase() + "%')";	
    	} 
    	
        String sqlStmt = "SELECT * FROM TBDEPARTMENT " + 
        				"WHERE " +  DepartmentIdFilter + " AND " + DepartmentNameFilter + " " +
        				"AND RECORDSTATUS = ? ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteDepartment(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBDEPARTMENT WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBDEPARTMENT SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);

			pStmt.setInt(1, ID);
			
			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete department master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public TBDEPARTMENT[] getDepartmentAllNoOffset() throws SQLException {
    	String tableName = "TBDEPARTMENT";
    	String sqlStmt = "SELECT * FROM "+tableName+" WHERE RECORDSTATUS = ? ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBDEPARTMENT> alDepartment;

		alDepartment = this.getDepartmentData(pStmt);

		if (alDepartment.size() == 0)
			return null;

		TBDEPARTMENT[] departments = new TBDEPARTMENT[alDepartment.size()];

		alDepartment.toArray(departments);

		return departments;
	}
	
	public TBDEPARTMENT[] getDepartmentAllNoOffsetOnly(String DEPARTMENTID) throws SQLException {
    	String tableName = "TBDEPARTMENT";
    	String sqlStmt = "SELECT * FROM "+tableName+" WHERE RECORDSTATUS = ? AND DEPARTMENTID IN (" + DEPARTMENTID + ") ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBDEPARTMENT> alDepartment;

		alDepartment = this.getDepartmentData(pStmt);

		if (alDepartment.size() == 0)
			return null;

		TBDEPARTMENT[] departments = new TBDEPARTMENT[alDepartment.size()];

		alDepartment.toArray(departments);

		return departments;
	}

	public ArrayList<TBDEPARTMENT> getDepartmentData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBDEPARTMENT> alDepartment = new ArrayList<TBDEPARTMENT>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBDEPARTMENT oDepartment = new TBDEPARTMENT();
			oDepartment.setID(srs.getInt("ID"));
			oDepartment.setDEPARTMENTID(srs.getString("DEPARTMENTID"));
			oDepartment.setDEPARTMENTNAME(srs.getString("DEPARTMENTNAME"));
			oDepartment.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oDepartment.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oDepartment.setCREATEDBY(srs.getString("CREATEDBY"));
			oDepartment.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oDepartment.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alDepartment.add(oDepartment);
		}

		return alDepartment;
	}
}
