package Biometrics;

import java.util.Date;

public class TBCLIENT {
	private int ID;
	private String CLIENTIP;
	private String CLIENTNAME;
	private String AREAID;
	private String RECORDSTATUS;
	private String CREATEDBY;
	private Date CREATEDSTAMP;
	private String LASTUPDATETIMEDBY;
	private Date LASTUPDATETIMEDSTAMP;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getCLIENTIP() {return CLIENTIP;}
	public void setCLIENTIP(String CLIENTIP) {this.CLIENTIP = CLIENTIP;}
	
	public String getCLIENTNAME() {return CLIENTNAME;}
	public void setCLIENTNAME(String CLIENTNAME) {this.CLIENTNAME = CLIENTNAME;}
	
	public String getAREAID() {return AREAID;}
	public void setAREAID(String AREAID) {this.AREAID = AREAID;}
	
	public String getRECORDSTATUS() {return RECORDSTATUS;}
	public void setRECORDSTATUS(String RECORDSTATUS) {this.RECORDSTATUS = RECORDSTATUS;}
	
	public String getCREATEDBY() {return CREATEDBY;}
	public void setCREATEDBY(String CREATEDBY) {this.CREATEDBY = CREATEDBY;}
	
	public Date getCREATEDSTAMP() {return CREATEDSTAMP;}
	public void setCREATEDSTAMP(Date CREATEDSTAMP) {this.CREATEDSTAMP = CREATEDSTAMP;}
	
	public String getLASTUPDATETIMEDBY() {return LASTUPDATETIMEDBY;}
	public void setLASTUPDATETIMEDBY(String LASTUPDATETIMEDBY) {this.LASTUPDATETIMEDBY = LASTUPDATETIMEDBY;}
	
	public Date getLASTUPDATETIMEDSTAMP() {return LASTUPDATETIMEDSTAMP;}
	public void setLASTUPDATETIMEDSTAMP(Date LASTUPDATETIMEDSTAMP) {this.LASTUPDATETIMEDSTAMP = LASTUPDATETIMEDSTAMP;}
	
}
