package Biometrics;

import java.util.Date;

public class REPORT4 {
	private String ATTENDANCEDATE;
	private String STAFFID;
	private String STAFFNAME;
	private String DEPARTMENTID;
	private String DEPARTMENTNAME;
	private String REASONID;
	private String REASONNAME;
	private String DESCRIPTION;
	private String CHECKIN;
	private String CHECKOUT;
	private String WORKHOUR;
	
	public String getATTENDANCEDATE() {return ATTENDANCEDATE;}
	public void setATTENDANCEDATE(String ATTENDANCEDATE) {this.ATTENDANCEDATE = ATTENDANCEDATE;}
	
	public String getSTAFFID() {return STAFFID;}
	public void setSTAFFID(String STAFFID) {this.STAFFID = STAFFID;}
	
	public String getSTAFFNAME() {return STAFFNAME;}
	public void setSTAFFNAME(String STAFFNAME) {this.STAFFNAME = STAFFNAME;}

	public String getDEPARTMENTID() {return DEPARTMENTID;}
	public void setDEPARTMENTID(String DEPARTMENTID) {this.DEPARTMENTID = DEPARTMENTID;}
	
	public String getDEPARTMENTNAME() {return DEPARTMENTNAME;}
	public void setDEPARTMENTNAME(String DEPARTMENTNAME) {this.DEPARTMENTNAME = DEPARTMENTNAME;}

	public String getREASONID() {return REASONID;}
	public void setREASONID(String REASONID) {this.REASONID = REASONID;}
	
	public String getREASONNAME() {return REASONNAME;}
	public void setREASONNAME(String REASONNAME) {this.REASONNAME = REASONNAME;}

	public String getDESCRIPTION() {return DESCRIPTION;}
	public void setDESCRIPTION(String DESCRIPTION) {this.DESCRIPTION = DESCRIPTION;}
	
	public String getCHECKIN() {return CHECKIN;}
	public void setCHECKIN(String CHECKIN) {this.CHECKIN = CHECKIN;}

	public String getCHECKOUT() {return CHECKOUT;}
	public void setCHECKOUT(String CHECKOUT) {this.CHECKOUT = CHECKOUT;}
	
	public String getWORKHOUR() {return WORKHOUR;}
	public void setWORKHOUR(String WORKHOUR) {this.WORKHOUR = WORKHOUR;}
}
