package Biometrics;

import java.sql.Connection;
import java.sql.SQLException;

import Database.MsSQLDatabase;





import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;



public class DatabaseManager {

	String dbUserId = "";
	String dbPassword = "";
	String dbUrl = "";
	
	private MsSQLDatabase msSQLDatabase;
	
	public DatabaseManager(){
		
	}
	
    public void setDBUserId(String dBUserId) {
		this.dbUserId = dBUserId;
	}

	public void setDBPassword(String dBPassword) {
		this.dbPassword = dBPassword;
	}

	public void setDBUrl(String dBUrl) {
		this.dbUrl = dBUrl;
	}
	
	public boolean openConnection() throws ClassNotFoundException {

		msSQLDatabase = new MsSQLDatabase(this.dbUserId, this.dbPassword, this.dbUrl);

		try {
			if (!msSQLDatabase.openConnection())
			{
				return false;
			}
		} catch (SQLException ex) { 
			System.out.println(ex.getMessage());
			return false;
		} 

		return true;
	}
	
	public boolean closeConnection(){
		
		try {
			msSQLDatabase.closeConnection();
			
			msSQLDatabase = null;
			
		} catch (SQLException ex) {
			return false;
		}
		
		return true;
	}
	
	public Connection getConnection()
	{
		if (msSQLDatabase == null)
			return null;
		
		return msSQLDatabase.getConnection();
	}
	
	
	public void testcon(){
		
		 Connection conn = null;
        String dbName = "ParksonScale";
        String serverip="PEAR";
        String serverport="1433";
        String url = "jdbc:sqlserver://"+serverip+"\\SQLEXPRESS:"+serverport+";databaseName="+dbName+"";
        Statement stmt = null;
        ResultSet result = null;
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        String databaseUserName = "sa";
        String databasePassword = "sssadmin";
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url, databaseUserName, databasePassword);
            stmt = conn.createStatement();
            result = null;
            String pa,us;
            
	            

	            conn.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		
	}
	
}
