package Biometrics;

import java.util.Date;

public class TBSTAFF {
	private int ID;
	private String STAFFID;
	private String STAFFNAME;
	private String CARDNO;
	private String PASSWORD;
	private String PRIVILEGE;
	private String ACCESSGROUPID;
	private String DEPARTMENTID;
	private String RECORDSTATUS;
	private String CREATEDBY;
	private Date CREATEDSTAMP;
	private String LASTUPDATETIMEDBY;
	private Date LASTUPDATETIMEDSTAMP;
	private String REGISTEREDBRANCHID;
	
	private String ACCESSGROUPNAME;
	private String DEPARTMENTNAME;
	
	private int THUMBCOUNT;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getSTAFFID() {return STAFFID;}
	public void setSTAFFID(String STAFFID) {this.STAFFID = STAFFID;}
	
	public String getSTAFFNAME() {return STAFFNAME;}
	public void setSTAFFNAME(String STAFFNAME) {this.STAFFNAME = STAFFNAME;}
	
	public String getCARDNO() {return CARDNO;}
	public void setCARDNO(String CARDNO) {this.CARDNO = CARDNO;}
	
	public String getPASSWORD() {return PASSWORD;}
	public void setPASSWORD(String PASSWORD) {this.PASSWORD = PASSWORD;}
	
	public String getPRIVILEGE() {return PRIVILEGE;}
	public void setPRIVILEGE(String PRIVILEGE) {this.PRIVILEGE = PRIVILEGE;}
	
	public String getACCESSGROUPID() {return ACCESSGROUPID;}
	public void setACCESSGROUPID(String ACCESSGROUPID) {this.ACCESSGROUPID = ACCESSGROUPID;}
	
	public String getDEPARTMENTID() {return DEPARTMENTID;}
	public void setDEPARTMENTID(String DEPARTMENTID) {this.DEPARTMENTID = DEPARTMENTID;}
	
	public String getRECORDSTATUS() {return RECORDSTATUS;}
	public void setRECORDSTATUS(String RECORDSTATUS) {this.RECORDSTATUS = RECORDSTATUS;}
	
	public String getCREATEDBY() {return CREATEDBY;}
	public void setCREATEDBY(String CREATEDBY) {this.CREATEDBY = CREATEDBY;}
	
	public Date getCREATEDSTAMP() {return CREATEDSTAMP;}
	public void setCREATEDSTAMP(Date CREATEDSTAMP) {this.CREATEDSTAMP = CREATEDSTAMP;}
	
	public String getLASTUPDATETIMEDBY() {return LASTUPDATETIMEDBY;}
	public void setLASTUPDATETIMEDBY(String LASTUPDATETIMEDBY) {this.LASTUPDATETIMEDBY = LASTUPDATETIMEDBY;}
	
	public Date getLASTUPDATETIMEDSTAMP() {return LASTUPDATETIMEDSTAMP;}
	public void setLASTUPDATETIMEDSTAMP(Date LASTUPDATETIMEDSTAMP) {this.LASTUPDATETIMEDSTAMP = LASTUPDATETIMEDSTAMP;}
	
	public String getREGISTEREDBRANCHID() {return REGISTEREDBRANCHID;}
	public void setREGISTEREDBRANCHID(String REGISTEREDBRANCHID) {this.REGISTEREDBRANCHID = REGISTEREDBRANCHID;}
	
	
	
	public String getACCESSGROUPNAME() {return ACCESSGROUPNAME;}
	public void setACCESSGROUPNAME(String ACCESSGROUPNAME) {this.ACCESSGROUPNAME = ACCESSGROUPNAME;}
	
	public String getDEPARTMENTNAME() {return DEPARTMENTNAME;}
	public void setDEPARTMENTNAME(String DEPARTMENTNAME) {this.DEPARTMENTNAME = DEPARTMENTNAME;}
	
	public int getTHUMBCOUNT() {return THUMBCOUNT;}
	public void setTHUMBCOUNT(int THUMBCOUNT) {this.THUMBCOUNT = THUMBCOUNT;}
}
