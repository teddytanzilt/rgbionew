package Biometrics;

import java.util.Date;

public class TBBRANCH {
	private int ID;
	private String BRANCHID;
	private String BRANCHNAME;
	private String AREAID;
	private String EMAILNOTIFYADDRESS;
	private String RECORDSTATUS;
	private String CREATEDBY;
	private Date CREATEDSTAMP;
	private String LASTUPDATETIMEDBY;
	private Date LASTUPDATETIMEDSTAMP;
	
	private String AREANAME;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getBRANCHID() {return BRANCHID;}
	public void setBRANCHID(String BRANCHID) {this.BRANCHID = BRANCHID;}
	
	public String getBRANCHNAME() {return BRANCHNAME;}
	public void setBRANCHNAME(String BRANCHNAME) {this.BRANCHNAME = BRANCHNAME;}
	
	public String getAREAID() {return AREAID;}
	public void setAREAID(String AREAID) {this.AREAID = AREAID;}
	
	public String getEMAILNOTIFYADDRESS() {return EMAILNOTIFYADDRESS;}
	public void setEMAILNOTIFYADDRESS(String EMAILNOTIFYADDRESS) {this.EMAILNOTIFYADDRESS = EMAILNOTIFYADDRESS;}
	
	public String getRECORDSTATUS() {return RECORDSTATUS;}
	public void setRECORDSTATUS(String RECORDSTATUS) {this.RECORDSTATUS = RECORDSTATUS;}
	
	public String getCREATEDBY() {return CREATEDBY;}
	public void setCREATEDBY(String CREATEDBY) {this.CREATEDBY = CREATEDBY;}
	
	public Date getCREATEDSTAMP() {return CREATEDSTAMP;}
	public void setCREATEDSTAMP(Date CREATEDSTAMP) {this.CREATEDSTAMP = CREATEDSTAMP;}
	
	public String getLASTUPDATETIMEDBY() {return LASTUPDATETIMEDBY;}
	public void setLASTUPDATETIMEDBY(String LASTUPDATETIMEDBY) {this.LASTUPDATETIMEDBY = LASTUPDATETIMEDBY;}
	
	public Date getLASTUPDATETIMEDSTAMP() {return LASTUPDATETIMEDSTAMP;}
	public void setLASTUPDATETIMEDSTAMP(Date LASTUPDATETIMEDSTAMP) {this.LASTUPDATETIMEDSTAMP = LASTUPDATETIMEDSTAMP;}

	public String getAREANAME() {return AREANAME;}
	public void setAREANAME(String AREANAME) {this.AREANAME = AREANAME;}
}
