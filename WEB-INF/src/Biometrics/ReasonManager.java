package Biometrics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ReasonManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addReason(TBREASON oReason) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBREASON WHERE REASONID = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oReason.getREASONID());
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Reason [" + oReason.getREASONID()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBREASON " + 
			"( REASONID , REASONNAME, RECORDSTATUS, CREATEDBY, CREATEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oReason.getREASONID());
			pStmt.setString(2, oReason.getREASONNAME());
			pStmt.setString(3, "A");
			pStmt.setString(4, oReason.getCREATEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateReason(TBREASON oReason) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBREASON WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oReason.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Reason [" + oReason.getREASONID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBREASON SET "
								+ "  REASONID = ? " 
								+ ", REASONNAME = ? "
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oReason.getREASONID());
			pStmt.setString(2, oReason.getREASONNAME());
			pStmt.setString(3, oReason.getLASTUPDATETIMEDBY());
			pStmt.setInt(4, oReason.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public TBREASON getReasonById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBREASON WHERE ID = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBREASON> alReason;

		alReason = this.getReasonData(pStmt);

		if (alReason.size() == 0)
			return null;

		return alReason.get(0);		
	}
	
	public TBREASON getReasonByReasonId(String REASONID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBREASON WHERE UPPER(REASONID) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, REASONID.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBREASON> alReason;

		alReason = this.getReasonData(pStmt);

		if (alReason.size() == 0)
			return null;

		return alReason.get(0);		
	}
	
	public TBREASON getReasonByReasonName(String REASONNAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBREASON WHERE UPPER(REASONNAME) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, REASONNAME.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBREASON> alReason;

		alReason = this.getReasonData(pStmt);

		if (alReason.size() == 0)
			return null;

		return alReason.get(0);		
	}
	
	public TBREASON[] getReasonAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBREASON";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    		
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum " +
    					"FROM "+tableName+" WHERE RECORDSTATUS = ?) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBREASON> alReason;

		alReason = this.getReasonData(pStmt);

		if (alReason.size() == 0)
			return null;

		TBREASON[] areas = new TBREASON[alReason.size()];

		alReason.toArray(areas);

		return areas;
	}
	
	public int countReasonAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBREASON WHERE RECORDSTATUS = ? ";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBREASON[] getReasonByFilter(String REASONID, String REASONNAME, String offset) throws SQLException {
    	String ReasonIdFilter = "1 = 1";
    	String ReasonNameFilter = "1 = 1";
    	String itemPerPage = "30";
    	if(REASONID != "" && REASONID.trim().length() > 0){
    		ReasonIdFilter = "UPPER(REASONID) = '" + REASONID.toUpperCase() + "'";	
    	}
    	if(REASONNAME != "" && REASONNAME.trim().length() > 0){
    		ReasonNameFilter = "UPPER(REASONNAME) LIKE ('%" + REASONNAME.toUpperCase() + "%')";	
    	} 
    	String tableName = "TBREASON";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum FROM "+tableName+" " +
    					"WHERE " + ReasonIdFilter + " AND " + ReasonNameFilter + " AND RECORDSTATUS = ? ) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        
        ArrayList<TBREASON> alReason = new ArrayList<TBREASON>();
        alReason = this.getReasonData(pStmt);
        TBREASON[] areas = new TBREASON[alReason.size()];
		alReason.toArray(areas);
		return areas;
    }
	
	public int countReasonByFilter(String REASONID, String REASONNAME) throws SQLException {
		String ReasonIdFilter = "1=1";
    	String ReasonNameFilter = "1=1";

    	if(REASONID != "" && REASONID.trim().length() > 0){
    		ReasonIdFilter = "UPPER(REASONID) = '" + REASONID.toUpperCase() + "'";	
    	}
    	if(REASONNAME != "" && REASONNAME.trim().length() > 0){
    		ReasonNameFilter = "UPPER(REASONNAME) LIKE ('%" + REASONNAME.toUpperCase() + "%')";	
    	} 
    	
        String sqlStmt = "SELECT * FROM TBREASON " + 
        				"WHERE " +  ReasonIdFilter + " AND " + ReasonNameFilter + " " +
        				"AND RECORDSTATUS = ? ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteReason(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBREASON WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBREASON SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);

			pStmt.setInt(1, ID);
			
			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete area master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public TBREASON[] getReasonAllNoOffset() throws SQLException {
    	String tableName = "TBREASON";
    	String sqlStmt = "SELECT * FROM "+tableName+" WHERE RECORDSTATUS = ? ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBREASON> alReason;

		alReason = this.getReasonData(pStmt);

		if (alReason.size() == 0)
			return null;

		TBREASON[] areas = new TBREASON[alReason.size()];

		alReason.toArray(areas);

		return areas;
	}

	public ArrayList<TBREASON> getReasonData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBREASON> alReason = new ArrayList<TBREASON>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBREASON oReason = new TBREASON();
			oReason.setID(srs.getInt("ID"));
			oReason.setREASONID(srs.getString("REASONID"));
			oReason.setREASONNAME(srs.getString("REASONNAME"));
			oReason.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oReason.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oReason.setCREATEDBY(srs.getString("CREATEDBY"));
			oReason.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oReason.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alReason.add(oReason);
		}

		return alReason;
	}
}
