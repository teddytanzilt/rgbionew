package Biometrics;

import java.util.Date;

public class REPORT1 {
	private String STAFFID;
	private String STAFFNAME;
	private String REGISTERED;
	private String STATUS;
	
	public String getSTAFFID() {return STAFFID;}
	public void setSTAFFID(String STAFFID) {this.STAFFID = STAFFID;}
	
	public String getSTAFFNAME() {return STAFFNAME;}
	public void setSTAFFNAME(String STAFFNAME) {this.STAFFNAME = STAFFNAME;}
	
	public String getREGISTERED() {return REGISTERED;}
	public void setREGISTERED(String REGISTERED) {this.REGISTERED = REGISTERED;}
	
	public String getSTATUS() {return STATUS;}
	public void setSTATUS(String STATUS) {this.STATUS = STATUS;}
}
