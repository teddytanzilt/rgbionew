package util;

import java.text.DecimalFormat;;

public final class NumberFormatConverter {

	public static String format(float value, String format) {
		DecimalFormat df = new DecimalFormat(format);

		String result = df.format(value);

		return result;
	}
}
