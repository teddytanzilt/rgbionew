package util;

import java.util.Date;
import java.util.Locale;

import java.text.SimpleDateFormat;
import java.text.DateFormat;

import java.util.TimeZone;
import java.text.ParseException;

public final class DateFormatConverter {

	public static String format(Date date, String format) {
		DateFormat df = new SimpleDateFormat(format);
		String result = df.format(date);
		return result;
	}

	public static String format2(Date date, String format) {
		DateFormat df = new SimpleDateFormat(format);
		df.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
		String result = df.format(date);
		return result;
	}
	
	public static String format3(String date, String fromFormat, String toFormat)  throws ParseException {
		try {
		  Date utilDate = null;
	      SimpleDateFormat formatter = new SimpleDateFormat(fromFormat);
	      utilDate = formatter.parse(date);
	      String result = format2(utilDate, toFormat);
	      return result;
	    } catch (ParseException e) {
	      /*System.out.println(e.toString());
	      e.printStackTrace();*/
	    	return date;
	    }
	}

	public static String getWeekDay(String date)  throws ParseException {
		try {
			int year = Integer.parseInt(date.substring(0, 4));
			int month = Integer.parseInt(date.substring(4, 6));
			int day = Integer.parseInt(date.substring(6, 8));
			
			String dateString = String.format("%d-%d-%d", year, month, day);
			Date dateDate = new SimpleDateFormat("yyyy-M-d").parse(dateString);

			String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(dateDate);

			return dayOfWeek;
	    } catch (ParseException e) {
	      /*System.out.println(e.toString());
	      e.printStackTrace();*/
	    	return date;
	    }
	}
}
