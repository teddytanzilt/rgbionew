package Database;

import java.sql.*;

public class MsSQLDatabase {
	
	private String userid;
	private String password;
	private String url;
	private Connection msSqlConn;
	
	public MsSQLDatabase(String userid, String password, String url){
		this.userid = userid;
		this.password = password;
		this.url = url;
	}
	
	public boolean openConnection() throws SQLException, ClassNotFoundException
	{
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		if (msSqlConn == null)
			this.msSqlConn = DriverManager.getConnection(url, userid, password);
		else if(msSqlConn.isClosed())
			this.msSqlConn = DriverManager.getConnection(url, userid, password);
		return true;
	}
	
	public void closeConnection() throws SQLException
	{
		if (!msSqlConn.isClosed())
			msSqlConn.close();
	}
	
	public Connection getConnection()
	{
		return msSqlConn;
	}

}
