<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String sSuccessPageName = "UserList.jsp"; %>
<% String sBackPageName = "UserEdit.jsp"; %>

<%	
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0;
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	UserMgr.setConnection(DBMgr.getConnection());	
	
	TBUSER SessionUser = new TBUSER();		
	sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
	String txtUserName = request.getParameter("txtUserName").toString().trim();
	String txtPassword = request.getParameter("txtPassword").toString().trim();
	String txtName = request.getParameter("txtName").toString().trim();
	SessionUser = UserMgr.getUserById(SessionUserId);
	String cbRole = request.getParameter("cbRole").toString().trim();
	String cbDepartmentId = request.getParameter("cbDepartmentId").toString().trim();
	
	TBUSER currentUser = UserMgr.getUserById(sId);
	currentUser.setUSERNAME(txtUserName);
	currentUser.setNAME(txtName);
	currentUser.setPASSWORD(txtPassword);
	currentUser.setLASTUPDATETIMEDBY(SessionUser.getUSERNAME());
	currentUser.setROLE(cbRole);
	currentUser.setDEPARTMENTID(cbDepartmentId);
	
	boolean updateStatus = UserMgr.updateUser(currentUser);
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (updateStatus)  
		response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
	else{
		request.getSession().setAttribute("LastErrMsg", UserMgr.getErrorMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
	}
} catch (Exception e) {
	request.getSession().setAttribute("LastErrMsg", e.getMessage());
	response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
}
%>

