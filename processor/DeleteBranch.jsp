<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBBRANCH"%>

<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />
<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />

<% String sSuccessPageName = "BranchList.jsp"; %>
<% String sBackPageName = "BranchList.jsp"; %>

<%	
int SessionBranchId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0;
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	BranchMgr.setConnection(DBMgr.getConnection());	
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	
	boolean deleteStatus = BranchMgr.deleteBranch(sId);
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (deleteStatus)  
		response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
	else{
		request.getSession().setAttribute("LastErrMsg", BranchMgr.getErrorMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
} catch (Exception e) {
	request.getSession().setAttribute("LastErrMsg", e.getMessage());
	response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
}
%>

