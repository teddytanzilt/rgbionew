<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBATTENDANCE"%>

<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "AttendanceList.jsp" + CurrentParam; %>
<% String sBackPageName = "AttendanceEdit.jsp"; %>

<%	
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0; int sTotal = 0;
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	AttendanceMgr.setConnection(DBMgr.getConnection());	
	
	TBUSER SessionUser = new TBUSER();
	SessionUser = UserMgr.getUserById(SessionUserId);

	sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
	sTotal = Integer.parseInt(String.valueOf(request.getParameter("hdnTotal")));
	String cbReasonId = request.getParameter("cbReasonId").toString().trim();
	String txtDescription = request.getParameter("txtDescription").toString().trim();
	
	if(sTotal == 1) {
		sSuccessPageName = "WorkHourList.jsp" + CurrentParam; 
	}
	
	TBATTENDANCE currentAttendance = AttendanceMgr.getAttendanceById(sId);
	currentAttendance.setREASONID(cbReasonId);
	currentAttendance.setDESCRIPTION(txtDescription);
	
	boolean updateStatus = AttendanceMgr.updateAttendance(currentAttendance);
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (updateStatus)  
		response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
	else{
		request.getSession().setAttribute("LastErrMsg", AttendanceMgr.getErrorMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
	}
} catch (Exception e) {
	request.getSession().setAttribute("LastErrMsg", e.getMessage());
	response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
}
%>

