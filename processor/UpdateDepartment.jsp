<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBDEPARTMENT"%>

<jsp:useBean id="DepartmentMgr" scope="page" class="Biometrics.DepartmentManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String sSuccessPageName = "DepartmentList.jsp"; %>
<% String sBackPageName = "DepartmentEdit.jsp"; %>

<%	
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0;
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	DepartmentMgr.setConnection(DBMgr.getConnection());	
	
	TBUSER SessionUser = new TBUSER();
	SessionUser = UserMgr.getUserById(SessionUserId);
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
	String txtDepartmentId = request.getParameter("txtDepartmentId").toString().trim();
	String txtDepartmentName = request.getParameter("txtDepartmentName").toString().trim();
	
	TBDEPARTMENT currentDepartment = DepartmentMgr.getDepartmentById(sId);
	currentDepartment.setDEPARTMENTID(txtDepartmentId);
	currentDepartment.setDEPARTMENTNAME(txtDepartmentName);
	currentDepartment.setLASTUPDATETIMEDBY(SessionUser.getUSERNAME());
	
	boolean updateStatus = DepartmentMgr.updateDepartment(currentDepartment);
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (updateStatus)  
		response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
	else{
		request.getSession().setAttribute("LastErrMsg", DepartmentMgr.getErrorMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
	}
} catch (Exception e) {
	request.getSession().setAttribute("LastErrMsg", e.getMessage());
	response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
}
%>

