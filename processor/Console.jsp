<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>

<jsp:useBean id="DBMgr" scope="session" class="dms.DatabaseManager" />
<jsp:useBean id="ProcessUtl" scope="session" class="util.ProcessUtil" />

<% String sSuccessPageName = "StaffList.jsp"; %>
<% String sBackPageName = "StaffList.jsp"; %>

<%	
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	int sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	
	String syncProgName = request.getSession().getServletContext().getInitParameter("SyncProgName");
	String syncProgLocation = request.getSession().getServletContext().getInitParameter("SyncProgLocation");
	
	Process process = Runtime.getRuntime().exec(syncProgLocation + syncProgName + " " + sId + " ");		
	while(ProcessUtl.CheckRunning(process)){
		
		
	}
	
	BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
	String line = "";
	while ((line = input.readLine()) != null) {
		
	}	
} catch (Exception e) {
	request.getSession().setAttribute("LastErrMsg", e.getMessage());
	response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
}
%>

