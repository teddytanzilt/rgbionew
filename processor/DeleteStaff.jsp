<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBSTAFF"%>
<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />
<jsp:useBean id="StaffMgr" scope="page" class="Biometrics.StaffManager" />
<jsp:useBean id="ProcessUtl" scope="session" class="util.ProcessUtil" />

<% String sSuccessPageName = "StaffList.jsp"; %>
<% String sBackPageName = "StaffList.jsp"; %>

<%	
int SessionStaffId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0;
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	StaffMgr.setConnection(DBMgr.getConnection());	
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	
	TBSTAFF currentStaff = StaffMgr.getStaffById(sId);	
	
	String resp = "";
	boolean deleteStatus = false;
	boolean deleteThumbStatus = false;
	boolean serviceError = false;
	resp = DeviceControl.deleteStaff(currentStaff.getSTAFFID());
	
	if(resp == "OK" || resp.equals("OK")) {
		deleteStatus = StaffMgr.deleteStaff(sId); 
		deleteThumbStatus = StaffMgr.deleteStaffThumb(currentStaff.getSTAFFID());
	} else {
		serviceError = true;
	}
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (deleteStatus) {
		response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
	} else { 
		if(serviceError) {
			request.getSession().setAttribute("LastErrMsg", resp);
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		} else {
			request.getSession().setAttribute("LastErrMsg", StaffMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		}
	}
} catch (Exception e) {
	request.getSession().setAttribute("LastErrMsg", e.getMessage());
	response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
}
%>

