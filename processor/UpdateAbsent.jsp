<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBATTENDANCE"%>

<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String sSuccessPageName = "AbsentList.jsp"; %>
<% String sBackPageName = "AbsentEdit.jsp"; %>

<%	
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0;
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	AttendanceMgr.setConnection(DBMgr.getConnection());	
	
	TBUSER SessionUser = new TBUSER();
	SessionUser = UserMgr.getUserById(SessionUserId);
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
	String cbReasonId = request.getParameter("cbReasonId").toString().trim();
	String txtDescription = request.getParameter("txtDescription").toString().trim();
	//String cbBranchId = request.getParameter("cbBranchId").toString().trim();
	//String cbDeviceId = request.getParameter("cbDeviceId").toString().trim();
	
	TBATTENDANCE currentAttendance = AttendanceMgr.getAttendanceById(sId);
	currentAttendance.setREASONID(cbReasonId);
	currentAttendance.setDESCRIPTION(txtDescription);
	//currentAttendance.setBRANCHID(cbBranchId);
	//currentAttendance.setDEVICEID(cbDeviceId);
	
	boolean updateStatus = AttendanceMgr.updateAbsent(currentAttendance);
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (updateStatus)  
		response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
	else{
		request.getSession().setAttribute("LastErrMsg", UserMgr.getErrorMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
} catch (Exception e) {
	request.getSession().setAttribute("LastErrMsg", e.getMessage());
	response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
}
%>

