<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBDEPARTMENT"%>
<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="DepartmentMgr" scope="page" class="Biometrics.DepartmentManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String sSuccessPageName = "DepartmentList.jsp"; %>
<% String sBackPageName = "DepartmentList.jsp"; %>

<%	
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	DepartmentMgr.setConnection(DBMgr.getConnection());
	UserMgr.setConnection(DBMgr.getConnection());	
	
	TBDEPARTMENT sDepartment = new TBDEPARTMENT();
	TBUSER SessionUser = new TBUSER();

	SessionUser = UserMgr.getUserById(SessionUserId);
	
	String txtDepartmentId = request.getParameter("txtDepartmentId").toString().trim();
	String txtDepartmentName = request.getParameter("txtDepartmentName").toString().trim();
	
	sDepartment.setDEPARTMENTID(txtDepartmentId);
	sDepartment.setDEPARTMENTNAME(txtDepartmentName);
	sDepartment.setCREATEDBY(SessionUser.getUSERNAME());
	
	boolean addStatus = DepartmentMgr.addDepartment(sDepartment);
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (addStatus)  
		response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
	else{
		request.getSession().setAttribute("LastErrMsg", UserMgr.getErrorMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
} catch (Exception e) {
	request.getSession().setAttribute("LastErrMsg", e.getMessage());
	response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
}
%>

