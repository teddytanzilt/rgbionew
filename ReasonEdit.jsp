<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.ReasonManager"%>
<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="ReasonMgr" scope="page" class="Biometrics.ReasonManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String sTableTitle = "Edit Reason"; %>
<% String sDataTitle = "Reason Detail"; %>
<% String sPageName = "ReasonEdit.jsp"; %>
<% String sBackPageName = "ReasonList.jsp"; %>
<% String sProcessPageName = "processor/UpdateReason.jsp"; %>

<%!Biometrics.TBUSER SessionUser;%>
<%!Biometrics.TBREASON sReason;%>
<%!String sCurrentError = "";%>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0; String sReasonId = ""; String sReasonName = "";
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	ReasonMgr.setConnection(DBMgr.getConnection());
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	SessionUser = UserMgr.getUserById(SessionUserId); 	
	sReason = ReasonMgr.getReasonById(sId);	

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (sReason == null) {
		request.getSession().setAttribute("LastErrMsg", "Reason not found");
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		return;
	}
	
	sId = sReason.getID();
	sReasonId = sReason.getREASONID();
	sReasonName = sReason.getREASONNAME();
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
 
function CheckForm(theform) {

	if (!CheckReasonId(theform)){
		return (false);
	}
	
	if (!CheckReasonName(theform)){
		return (false);
	}
	  
	return (true);
}

function CheckReasonId(theForm){
    if (theForm.txtReasonId.value==""){
       	alert("Please enter Reason Id!");
        return (false);
    }
    return (true);
}

function CheckReasonName(theForm){
    if (theForm.txtReasonName.value==""){
       	alert("Please enter Reason Name!");
        return (false);
    }
    return (true);
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
								<input type=hidden name="hdnId" value='<%=sId%>' />
                                <div class="form-group">
                                    <label>Reason Id</label>
                                    <input class="form-control" id="txtReasonId" type="text" name="txtReasonId" value = "<%=sReasonId%>" readonly />
                                </div>
                                <div class="form-group">
                                    <label>Reason Name</label>
                                    <input class="form-control" id="txtReasonName" type="text" name="txtReasonName" value = "<%=sReasonName%>" />
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
