<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<link href="assets/css/jquery.tagsinput.min.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/jquery.1.7.1.min.js"></script>
<script type='text/javascript' src='assets/js/jquery-ui.1.8.16.min.js'></script>
<link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.1.8.13.css" />
	
<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBDEPARTMENT"%>

<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DepartmentMgr" scope="page" class="Biometrics.DepartmentManager" />

<% String sTableTitle = "Edit User"; %>
<% String sDataTitle = "User Detail"; %>
<% String sPageName = "UserEdit.jsp"; %>
<% String sBackPageName = "UserList.jsp"; %>
<% String sProcessPageName = "processor/UpdateUser.jsp"; %>

<%!Biometrics.TBUSER SessionUser;%>
<%!Biometrics.TBUSER sUser;%>
<%!TBDEPARTMENT[] aDepartments;%>
<%!String sCurrentError = "";%>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0; String sUserName = ""; String sName = ""; String sPassword = ""; String sRole = ""; String sDepartmentId = "";
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}

	sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	UserMgr.setConnection(DBMgr.getConnection());
	DepartmentMgr.setConnection(DBMgr.getConnection());
	SessionUser = UserMgr.getUserById(SessionUserId); 	
	sUser = UserMgr.getUserById(sId);
	aDepartments = DepartmentMgr.getDepartmentAllNoOffset();
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (sUser == null) {
		request.getSession().setAttribute("LastErrMsg", "User not found");
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		return;
	}
	
	sId = sUser.getID();
	sUserName = sUser.getUSERNAME();
	sName = sUser.getNAME();
	sPassword = sUser.getPASSWORD();
	sRole = sUser.getROLE();
	sDepartmentId = sUser.getDEPARTMENTID();
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript"> 
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
function CheckForm(theform) {

	if (!CheckUserName(theform)){
		return (false);
	}
	
	if (!CheckName(theform)){
		return (false);
	}
	
	if (!CheckPassword(theform)){
		return (false);
	}

	if (!CheckConfirmPassword(theform)){
		return (false);
	}

	if (!CheckAndComparePassword(theform)){
	     return (false);
	  }
	  
	return (true);
}

function CheckAndComparePassword(theForm){
    if (theForm.txtPassword.value!=theForm.txtConfirmPassword.value){
       alert("Password must be same as confirm password!");
        return (false);
    }
    return (true);
}

function CheckUserName(theForm){
    if (theForm.txtUserName.value==""){
       	alert("Please enter User Name!");
        return (false);
    }
    return (true);
}

function CheckName(theForm){
    if (theForm.txtName.value==""){
       	alert("Please enter Name!");
        return (false);
    }
    return (true);
}

function CheckPassword(theForm){
    if (theForm.txtPassword.value==""){
       	alert("Please enter Password!");
        return (false);
    }
    return (true);
}

function CheckConfirmPassword(theForm){
    if (theForm.txtConfirmPassword.value==""){
       	alert("Please enter Confirm Password!");
        return (false);
    }
    return (true);
}

</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
                           		<input type=hidden name="hdnId" value='<%=sId%>' />
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input class="form-control" id="txtUserName" type="text" name="txtUserName" value = "<%=sUserName%>" readonly />
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" id="txtName" type="text" name="txtName" value = "<%=sName%>"/>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" id="txtPassword" type="password" name="txtPassword" value = "<%=sPassword%>" />
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input class="form-control" id="txtConfirmPassword" type="password" name="txtConfirmPassword" value = "<%=sPassword%>" />
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
									<select class="form-control" name ="cbRole">
										<option value = "ADMIN" <%=sRole.equals("ADMIN")?"selected":""%>>ADMIN</option>
										<option value = "HOD" <%=sRole.equals("HOD")?"selected":""%>>HOD</option>
									</select>
                                </div>
                                <div class="form-group">
                                    <label>Department (HOD Only)</label>
									<input id='cbDepartmentId' type='text' class='tags' name = "cbDepartmentId" value = "<%=sDepartmentId%>">
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
    <script src="assets/js/jquery.tagsinput.min.js"></script>
    <script>
    jQuery.curCSS = function(element, prop, val) {
        return jQuery(element).css(prop, val);
    };
     var tags_array = [ { "id": "-", "label": "-", "value": "-" } ];
     <%if(aDepartments != null){ %>
		<%for (int i = 0; i < aDepartments.length; i++) { %>
     		var each_array = [];
     		each_array['id'] = '<%=aDepartments[i].getDEPARTMENTID()%>';
     		each_array['label'] = '<%=aDepartments[i].getDEPARTMENTNAME()%>';
     		each_array['value'] = '<%=aDepartments[i].getDEPARTMENTID()%>';
     
     		tags_array.push(each_array);
     	<%} %>
	<%} %>
    
    $('#cbDepartmentId').tagsInput({
		width: 'auto',
		autocomplete_url: '',
		autocomplete :{
	        'source': tags_array
	    }
	});
    
    </script>
</body>
<%@include file="layout/Footer.jsp" %>
