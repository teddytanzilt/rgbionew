<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.DepartmentManager"%>
<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="DepartmentMgr" scope="page" class="Biometrics.DepartmentManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String sTableTitle = "Edit Department"; %>
<% String sDataTitle = "Department Detail"; %>
<% String sPageName = "DepartmentEdit.jsp"; %>
<% String sBackPageName = "DepartmentList.jsp"; %>
<% String sProcessPageName = "processor/UpdateDepartment.jsp"; %>

<%!Biometrics.TBUSER SessionUser;%>
<%!Biometrics.TBDEPARTMENT sDepartment;%>
<%!String sCurrentError = "";%>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0; String sDepartmentId = ""; String sDepartmentName = "";
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	DepartmentMgr.setConnection(DBMgr.getConnection());
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	SessionUser = UserMgr.getUserById(SessionUserId); 	
	sDepartment = DepartmentMgr.getDepartmentById(sId);	

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (sDepartment == null) {
		request.getSession().setAttribute("LastErrMsg", "Department not found");
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		return;
	}
	
	sId = sDepartment.getID();
	sDepartmentId = sDepartment.getDEPARTMENTID();
	sDepartmentName = sDepartment.getDEPARTMENTNAME();
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
 
function CheckForm(theform) {

	if (!CheckDepartmentId(theform)){
		return (false);
	}
	
	if (!CheckDepartmentName(theform)){
		return (false);
	}
	  
	return (true);
}

function CheckDepartmentId(theForm){
    if (theForm.txtDepartmentId.value==""){
       	alert("Please enter Department Id!");
        return (false);
    }
    return (true);
}

function CheckDepartmentName(theForm){
    if (theForm.txtDepartmentName.value==""){
       	alert("Please enter Department Name!");
        return (false);
    }
    return (true);
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
								<input type=hidden name="hdnId" value='<%=sId%>' />
                                <div class="form-group">
                                    <label>Department Id</label>
                                    <input class="form-control" id="txtDepartmentId" type="text" name="txtDepartmentId" value = "<%=sDepartmentId%>" readonly />
                                </div>
                                <div class="form-group">
                                    <label>Department Name</label>
                                    <input class="form-control" id="txtDepartmentName" type="text" name="txtDepartmentName" value = "<%=sDepartmentName%>" />
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
