<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.AttendanceManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBREASON"%>

<jsp:useBean id="ReasonMgr" scope="page" class="Biometrics.ReasonManager" />
<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sTableTitle = "Edit Transaction"; %>
<% String sDataTitle = "Attendance Detail"; %>
<% String sPageName = "AttendanceEdit.jsp"; %>
<% String sBackPageName = "AttendanceList.jsp" + CurrentParam; %>
<% String sProcessPageName = "processor/UpdateAttendance.jsp"; %>

<%!Biometrics.TBUSER SessionUser;%>
<%!Biometrics.TBATTENDANCE sAttendance;%>
<%!TBREASON[] aReasons;%>
<%!String sCurrentError = "";%>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0; 
String sStaffId = ""; 
String sDeviceId = "";
String sBranchId = "";
String sInOut = "";
String sAttendanceDate = "";
String sAttendanceTime = "";
String sReasonId = "";
String sDescription = "";
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	AttendanceMgr.setConnection(DBMgr.getConnection());
	ReasonMgr.setConnection(DBMgr.getConnection());
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	SessionUser = UserMgr.getUserById(SessionUserId); 	
	sAttendance = AttendanceMgr.getAttendanceById(sId);
	aReasons = ReasonMgr.getReasonAllNoOffset();

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (sAttendance == null) {
		request.getSession().setAttribute("LastErrMsg", "Attendance not found");
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		return;
	}
	
	sId = sAttendance.getID();
	sStaffId = sAttendance.getSTAFFID();
	sDeviceId = sAttendance.getDEVICEID();
	sBranchId = sAttendance.getBRANCHID();
	sInOut = sAttendance.getINOUT();
	sAttendanceDate = sAttendance.getATTENDANCEDATE();
	sAttendanceTime = sAttendance.getATTENDANCETIME();
	sReasonId = sAttendance.getREASONID();
	sDescription = sAttendance.getDESCRIPTION();
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
 
function CheckForm(theform) {
	if (!CheckReasonId(theform)){
		return (false);
	}
	  
	return (true);
}

function CheckReasonId(theForm){
    if (theForm.cbReasonId.value==""){
       	alert("Please enter Reason Id!");
        return (false);
    }
    return (true);
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
								<input type=hidden name="hdnId" value='<%=sId%>' />
								<input type=hidden name="hdnTotal" value='0' />
                                <div class="form-group">
                                    <label>Staff Id</label>
                                    <input class="form-control" id="txtStaffId" type="text" name="txtStaffId" value = "<%=sStaffId%>" readonly />
                                </div>
								<div class="form-group">
                                    <label>Device Id</label>
                                    <input class="form-control" id="txtDeviceId" type="text" name="txtDeviceId" value = "<%=sDeviceId%>" readonly />
                                </div>
								<div class="form-group">
                                    <label>Branch Id</label>
                                    <input class="form-control" id="txtBranchId" type="text" name="txtBranchId" value = "<%=sBranchId%>" readonly />
                                </div>
								<div class="form-group">
                                    <label>IN / OUT</label>
                                    <input class="form-control" id="txtInOut" type="text" name="txtInOut" value = "<%=sInOut%>" readonly />
                                </div>
								<div class="form-group">
                                    <label>Attendance Date</label>
                                    <input class="form-control" id="txtAttendanceDate" type="text" name="txtAttendanceDate" value = "<%=sAttendanceDate%>" readonly />
                                </div>
								<div class="form-group">
                                    <label>Attendance Time</label>
                                    <input class="form-control" id="txtAttendanceTime" type="text" name="txtAttendanceTime" value = "<%=sAttendanceTime%>" readonly />
                                </div>
								<div class="form-group">
                                    <label>Reason Id</label>
                                    <select class="form-control" name ="cbReasonId">
										<option value = "-">-</option>
										<%if(aReasons != null){ %>
											<%for (int i = 0; i < aReasons.length; i++) { %>
												<option value = "<%=aReasons[i].getREASONID()%>" <%=(sReasonId!=null)?(sReasonId.equals(aReasons[i].getREASONID())?"selected":""):""%>><%=aReasons[i].getREASONNAME()%></option>
											<%} %>
										<%} %>
									</select>
                                </div>
								<div class="form-group">
                                    <label>Description</label>
                                    <input class="form-control" id="txtDescription" type="text" name="txtDescription" value = "<%=sDescription%>"  />
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
